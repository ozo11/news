package com.ozo.news.mapper;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.Comment;
import com.ozo.news.entity.News;
import com.ozo.news.entity.User;
import com.ozo.news.entity.Verify;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class VerifyMapperTest {

    @Autowired
    VerifyMapper verifyMapper;

    @Test
    void findAllVerify() {
        verifyMapper.findAllVerify().forEach(System.out::println);
    }

    @Test
    void findVerifyByCondition(){
        Verify verify = new Verify();
        //verify.setVerifyId(102L);
        User user = new User();
        user.setUserId(201L);
        verify.setUser(user);
        /*News news = new News();
        news.setNewId(103L);
        verify.setNews(news);*/
        //分页信息
        PageHelper.startPage(1, 3);
        //执行分页查询
        PageInfo<Verify> page = new PageInfo<Verify>(verifyMapper.findVerifyByCondition(verify));
        //打印用户列表
        List<Verify> list = page.getList();
        list.stream().forEach(System.out::println);

    }

    @Test
    void insertVerify(){
        Verify verify = new Verify();
        User user = new User();
        user.setUserId(201L);
        News news = new News();
        news.setNewId(101L);
        verify.setNews(news);
        verify.setUser(user);
        verify.setVerifyStatus("审核通过");
        verifyMapper.insertVerify(verify);
    }

    @Test
    void deleteVerify(){
        verifyMapper.deleteVerify(105L);
    }

    @Test
    void updateVerify() {
        Verify verify = new Verify();
        User user = new User();
        user.setUserId(301L);
        News news = new News();
        news.setNewId(102L);
        verify.setVerifyId(104L);
        verify.setUser(user);
        verify.setNews(news);
        verify.setVerifyStatus("tongguo");
        verifyMapper.updateVerify(verify);
    }
}