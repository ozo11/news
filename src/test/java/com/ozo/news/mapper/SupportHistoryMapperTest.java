package com.ozo.news.mapper;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.News;
import com.ozo.news.entity.SupportHistory;
import com.ozo.news.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class SupportHistoryMapperTest {

    @Autowired
    SupportHistoryMapper supportHistoryMapper;

    @Test
    void findAllSupportHistory() {
        supportHistoryMapper.findAllSupportHistory().forEach(System.out::println);
    }

    @Test
    void findSupportHistoryByCondition(){
        SupportHistory supportHistory = new SupportHistory();
        //supportHistory.setSupportId(101L);
        /*User user = new User();
        user.setUserId(301L);
        supportHistory.setUser(user);*/
        News news = new News();
        news.setNewId(102L);
        supportHistory.setNews(news);
        PageHelper.startPage(1, 3);
        PageInfo<SupportHistory> page = new PageInfo<SupportHistory>(supportHistoryMapper.findSupportHistoryByCondition(supportHistory));
        List<SupportHistory> list = page.getList();
        list.stream().forEach(System.out::println);

    }

    @Test
    void findSupportHistoryByParam(){
        PageHelper.startPage(1, 50);
        PageInfo<SupportHistory> page = new PageInfo<SupportHistory>(supportHistoryMapper.findSupportHistoryByParam("1","1"));
        List<SupportHistory> list = page.getList();
        list.stream().forEach(System.out::println);
    }

    @Test
    void insertSupportHistory(){
        SupportHistory supportHistory = new SupportHistory();
        User user = new User();
        user.setUserId(101L);
        News news = new News();
        news.setNewId(101L);
        supportHistory.setUser(user);
        supportHistory.setNews(news);
        supportHistoryMapper.insertSupportHistory(supportHistory);
    }

    @Test
    void deleteSupportHistory(){
        supportHistoryMapper.deleteSupportHistory(112L);
    }

    @Test
    void updateSupportHistory(){
        SupportHistory supportHistory = new SupportHistory();
        User user = new User();
        user.setUserId(102L);
        News news = new News();
        news.setNewId(102L);
        supportHistory.setSupportId(111L);
        supportHistory.setUser(user);
        supportHistory.setNews(news);
        supportHistoryMapper.updateSupportHistory(supportHistory);
    }
}