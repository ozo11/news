package com.ozo.news.mapper;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.User;
import com.ozo.news.util.GuuidUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@SpringBootTest
class UserMapperTest {

    @Autowired
    UserMapper userMapper;

    @Test
    void findAllUser() {
        userMapper.findAllUser().forEach(System.out::println);
    }

    @Test
    void findLoginUser(){
        User user = new User();
        user.setUsername("c1");
        user.setPassword("123");
        //user.setUserId(301L);
        User user1 = userMapper.findLoginUser(user);
        System.out.println(user1);
    }

    @Test
    void findUserByCondition(){
        User user = new User();
        //user.setUsername("张");
        user.setUserId(101L);
        PageHelper.startPage(1, 5);
        PageInfo<User> page = new PageInfo<User>(userMapper.findUserByCondition(user));
        System.out.println("\n");
        List<User> list = page.getList();
        list.stream().forEach(System.out::println);
        System.out.println("当前页码：第" + page.getPageNum() + "页");
        System.out.println("分页大小：每页" + page.getPageSize() + "条");
        System.out.println("数据总数：共" + page.getTotal() + "条");
        System.out.println("总页数：共" + page.getPages() + "页");
    }

    @Test
    void findUserByName(){
        userMapper.findUserByName("张三");
    }

    @Test
    void findUserByUsername(){
        userMapper.findUserByUsername("张").forEach(System.out::println);
    }

    @Test
    void findUserById(){
        System.out.println(userMapper.findUserById(101L));
    }

    @Test
    void insertUser(){
        User user = new User();
        user.setUserId(GuuidUtil.getUUID());
        user.setUsername("张三c");
        user.setPassword("123");
        user.setUserSex("男");
        user.setUserPhone("18055556666");
        user.setUserStatus("1");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        user.setEnrollTime(timestamp);
        System.out.println(user);
        userMapper.insertUser(user);
    }

    @Test
    void insertUserRole(){
        User user = new User();
        user.setUserId(GuuidUtil.getUUID());
        user.setUsername("张三c");
        user.setPassword("123");
        user.setUserSex("男");
        user.setUserPhone("18055556666");
        user.setUserStatus("1");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        user.setEnrollTime(timestamp);
        System.out.println(user);
        userMapper.insertUserRole(user.getUserId(),301L);
        userMapper.insertUser(user);
    }

    @Test
    void deleteUser(){
        userMapper.deleteUser(574459181570981888L);
    }

    @Test
    void updateUser(){
        User user = new User();
        user.setUserId(18L);
        user.setUsername("张三");
        user.setPassword("123");
        user.setUserSex("男");
        user.setUserPhone("18055556666");
        user.setUserStatus("0");
        System.out.println(user);
        userMapper.updateUser(user);
    }
}