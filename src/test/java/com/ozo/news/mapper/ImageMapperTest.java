package com.ozo.news.mapper;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.Classify;
import com.ozo.news.entity.Image;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ImageMapperTest {

    @Autowired
    ImageMapper imageMapper;

    @Test
    void findAllImage() {
        imageMapper.findAllImage();
    }

    @Test
    void findByCondition(){
        Image image = new Image();
        image.setConnectId(10L);
        //分页信息
        PageHelper.startPage(1, 2);
        //执行分页查询
        PageInfo<Image> userPage = new PageInfo<Image>(imageMapper.findImageByCondition(image));
        //打印用户列表
        System.out.println("\n");
        List<Image> userList = userPage.getList();
        userList.stream().forEach(System.out::println);
        //打印分页信息
        System.out.println("当前页码：第" + userPage.getPageNum() + "页");
        System.out.println("分页大小：每页" + userPage.getPageSize() + "条");
        System.out.println("数据总数：共" + userPage.getTotal() + "条");
        System.out.println("总页数：共" + userPage.getPages() + "页");
        imageMapper.findImageByCondition(image).forEach(System.out::println);
    }

    @Test
    void findImageByconnectId(){
        System.out.println(imageMapper.findImageByconnectId(101L));
    }

    @Test
    void findImageById(){
        System.out.println(imageMapper.findImageById(1L));
    }

    @Test
    void findImageByName(){
        System.out.println(imageMapper.findImageByName("2d27fd9846154500a102906a2c3dd52c.png"));
    }

    @Test
    void insertImage(){
        Image image = new Image();
        image.setConnectId(101L);
        image.setImageName("ozo1.jpg");
        image.setImageUrl("abc");
        System.out.println(image);
        imageMapper.insertImage(image);
    }

    @Test
    void deleteImage(){
        imageMapper.deleteImage(103L);
    }

    @Test
    void updateImage(){
        Image image = new Image();
        image.setConnectId(37L);
        image.setImageUrl("a");
        System.out.println(image);
        imageMapper.updateImage(image);
    }

    @Test
    void updateImageByConnectId(){
        Image image = new Image();
        image.setConnectId(1001L);
        image.setImageName("a");
        System.out.println(image);
        imageMapper.updateImageByConnectId(image);
    }
}