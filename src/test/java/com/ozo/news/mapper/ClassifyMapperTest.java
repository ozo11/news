package com.ozo.news.mapper;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.Classify;
import com.ozo.news.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ClassifyMapperTest {

    @Autowired
    ClassifyMapper classifyMapper;

    @Test
    void findAllClassify() {
        classifyMapper.findAllClassify().forEach(System.out::println);
    }

    @Test
    void findClassifyByCondition(){
        Classify classify = new Classify();
        classify.setClassifyName("球");
        //分页信息
        PageHelper.startPage(1, 2);
        //执行分页查询
        PageInfo<Classify> userPage = new PageInfo<Classify>(classifyMapper.findClassifyByCondition(classify));
        //打印用户列表
        System.out.println("\n");
        List<Classify> userList = userPage.getList();
        userList.stream().forEach(System.out::println);
        //打印分页信息
        System.out.println("当前页码：第" + userPage.getPageNum() + "页");
        System.out.println("分页大小：每页" + userPage.getPageSize() + "条");
        System.out.println("数据总数：共" + userPage.getTotal() + "条");
        System.out.println("总页数：共" + userPage.getPages() + "页");
        classifyMapper.findClassifyByCondition(classify).forEach(System.out::println);
    }

    @Test
    void findSecondClassify(){
        classifyMapper.findSecondClassify().forEach(System.out::println);
    }

    @Test
    void findClassifyById(){
        System.out.println(classifyMapper.findClassifyById(101001L));
    }

    @Test
    void insertClassify() {
        Classify classify = new Classify();
        classify.setClassifyName("马球");
        classify.setClassifyLevel(2);
        classify.setParentId(103L);
        System.out.println(classify);
        classifyMapper.insertClassify(classify);
    }

    @Test
    void deleteClassify() {
        classifyMapper.deleteClassify(109L);
    }

    @Test
    void updateClassify() {
        Classify classify = new Classify();
        classify.setClassifyId(112L);
        classify.setClassifyName("老马球");
        classify.setClassifyLevel(2);
        classify.setParentId(103L);
        System.out.println(classify);
        classifyMapper.updateClassify(classify);
    }

}