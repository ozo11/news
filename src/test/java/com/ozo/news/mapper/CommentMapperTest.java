package com.ozo.news.mapper;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.Comment;
import com.ozo.news.entity.News;
import com.ozo.news.entity.SupportHistory;
import com.ozo.news.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CommentMapperTest {

    @Autowired
    CommentMapper commentMapper;

    @Test
    void findAllComment() {
        commentMapper.findAllComment().forEach(System.out::println);
    }

    @Test
    void findCommentByCondition(){
        Comment comment = new Comment();
        //comment.setCommentId(102L);
        /*User user = new User();
        user.setUserId(301L);
        comment.setUser(user);
        News news = new News();
        news.setNewId(1003L);
        comment.setNews(news);*/
        PageHelper.startPage(1, 3);
        PageInfo<Comment> page = new PageInfo<Comment>(commentMapper.findCommentByCondition(comment));
        List<Comment> list = page.getList();
        list.stream().forEach(System.out::println);
    }

    @Test
    void findCommentByParam(){
        PageHelper.startPage(1, 3);
        PageInfo<Comment> page = new PageInfo<Comment>(commentMapper.findCommentByParam("c1","三"));
        List<Comment> list = page.getList();
        list.stream().forEach(System.out::println);
    }

    @Test
    void findCommentByNewId(){
        commentMapper.findCommentByNewId(1014L).forEach(System.out::println);
    }

    @Test
    void insertComment(){
        Comment comment = new Comment();
        User user = new User();
        user.setUserId(401L);
        News news = new News();
        news.setNewId(104L);
        comment.setUser(user);
        comment.setNews(news);
        comment.setCommentContent("root");
        commentMapper.insertComment(comment);
    }

    @Test
    void deleteComment(){
        commentMapper.deleteComment(106L);
    }

    @Test
    void updateComment(){
        Comment comment = new Comment();
        User user = new User();
        user.setUserId(501L);
        News news = new News();
        news.setNewId(105L);
        comment.setCommentId(105L);
        comment.setCommentContent("mmmmm");
        comment.setNews(news);
        comment.setUser(user);
        commentMapper.updateComment(comment);
    }
}