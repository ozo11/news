package com.ozo.news.mapper;

import com.ozo.news.entity.Role;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class RoleMapperTest {

    @Autowired
    private RoleMapper roleMapper;

    @Test
    void findRoleByUserId() {
        roleMapper.findRoleByUserId(101L).forEach(System.out::println);
    }

    @Test
    void findAllRole(){
        roleMapper.findAllRole().forEach(System.out::println);
    }

    @Test
    void findRoleByUsername(){
        roleMapper.findRoleByUsername("admin").forEach(System.out::println);
    }

    @Test
    void insertRole(){
        Role role = new Role();
        role.setRoleId(401L);
        role.setRoleName("观察者");
        role.setRoleDescribe("观察");
        System.out.println(role);
        roleMapper.insertRole(role);

    }

    @Test
    void deleteRole(){
        roleMapper.deleteRole(401L);
    }

    @Test
    void updateRole(){
        Role role = new Role();
        role.setRoleId(401L);
        role.setRoleName("观察者");
        role.setRoleDescribe("观察1号");
        System.out.println(role);
        roleMapper.updateRole(role);
    }

    @Test
    void updateRoleById(){
        Role role = new Role();
        role.setRoleName("观察者");
        role.setRoleDescribe("观察1号");
        System.out.println(role);
        roleMapper.updateRoleById(role,402L);
    }
}