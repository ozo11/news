package com.ozo.news.mapper;

import com.ozo.news.entity.Role;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PermsMapperTest {

    @Autowired
    private PermsMapper permsMapper;

    @Test
    void findPermsByRoleId() {
        permsMapper.findPermsByRoleId(201L).forEach(System.out::println);
    }

    @Test
    void findAllPerms(){
        permsMapper.findAllPerms().forEach(System.out::println);
    }
}