package com.ozo.news.mapper;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.Interest;
import com.ozo.news.entity.OpposeHistory;
import com.ozo.news.entity.User;
import org.junit.jupiter.api.Test;
import org.omg.PortableInterceptor.INACTIVE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class InterestMapperTest {

    @Autowired
    InterestMapper interestMapper;

    @Test
    void findAllInterest() {
        interestMapper.findAllInterest().forEach(System.out::println);
    }

    @Test
    void findInterestByCondition(){
        Interest interest = new Interest();
        interest.setInterestId(101L);
        /*User user = new User();
        user.setUserId(301L);
        interest.setUser(user);*/
        PageHelper.startPage(1, 3);
        PageInfo<Interest> page = new PageInfo<Interest>(interestMapper.findInterestByCondition(interest));
        List<Interest> list = page.getList();
        list.stream().forEach(System.out::println);
    }

    @Test
    void findInterestByUsername(){
        PageHelper.startPage(1, 50);
        PageInfo<Interest> page = new PageInfo<Interest>(interestMapper.findInterestByUsername("admin"));
        List<Interest> list = page.getList();
        list.stream().forEach(System.out::println);
    }

    @Test
    void insertInterest() {
        Interest interest = new Interest();
        User user = new User();
        user.setUserId(201L);
        interest.setUser1(user);
        interest.setInterestName("tianjin");
        interestMapper.insertInterest(interest);
    }

    @Test
    void deleteInterest(){
        interestMapper.deleteInterest(105L);
    }

    @Test
    void deleteInterestByParam(){
        interestMapper.deleteInterestByParam("中超",101L);
    }

    @Test
    void updateInterest(){
        Interest interest = new Interest();
        User user = new User();
        user.setUserId(301L);
        interest.setUser1(user);
        interest.setInterestName("youyong");
        interest.setInterestId(106L);
        interestMapper.updateInterest(interest);
    }
}