package com.ozo.news.mapper;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.Classify;
import com.ozo.news.entity.Comment;
import com.ozo.news.entity.News;
import com.ozo.news.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class NewsMapperTest {

    @Autowired
    NewsMapper newsMapper;

    @Test
    void findAllNews() {
        newsMapper.findAllNews().forEach(System.out::println);
    }

    @Test
    void findNewsByName(){
        newsMapper.findNewsByName("周").forEach(System.out::println);
    }

    @Test
    void findNewsById(){
//        News news = new News();
//        news.setNewId(1002L);
        System.out.println(newsMapper.findNewsById(1002L));
    }

    @Test
    void findNewsByCondition(){
        News news = new News();
        //news.setNewId(102L);
        User user = new User();
        user.setUserId(101L);
        news.setUser(user);
        Classify classify = new Classify();
        classify.setClassifyId(101001L);
        news.setClassify(classify);
        //分页信息
        PageHelper.startPage(1, 3);
        //执行分页查询
        PageInfo<News> page = new PageInfo<News>(newsMapper.findNewsByCondition(news));
        //打印用户列表
        List<News> list = page.getList();
        list.stream().forEach(System.out::println);
    }

    @Test
    void findNewsByParam(){
        //分页信息
        PageHelper.startPage(1, 8);
        //执行分页查询
        PageInfo<News> page = new PageInfo<News>(newsMapper.findNewsByParam("11",0L));
        //打印用户列表
        List<News> list = page.getList();
        list.stream().forEach(System.out::println);
    }

    @Test
    void findNewsByClassifyId(){
        PageHelper.startPage(1, 50);
        PageInfo<News> page = new PageInfo<News>(newsMapper.findNewsByClassifyId(101001L,101001L));
        List<News> list = page.getList();
        list.stream().forEach(System.out::println);
    }

    @Test
    void findNewsByClassifyName(){
        List<String> list1 = new ArrayList<>();
        list1.add("NBA");
        list1.add("英超");
        PageHelper.startPage(1, 50);
        PageInfo<News> page = new PageInfo<News>(newsMapper.findNewsByClassifyName(list1));
        List<News> list = page.getList();
        list.stream().forEach(System.out::println);
    }

    @Test
    void findNewsByClassifyName1(){
        PageHelper.startPage(1, 50);
        PageInfo<News> page = new PageInfo<News>(newsMapper.findNewsByClassifyName1("CBA"));
        List<News> list = page.getList();
        list.stream().forEach(System.out::println);
    }

    @Test
    void insertNews(){
        News news = new News();
        User user = new User();
        user.setUserId(101L);
        Classify classify = new Classify();
        classify.setClassifyId(101001L);
        news.setUser(user);
        news.setClassify(classify);
        news.setNewName("my");
        news.setNewDescribe("name");
        news.setNewWord("is one");
        news.setNewCommentsum(10);
        news.setNewViewsum(10);
        news.setNewOpposesum(0);
        news.setNewSupportsum(10);
        newsMapper.insertNews(news);
    }

    @Test
    void deleteNews(){
        newsMapper.deleteNews(105L);
    }

    @Test
    void updateNews(){
        News news = new News();
        User user = new User();
        user.setUserId(101L);
        Classify classify = new Classify();
        classify.setClassifyId(105L);
        news.setNewId(106L);
        news.setUser(user);
        news.setClassify(classify);
        news.setNewName("qqq");
        news.setNewDescribe("www");
        news.setNewWord("is eee");
        news.setNewCommentsum(10);
        news.setNewViewsum(10);
        news.setNewOpposesum(0);
        news.setNewSupportsum(10);
        newsMapper.updateNews(news);
    }
}