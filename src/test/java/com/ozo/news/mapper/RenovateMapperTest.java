package com.ozo.news.mapper;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.News;
import com.ozo.news.entity.Renovate;
import com.ozo.news.entity.User;
import com.ozo.news.entity.Verify;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class RenovateMapperTest {

    @Autowired
    RenovateMapper renovateMapper;

    @Test
    void findAllRenovate() {
        renovateMapper.findAllRenovate().forEach(System.out::println);
    }

    @Test
    void findRenovateByCondition(){
        Renovate renovate = new Renovate();
        renovate.setRenovateId(102L);
        /*User user = new User();
        user.setUserId(101L);
        renovate.setUser(user);*/
        /*News news = new News();
        news.setNewId(102L);
        renovate.setNews(news);*/
        //分页信息
        PageHelper.startPage(1, 3);
        //执行分页查询
        PageInfo<Renovate> page = new PageInfo<Renovate>(renovateMapper.findRenovateByCondition(renovate));
        //打印用户列表
        List<Renovate> list = page.getList();
        list.stream().forEach(System.out::println);
    }

    @Test
    void insertRenovate(){
        Renovate renovate = new Renovate();
        User user = new User();
        user.setUserId(102L);
        News news = new News();
        news.setNewId(102L);
        renovate.setNews(news);
        renovate.setUser(user);
        renovateMapper.insertRenovate(renovate);
    }

    @Test
    void deleteRenovate(){
        renovateMapper.deleteRenovate(104L);
    }

    @Test
    void updateRenovate(){
        Renovate renovate = new Renovate();
        User user = new User();
        user.setUserId(102L);
        News news = new News();
        news.setNewId(102L);
        renovate.setUser(user);
        renovate.setNews(news);
        renovate.setRenovateId(103L);
        renovateMapper.updateRenovate(renovate);
    }
}