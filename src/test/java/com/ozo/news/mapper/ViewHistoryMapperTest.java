package com.ozo.news.mapper;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.News;
import com.ozo.news.entity.SupportHistory;
import com.ozo.news.entity.User;
import com.ozo.news.entity.ViewHistory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ViewHistoryMapperTest {

    @Autowired
    ViewHistoryMapper viewHistoryMapper;

    @Test
    void findAllViewHistory() {
        viewHistoryMapper.findAllViewHistory().forEach(System.out::println);
    }

    @Test
    void findViewHistoryByCondition(){
        ViewHistory viewHistory = new ViewHistory();
        //viewHistory.setViewId(102L);
        User user = new User();
        user.setUserId(301L);
        viewHistory.setUser(user);
        /*News news = new News();
        news.setNewId(102L);
        viewHistory.setNews(news);*/
        PageHelper.startPage(1, 3);
        PageInfo<ViewHistory> page = new PageInfo<ViewHistory>(viewHistoryMapper.findViewHistoryByCondition(viewHistory));
        List<ViewHistory> list = page.getList();
        list.stream().forEach(System.out::println);
    }

    @Test
    void findViewHistoryByParam(){
        PageHelper.startPage(1, 50);
        PageInfo<ViewHistory> page = new PageInfo<ViewHistory>(viewHistoryMapper.findViewHistoryByParam("1","1"));
        List<ViewHistory> list = page.getList();
        list.stream().forEach(System.out::println);
    }

    @Test
    void insertViewHistory(){
        ViewHistory viewHistory = new ViewHistory();
        User user = new User();
        user.setUserId(401L);
        News news = new News();
        news.setNewId(104L);
        viewHistory.setNews(news);
        viewHistory.setUser(user);
        viewHistoryMapper.insertViewHistory(viewHistory);
    }

    @Test
    void deleteViewHistory(){
        viewHistoryMapper.deleteViewHistory(107L);
    }
}