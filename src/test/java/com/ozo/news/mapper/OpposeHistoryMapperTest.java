package com.ozo.news.mapper;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.News;
import com.ozo.news.entity.OpposeHistory;
import com.ozo.news.entity.SupportHistory;
import com.ozo.news.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class OpposeHistoryMapperTest {

    @Autowired
    OpposeHistoryMapper opposeHistoryMapper;

    @Test
    void findAllOpposeHistory() {
        opposeHistoryMapper.findAllOpposeHistory().forEach(System.out::println);
    }

    @Test
    void findOpposeHistoryByCondition(){
        OpposeHistory opposeHistory = new OpposeHistory();
        //根据opposeId查询
        //opposeHistory.setOpposeId(101L);
        //根据userId查询
        User user = new User();
        user.setUserId(301L);
        opposeHistory.setUser(user);
        //根据newId查询
        /*News news = new News();
        news.setNewId(102L);
        opposeHistory.setNews(news);*/
        PageHelper.startPage(1, 3);
        PageInfo<OpposeHistory> page = new PageInfo<OpposeHistory>(opposeHistoryMapper.findOpposeHistoryByCondition(opposeHistory));
        List<OpposeHistory> list = page.getList();
        list.stream().forEach(System.out::println);
    }

    @Test
    void findOpposeHistoryByParam(){
        PageHelper.startPage(13, 8);
        PageInfo<OpposeHistory> page = new PageInfo<OpposeHistory>(opposeHistoryMapper.findOpposeHistoryByParam("",""));
        List<OpposeHistory> list = page.getList();
        list.stream().forEach(System.out::println);
    }

    @Test
    void insertOpposeHistory(){
        OpposeHistory opposeHistory = new OpposeHistory();
        User user = new User();
        user.setUserId(205L);
        News news = new News();
        news.setNewId(105L);
        opposeHistory.setUser(user);
        opposeHistory.setNews(news);
        opposeHistoryMapper.insertOpposeHistory(opposeHistory);
    }

    @Test
    void deleteOpposeHistory(){
        opposeHistoryMapper.deleteOpposeHistory(102L);
    }

    @Test
    void updateOpposeHistory(){
        OpposeHistory opposeHistory = new OpposeHistory();
        User user = new User();
        user.setUserId(204L);
        News news = new News();
        news.setNewId(104L);
        opposeHistory.setOpposeId(104L);
        opposeHistory.setNews(news);
        opposeHistory.setUser(user);
        opposeHistoryMapper.updateOpposeHistory(opposeHistory);
    }
}