/*
 Navicat Premium Data Transfer

 Source Server         : ozo1
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : news

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 06/05/2021 02:41:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for classify
-- ----------------------------
DROP TABLE IF EXISTS `classify`;
CREATE TABLE `classify`  (
  `classify_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `classify_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `parent_id` bigint(32) NULL DEFAULT NULL COMMENT '父分类id',
  `classify_level` int(11) NULL DEFAULT NULL COMMENT '分类等级',
  PRIMARY KEY (`classify_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103010 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '新闻分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of classify
-- ----------------------------
INSERT INTO `classify` VALUES (101, '篮球', NULL, 1);
INSERT INTO `classify` VALUES (102, '足球', NULL, 1);
INSERT INTO `classify` VALUES (103, '综合体育', NULL, 1);
INSERT INTO `classify` VALUES (101001, 'NBA', 101, 2);
INSERT INTO `classify` VALUES (101002, 'CBA', 101, 2);
INSERT INTO `classify` VALUES (102001, '欧冠', 102, 2);
INSERT INTO `classify` VALUES (102002, '西甲', 102, 2);
INSERT INTO `classify` VALUES (102003, '英超', 102, 2);
INSERT INTO `classify` VALUES (102004, '德甲', 102, 2);
INSERT INTO `classify` VALUES (102005, '意甲', 102, 2);
INSERT INTO `classify` VALUES (102006, '中超', 102, 2);
INSERT INTO `classify` VALUES (102007, '国足', 102, 2);
INSERT INTO `classify` VALUES (103001, '网球', 103, 2);
INSERT INTO `classify` VALUES (103002, '排球', 103, 2);
INSERT INTO `classify` VALUES (103003, '电竞', 103, 2);
INSERT INTO `classify` VALUES (103004, '冬奥', 103, 2);
INSERT INTO `classify` VALUES (103005, '跑步', 103, 2);
INSERT INTO `classify` VALUES (103006, '游泳跳水', 103, 2);
INSERT INTO `classify` VALUES (103007, '台球', 103, 2);
INSERT INTO `classify` VALUES (103008, '拳击', 103, 2);
INSERT INTO `classify` VALUES (103009, '乒乓球', 103, 2);

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `comment_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '评论id',
  `user_id` bigint(32) NULL DEFAULT NULL COMMENT '评论用户id',
  `new_id` bigint(32) NULL DEFAULT NULL COMMENT '评论新闻id',
  `comment_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '评论内容',
  `comment_time` datetime(0) NULL DEFAULT NULL COMMENT '评论时间',
  PRIMARY KEY (`comment_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 159 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '新闻评论' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES (101, 101, 1001, '马竞必胜！！！', '2021-04-24 19:58:52');
INSERT INTO `comment` VALUES (102, 201, 1001, '看韦斯卡这支升班马的新教练帕切塔（Pacheta）如何带领球队走向胜利', '2021-03-29 12:15:57');
INSERT INTO `comment` VALUES (103, 301, 1002, '因为北京首钢四年三冠。广东队员包括球迷被打出永远抹不去的阴影。', '2021-03-29 13:16:47');
INSERT INTO `comment` VALUES (104, 301, 1003, '尤文无了，拜仁加油！！', '2021-03-29 20:19:16');
INSERT INTO `comment` VALUES (105, 201, 1002, '广厦站起来了呀！！！', '2021-04-24 20:07:43');
INSERT INTO `comment` VALUES (106, 201, 1003, '看皇马能否冲出重围。', '2021-04-24 20:08:49');
INSERT INTO `comment` VALUES (107, 301, 1001, '期待马德里竞技将面对塞维利亚的这场硬仗能打的精彩一点。', '2021-04-24 20:10:57');
INSERT INTO `comment` VALUES (108, 4512566170025984, 1004, '穆帅又一次被”淘汰“了', '2021-04-24 20:12:18');
INSERT INTO `comment` VALUES (109, 4512566170025984, 1007, '恭喜马内完成里程碑，向下一个里程碑迈进。', '2021-04-24 20:13:12');
INSERT INTO `comment` VALUES (110, 4512566170025984, 1008, '万众期待的新一年中超终于要开幕了。', '2021-04-24 20:13:58');
INSERT INTO `comment` VALUES (111, 4512566170025984, 1003, '今年又会是谁能捧起大耳朵杯呢？', '2021-04-24 20:15:05');
INSERT INTO `comment` VALUES (112, 5744592866844332, 1009, '三球加油！！未来是你的', '2021-04-24 20:15:29');
INSERT INTO `comment` VALUES (113, 5744592866844332, 1010, '库里永远的神！！！不可思议的奇迹。', '2021-04-24 20:16:03');
INSERT INTO `comment` VALUES (114, 5744592866844332, 1011, '好久才能看到一个完整的篮网呢？', '2021-04-24 20:16:33');
INSERT INTO `comment` VALUES (115, 5746686520543108, 1014, '可惜了，要不是穆雷赛季报销，掘金今年的机会还是很大的。', '2021-04-24 20:17:42');
INSERT INTO `comment` VALUES (116, 5746686520543108, 1017, '今年CBA的每一个队伍都不能小觑呀。', '2021-04-24 20:18:31');
INSERT INTO `comment` VALUES (117, 5746686520543108, 1019, '希望冯欣不会让大家失望', '2021-04-24 20:19:39');
INSERT INTO `comment` VALUES (118, 5746686520543108, 1020, '广厦你们是好样的，虽然比赛输了，但是你们的努力我们有目共睹。', '2021-04-24 20:20:44');
INSERT INTO `comment` VALUES (119, 5746687128004140, 1021, '周琦的下一站会是哪里呢？  期待', '2021-04-24 20:21:41');
INSERT INTO `comment` VALUES (120, 5746687128004140, 1022, '周琦去了广东的话那不就是冠军提前当选了？', '2021-04-24 20:22:38');
INSERT INTO `comment` VALUES (121, 5746719496328924, 1023, '武磊加油呀，你是中国足球的希望。', '2021-04-24 20:23:28');
INSERT INTO `comment` VALUES (122, 5746719496328924, 1024, '欧洲超级联赛是否能”活下去“呢？让我们拭目以待', '2021-04-24 20:24:18');
INSERT INTO `comment` VALUES (123, 5746719496328924, 1026, '卡纳瓦罗能否带领恒大重铸荣光呢、', '2021-04-24 20:26:35');
INSERT INTO `comment` VALUES (124, 5746719496328924, 1025, '欧洲超级联赛会不会变成一个“小丑”项目呢？真期待最后的结果', '2021-04-24 20:27:41');
INSERT INTO `comment` VALUES (125, 5747184000103856, 1002, '胡金秋真厉害呀。', '2021-04-24 20:28:44');
INSERT INTO `comment` VALUES (126, 5747184000103856, 1080, '强强对话。看谁能冲出重围呢？', '2021-04-24 20:29:35');
INSERT INTO `comment` VALUES (127, 5747184000103857, 1048, '龚翔宇加油，你是中国排球的另一个希望。', '2021-04-24 20:30:50');
INSERT INTO `comment` VALUES (128, 5747184000103857, 1049, '天津女排下赛季能够再次起飞，就看这个休赛期的引援了。', '2021-04-24 20:46:39');
INSERT INTO `comment` VALUES (129, 5769745737743028, 1042, '约书亚能否起飞就看梅威瑟的了', '2021-04-24 20:49:18');
INSERT INTO `comment` VALUES (130, 5769745737743028, 1040, '到底是洛佩兹赢比赛后的胡言乱语还是真就把洛马琴科打的精神不正常了呢？这值得深究一下', '2021-04-24 20:50:51');
INSERT INTO `comment` VALUES (131, 5769745737743030, 1037, '希望梅西真的会终老于巴萨', '2021-04-24 20:52:16');
INSERT INTO `comment` VALUES (132, 5769745737743030, 1035, '重庆加油呀！！', '2021-04-24 20:52:36');
INSERT INTO `comment` VALUES (133, 5769745737743035, 1031, '如果今年中超没了降级，那中甲的球队怎么办呢', '2021-04-24 20:53:50');
INSERT INTO `comment` VALUES (134, 5769745737743035, 1001, '皇马，巴萨的每一场比赛都将影响能否继续保持冲冠能力', '2021-04-24 20:55:34');
INSERT INTO `comment` VALUES (135, 5769745737743036, 1043, '丁俊晖加油，我相信你一定能恢复以前的状态', '2021-04-24 20:56:54');
INSERT INTO `comment` VALUES (136, 5769745737743036, 1044, '丁俊晖被淘汰了，就看火箭的比赛了，冲', '2021-04-24 20:57:46');
INSERT INTO `comment` VALUES (137, 5769745737743037, 1050, '缺了中美的世锦赛还有什么看点呢？果断不看了', '2021-04-24 20:59:20');
INSERT INTO `comment` VALUES (138, 5769745737743037, 1051, '800米，1分57秒73，真厉害呀', '2021-04-24 20:59:53');
INSERT INTO `comment` VALUES (139, 5769745737743037, 1052, '冲，让我们大家积极参加马拉松', '2021-04-24 21:00:18');
INSERT INTO `comment` VALUES (140, 5769745737743038, 1053, '在我的青春时期，王涵跳水是真的厉害', '2021-04-24 21:01:13');
INSERT INTO `comment` VALUES (141, 5769745737743038, 1054, '在南极完成铁三，我真的是想都不敢想', '2021-04-24 21:01:49');
INSERT INTO `comment` VALUES (142, 5769745737743039, 1057, 'QGhappy加油，冲出B组', '2021-04-24 21:02:53');
INSERT INTO `comment` VALUES (143, 5769745737743039, 1058, '希望Tabe能够顺利去往冰岛', '2021-04-24 21:03:33');
INSERT INTO `comment` VALUES (144, 5769745737743040, 1069, '给中国女足来个好签。', '2021-04-24 21:32:15');
INSERT INTO `comment` VALUES (145, 5769745737743040, 1070, '中国女足加油！！', '2021-04-24 21:32:23');
INSERT INTO `comment` VALUES (146, 5769745737743041, 1061, '这个东奥场馆也太还看了吧', '2021-04-24 21:33:03');
INSERT INTO `comment` VALUES (147, 5769745737743041, 1064, '为了能举办一个完美的东奥，中国冲压', '2021-04-24 21:33:39');
INSERT INTO `comment` VALUES (148, 5769745737743042, 1067, '今年的蒂姆真强', '2021-04-24 21:34:13');
INSERT INTO `comment` VALUES (149, 5769745737743042, 1068, '期待法网正赛', '2021-04-24 21:35:04');
INSERT INTO `comment` VALUES (150, 5775646896559288, 1076, '樊振东陈梦孙颖莎为了奥运，加油！！', '2021-04-24 21:36:54');
INSERT INTO `comment` VALUES (151, 5775673578120840, 1074, '池江真厉害呀！受伤回来后状态还是如此爆炸', '2021-04-24 21:37:31');
INSERT INTO `comment` VALUES (152, 5775674380365248, 1077, '国乒加油！', '2021-04-24 21:37:51');
INSERT INTO `comment` VALUES (156, 101, 1013, '得分王！厉害！', '2021-05-05 05:02:46');
INSERT INTO `comment` VALUES (157, 101, 1054, '厉害呀', '2021-05-05 05:03:22');
INSERT INTO `comment` VALUES (158, 5775646896559288, 1009, '加油加油', '2021-05-05 15:46:20');

-- ----------------------------
-- Table structure for image
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image`  (
  `image_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '图片id',
  `image_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `connect_id` bigint(32) NULL DEFAULT NULL COMMENT '关联id',
  `image_url` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片url',
  PRIMARY KEY (`image_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 119 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '新闻图片' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of image
-- ----------------------------
INSERT INTO `image` VALUES (1, '34dfc4ccd1844e6ea17bdc315edb0163.jpg', 101, 'image/34dfc4ccd1844e6ea17bdc315edb0163.jpg');
INSERT INTO `image` VALUES (13, '4618fb37b44c470c93f4ebdfd520272a.png', 201, 'image/4618fb37b44c470c93f4ebdfd520272a.png');
INSERT INTO `image` VALUES (16, '20c8773251854e84b75ed6e79a574710.png', 1001, 'image/20c8773251854e84b75ed6e79a574710.png');
INSERT INTO `image` VALUES (40, '79694e2c9cbc4f5f8456a7edb7b69de3.jpeg', 1002, 'image/79694e2c9cbc4f5f8456a7edb7b69de3.jpeg');
INSERT INTO `image` VALUES (41, '83d5132e663749b3bdc4f20a15563cc1.jpeg', 1003, 'image/83d5132e663749b3bdc4f20a15563cc1.jpeg');
INSERT INTO `image` VALUES (42, 'ef80437ba7ee4bce9bedace248dde6fa.jpg', 1004, 'image/ef80437ba7ee4bce9bedace248dde6fa.jpg');
INSERT INTO `image` VALUES (43, '4010ccf6df48421b949e604651e46a01.jpg', 1006, 'image/4010ccf6df48421b949e604651e46a01.jpg');
INSERT INTO `image` VALUES (44, 'ee2ea2fb5ac3471ea8d2e9362d376fba.jpg', 1005, 'image/ee2ea2fb5ac3471ea8d2e9362d376fba.jpg');
INSERT INTO `image` VALUES (45, '2f0f0157ce5c45818aad82c17669e263.webp', 1007, 'image/2f0f0157ce5c45818aad82c17669e263.webp');
INSERT INTO `image` VALUES (46, '80d30a5eb04b4c56b1cf3ecd74385465.jpg', 1008, 'image/80d30a5eb04b4c56b1cf3ecd74385465.jpg');
INSERT INTO `image` VALUES (47, '78a2eea5b087421eb563cfaec3092e36.jpg', 1009, 'image/78a2eea5b087421eb563cfaec3092e36.jpg');
INSERT INTO `image` VALUES (48, 'c4e1a017c8a34d2d9e4814ebcacabfdd.png', 1010, 'image/c4e1a017c8a34d2d9e4814ebcacabfdd.png');
INSERT INTO `image` VALUES (49, '984671ea928f4e14b8edf25c638f0fec.jpg', 1012, 'image/984671ea928f4e14b8edf25c638f0fec.jpg');
INSERT INTO `image` VALUES (50, 'f1872468eb8b478895c74d3bd7ab4987.jpeg', 1013, 'image/f1872468eb8b478895c74d3bd7ab4987.jpeg');
INSERT INTO `image` VALUES (51, '4e6950c479c6412084f019d8d7c67cf2.jpg', 1015, 'image/4e6950c479c6412084f019d8d7c67cf2.jpg');
INSERT INTO `image` VALUES (52, '3eec48b20f384d078c79518338a26c72.jpg', 1016, 'image/3eec48b20f384d078c79518338a26c72.jpg');
INSERT INTO `image` VALUES (53, '0f86455b50d24d19947bbe448deabae1.jpg', 1017, 'image/0f86455b50d24d19947bbe448deabae1.jpg');
INSERT INTO `image` VALUES (54, '4ec229feef7049268a059afd97edd3b3.jpeg', 1011, 'image/4ec229feef7049268a059afd97edd3b3.jpeg');
INSERT INTO `image` VALUES (55, 'cf6b3fa147ad405eb6aacd9bd710dfc2.png', 1014, 'image/cf6b3fa147ad405eb6aacd9bd710dfc2.png');
INSERT INTO `image` VALUES (56, '71ac028fb04b474d8cd3f0d65937c622.png', 1018, 'image/71ac028fb04b474d8cd3f0d65937c622.png');
INSERT INTO `image` VALUES (57, 'e6c8c0424d90459585e86d4341e3b68a.jpg', 1019, 'image/e6c8c0424d90459585e86d4341e3b68a.jpg');
INSERT INTO `image` VALUES (58, 'a172d7518d9244ce8e1b40099d348a3d.png', 1020, 'image/a172d7518d9244ce8e1b40099d348a3d.png');
INSERT INTO `image` VALUES (59, '211fd882125546f9aa19bae83ef2866f.jpeg', 1021, 'image/211fd882125546f9aa19bae83ef2866f.jpeg');
INSERT INTO `image` VALUES (60, 'e697fdb95027449ba114c9e89ad6c1cb.jpg', 1022, 'image/e697fdb95027449ba114c9e89ad6c1cb.jpg');
INSERT INTO `image` VALUES (61, '6e33da58bafd439cb92b8b33237f3d2c.jpeg', 1023, 'image/6e33da58bafd439cb92b8b33237f3d2c.jpeg');
INSERT INTO `image` VALUES (62, '3b35b2c89f83421ebff19c020f08b75c.webp', 1024, 'image/3b35b2c89f83421ebff19c020f08b75c.webp');
INSERT INTO `image` VALUES (63, '264fc46d540f4d958c0ae18069a9fabf.jpeg', 1025, 'image/264fc46d540f4d958c0ae18069a9fabf.jpeg');
INSERT INTO `image` VALUES (64, '89c6b327c76c4572856c12ff61565247.png', 1026, 'image/89c6b327c76c4572856c12ff61565247.png');
INSERT INTO `image` VALUES (65, 'ee5d2b79a72e41b7916fd3f03cb2c92e.webp', 1027, 'image/ee5d2b79a72e41b7916fd3f03cb2c92e.webp');
INSERT INTO `image` VALUES (66, 'a468dbcd737e407194ea824aec6f4d54.jpeg', 1028, 'image/a468dbcd737e407194ea824aec6f4d54.jpeg');
INSERT INTO `image` VALUES (67, 'cd77c30f1e2b4b73a7c2b44c5760d199.webp', 1029, 'image/cd77c30f1e2b4b73a7c2b44c5760d199.webp');
INSERT INTO `image` VALUES (68, '325335de11aa4361810be91f9d2b8fbe.webp', 1030, 'image/325335de11aa4361810be91f9d2b8fbe.webp');
INSERT INTO `image` VALUES (69, '94c3a104d4064da6bf1dc4008b198c4a.jpg', 1031, 'image/94c3a104d4064da6bf1dc4008b198c4a.jpg');
INSERT INTO `image` VALUES (70, '833096b5f7d3494480883c59698f52c7.jpg', 1032, 'image/833096b5f7d3494480883c59698f52c7.jpg');
INSERT INTO `image` VALUES (71, '14e86c5fadc942dbbfbd07b92f0dfad5.jpeg', 1033, 'image/14e86c5fadc942dbbfbd07b92f0dfad5.jpeg');
INSERT INTO `image` VALUES (72, '563a7c6358ac4ea1899bd39ea1e180b2.jpeg', 1034, 'image/563a7c6358ac4ea1899bd39ea1e180b2.jpeg');
INSERT INTO `image` VALUES (73, 'a6aae46f5fcd4c47b3db5ea69dcda0aa.webp', 1035, 'image/a6aae46f5fcd4c47b3db5ea69dcda0aa.webp');
INSERT INTO `image` VALUES (74, 'aa572ba1bdee4903ac7be83be3072ae4.png', 1036, 'image/aa572ba1bdee4903ac7be83be3072ae4.png');
INSERT INTO `image` VALUES (75, 'cf87860ed6674177b014f353c30381cd.jpeg', 1037, 'image/cf87860ed6674177b014f353c30381cd.jpeg');
INSERT INTO `image` VALUES (76, '993e639f9a4c4dc7bb78da1890acf107.webp', 1038, 'image/993e639f9a4c4dc7bb78da1890acf107.webp');
INSERT INTO `image` VALUES (77, '50da4c74b64e427897bfabd45daf978c.jpg', 1039, 'image/50da4c74b64e427897bfabd45daf978c.jpg');
INSERT INTO `image` VALUES (78, 'a425be3c6cd84f17841a6bf2dc2e540a.webp', 1040, 'image/a425be3c6cd84f17841a6bf2dc2e540a.webp');
INSERT INTO `image` VALUES (79, '99213504d72844a8b3da996367e31ce1.jpeg', 1041, 'image/99213504d72844a8b3da996367e31ce1.jpeg');
INSERT INTO `image` VALUES (80, 'a8e95fc9865c4f8a85eb52a0868bd1e2.jpeg', 1042, 'image/a8e95fc9865c4f8a85eb52a0868bd1e2.jpeg');
INSERT INTO `image` VALUES (81, '2d1c40f39ef34f45b752b8231e0ea0ef.webp', 1043, 'image/2d1c40f39ef34f45b752b8231e0ea0ef.webp');
INSERT INTO `image` VALUES (82, '46dae3b2d2754bada53d4ac874eb9e20.jpg', 1044, 'image/46dae3b2d2754bada53d4ac874eb9e20.jpg');
INSERT INTO `image` VALUES (83, '20761bc7f0594df3a751964380d5a18a.webp', 1045, 'image/20761bc7f0594df3a751964380d5a18a.webp');
INSERT INTO `image` VALUES (84, '2a09c89682534eac803c6fd19d067293.webp', 1046, 'image/2a09c89682534eac803c6fd19d067293.webp');
INSERT INTO `image` VALUES (85, '61eb2246ee124d719d869cd96cb7ff86.webp', 1047, 'image/61eb2246ee124d719d869cd96cb7ff86.webp');
INSERT INTO `image` VALUES (86, '3115d2633593445d8eee0cc541a51944.webp', 1048, 'image/3115d2633593445d8eee0cc541a51944.webp');
INSERT INTO `image` VALUES (87, '1f5efbdd3de94e7a9d813e38a2d8b4d0.webp', 1049, 'image/1f5efbdd3de94e7a9d813e38a2d8b4d0.webp');
INSERT INTO `image` VALUES (88, 'b23fbdb04d104e5a8189a3eb2b2d78c3.png', 1050, 'image/b23fbdb04d104e5a8189a3eb2b2d78c3.png');
INSERT INTO `image` VALUES (89, '775008d071bd49778c7bcbc05ebd668b.jpeg', 1051, 'image/775008d071bd49778c7bcbc05ebd668b.jpeg');
INSERT INTO `image` VALUES (90, 'b91ca66510a34159ad69742a2442c9f1.jpg', 1052, 'image/b91ca66510a34159ad69742a2442c9f1.jpg');
INSERT INTO `image` VALUES (91, '4f37a951092043c38bc5265ec53af6a5.jpg', 1053, 'image/4f37a951092043c38bc5265ec53af6a5.jpg');
INSERT INTO `image` VALUES (92, '2df3f5f280d74e4ba826398628f46ecb.webp', 1054, 'image/2df3f5f280d74e4ba826398628f46ecb.webp');
INSERT INTO `image` VALUES (93, 'e7169abb6e2d4cf2a9c89183719f6e6c.webp', 1055, 'image/e7169abb6e2d4cf2a9c89183719f6e6c.webp');
INSERT INTO `image` VALUES (94, 'f360f2ebcfbd4eec95db6e38e1c068c9.jpeg', 1056, 'image/f360f2ebcfbd4eec95db6e38e1c068c9.jpeg');
INSERT INTO `image` VALUES (95, 'ef983efe33264bc38691969c909a20ea.jpg', 1057, 'image/ef983efe33264bc38691969c909a20ea.jpg');
INSERT INTO `image` VALUES (96, 'df029fd04286407ab0b493ee0a196d22.webp', 1058, 'image/df029fd04286407ab0b493ee0a196d22.webp');
INSERT INTO `image` VALUES (97, '10b4192a5f844d599c146978b859cccf.webp', 1059, 'image/10b4192a5f844d599c146978b859cccf.webp');
INSERT INTO `image` VALUES (98, '880f55f216d340f8bd775487a1e795a3.jpg', 1060, 'image/880f55f216d340f8bd775487a1e795a3.jpg');
INSERT INTO `image` VALUES (99, 'ed78c7cd64d84dcf92eb12c75473f49e.png', 1061, 'image/ed78c7cd64d84dcf92eb12c75473f49e.png');
INSERT INTO `image` VALUES (100, 'af46f1d195d1498b9205d61f474786a6.webp', 1062, 'image/af46f1d195d1498b9205d61f474786a6.webp');
INSERT INTO `image` VALUES (101, '42d6de3cd3de492986c0fb62fc8a840e.png', 1063, 'image/42d6de3cd3de492986c0fb62fc8a840e.png');
INSERT INTO `image` VALUES (102, 'a04d6cd154f248eba23e26ec8c71cdd3.jpg', 1064, 'image/a04d6cd154f248eba23e26ec8c71cdd3.jpg');
INSERT INTO `image` VALUES (103, '3620768a988a4186a2c5c04d232b5a0c.webp', 1065, 'image/3620768a988a4186a2c5c04d232b5a0c.webp');
INSERT INTO `image` VALUES (104, '5eacfacd8f064395922c8fe76f2093a6.webp', 1066, 'image/5eacfacd8f064395922c8fe76f2093a6.webp');
INSERT INTO `image` VALUES (105, '8aa46887492643adbaff73e9244eca40.jpeg', 1067, 'image/8aa46887492643adbaff73e9244eca40.jpeg');
INSERT INTO `image` VALUES (106, 'be2c2d1531f847d79597afcab5eb1e43.jpg', 1068, 'image/be2c2d1531f847d79597afcab5eb1e43.jpg');
INSERT INTO `image` VALUES (107, 'ba27065f666d4859aa54f654e574b727.jpg', 1069, 'image/ba27065f666d4859aa54f654e574b727.jpg');
INSERT INTO `image` VALUES (108, 'f7c0d17728914885977d5ea4f6266890.jpg', 1070, 'image/f7c0d17728914885977d5ea4f6266890.jpg');
INSERT INTO `image` VALUES (109, '85bff36c1b544b45ae5e3d2068bd43c6.jpg', 1071, 'image/85bff36c1b544b45ae5e3d2068bd43c6.jpg');
INSERT INTO `image` VALUES (110, '07c05d42bb7a4da891a94f1a0e10af84.webp', 1072, 'image/07c05d42bb7a4da891a94f1a0e10af84.webp');
INSERT INTO `image` VALUES (111, 'bf0fc02bdb8b4672a061b64f221e952b.', 1073, 'image/bf0fc02bdb8b4672a061b64f221e952b.');
INSERT INTO `image` VALUES (112, '24cf7bdc74eb4a05b7471b57bd143642.jpg', 1074, 'image/24cf7bdc74eb4a05b7471b57bd143642.jpg');
INSERT INTO `image` VALUES (113, '4a8725f0cc8b4f2885f75da127c55b94.jpeg', 1075, 'image/4a8725f0cc8b4f2885f75da127c55b94.jpeg');
INSERT INTO `image` VALUES (114, '9536bea60cec46c38a9cdae4eb29544f.png', 1076, 'image/9536bea60cec46c38a9cdae4eb29544f.png');
INSERT INTO `image` VALUES (115, '8a4025011d084816adafb79d28dfb441.jpg', 1077, 'image/8a4025011d084816adafb79d28dfb441.jpg');
INSERT INTO `image` VALUES (116, '106c7aca82a64ddfa9a4bb1f92ae09fe.jpg', 1078, 'image/106c7aca82a64ddfa9a4bb1f92ae09fe.jpg');
INSERT INTO `image` VALUES (117, '42a00c3b875a4ef3a96a74d919b4b7d0.jpg', 1079, 'image/42a00c3b875a4ef3a96a74d919b4b7d0.jpg');
INSERT INTO `image` VALUES (118, 'cca6bf9b9e9741fead1d4b8db0fbf911.jpeg', 1080, 'image/cca6bf9b9e9741fead1d4b8db0fbf911.jpeg');

-- ----------------------------
-- Table structure for interest
-- ----------------------------
DROP TABLE IF EXISTS `interest`;
CREATE TABLE `interest`  (
  `interest_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '兴趣id',
  `interest_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '兴趣名称',
  `user_id` bigint(32) NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`interest_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 157 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '兴趣' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of interest
-- ----------------------------
INSERT INTO `interest` VALUES (101, '篮球', 301);
INSERT INTO `interest` VALUES (102, '足球', 301);
INSERT INTO `interest` VALUES (103, '排球', 201);
INSERT INTO `interest` VALUES (104, 'CBA', 4512566170025984);
INSERT INTO `interest` VALUES (105, '欧冠', 4512566170025984);
INSERT INTO `interest` VALUES (106, '乒乓球', 5744592866844332);
INSERT INTO `interest` VALUES (107, '网球', 5744592866844332);
INSERT INTO `interest` VALUES (111, '游泳跳水', 5747184000103856);
INSERT INTO `interest` VALUES (112, 'NBA', 5747184000103857);
INSERT INTO `interest` VALUES (113, '西甲', 5769745737743028);
INSERT INTO `interest` VALUES (114, '意甲', 5769745737743037);
INSERT INTO `interest` VALUES (115, '国足', 5769745737743036);
INSERT INTO `interest` VALUES (116, '拳击', 5769745737743037);
INSERT INTO `interest` VALUES (117, '台球', 5769745737743037);
INSERT INTO `interest` VALUES (118, '电竞', 5769745737743038);
INSERT INTO `interest` VALUES (119, '冬奥', 5769745737743038);
INSERT INTO `interest` VALUES (120, '跑步', 5769745737743038);
INSERT INTO `interest` VALUES (121, 'NBA', 5769745737743039);
INSERT INTO `interest` VALUES (122, '国足', 5769745737743040);
INSERT INTO `interest` VALUES (123, '中超', 5769745737743041);
INSERT INTO `interest` VALUES (124, '英超', 5769745737743042);
INSERT INTO `interest` VALUES (125, '德甲', 5775646896559288);
INSERT INTO `interest` VALUES (126, 'NBA', 5775673578120840);
INSERT INTO `interest` VALUES (127, 'CBA', 5775673578120840);
INSERT INTO `interest` VALUES (128, '欧冠', 5775674380365248);
INSERT INTO `interest` VALUES (129, '西甲', 5775674380365248);
INSERT INTO `interest` VALUES (130, 'NBA', 101);
INSERT INTO `interest` VALUES (131, '乒乓球', 5746686520543108);
INSERT INTO `interest` VALUES (132, '网球', 5746687128004140);
INSERT INTO `interest` VALUES (133, '欧冠', 5746719496328924);
INSERT INTO `interest` VALUES (134, '西甲', 5769745737743030);
INSERT INTO `interest` VALUES (135, '电竞', 5769745737743035);
INSERT INTO `interest` VALUES (136, '台球', 5769745737743036);
INSERT INTO `interest` VALUES (138, 'CBA', 101);
INSERT INTO `interest` VALUES (139, '德甲', 5744592866844332);
INSERT INTO `interest` VALUES (140, '意甲', 5744592866844332);
INSERT INTO `interest` VALUES (141, 'CBA', 5746686520543108);
INSERT INTO `interest` VALUES (142, '欧冠', 5746686520543108);
INSERT INTO `interest` VALUES (143, '欧冠', 5747184000103856);
INSERT INTO `interest` VALUES (144, '西甲', 5747184000103856);
INSERT INTO `interest` VALUES (148, '意甲', 5747184000103856);
INSERT INTO `interest` VALUES (151, '德甲', 5747184000103856);
INSERT INTO `interest` VALUES (152, '中超', 5747184000103856);
INSERT INTO `interest` VALUES (153, '西甲', 201);
INSERT INTO `interest` VALUES (154, '英超', 201);
INSERT INTO `interest` VALUES (155, '德甲', 201);
INSERT INTO `interest` VALUES (158, '西甲', 101);
INSERT INTO `interest` VALUES (163, 'NBA', 4589832767995904);
INSERT INTO `interest` VALUES (164, '西甲', 4589861089738752);
INSERT INTO `interest` VALUES (165, '电竞', 4589925157273600);

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news`  (
  `new_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '新闻id',
  `new_name` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '新闻名称',
  `new_describe` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '新闻描述',
  `classify_id` bigint(32) NULL DEFAULT NULL COMMENT '新闻分类',
  `new_word` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '新闻文字',
  `new_commentsum` int(11) NULL DEFAULT NULL COMMENT '评论数量',
  `new_viewsum` int(11) NULL DEFAULT NULL COMMENT '浏览次数',
  `new_supportsum` int(11) NULL DEFAULT NULL COMMENT '点赞',
  `new_opposesum` int(11) NULL DEFAULT NULL COMMENT '踩',
  `user_id` bigint(32) NULL DEFAULT NULL COMMENT '上传用户',
  `new_create_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '上传时间',
  `new_update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`new_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4578663976173569 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '新闻' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES (1001, '西甲第29轮前瞻：榜首马竞再遇硬仗，巴斯克德比连续上演', '三月份的国际比赛日已经结束，现在西班牙各方的焦点又回到国内足坛，本赛季西甲联赛第29轮即将在本周末打响。除了榜首马德里竞技将面对塞维利亚的一场硬仗，本周值得期待的赛程还有皇马和巴萨的比赛，他们分别将面对两支在降级边缘的球队，这意味着后者将会毫无保留地为三分而战。', 102002, '本轮比赛将在北京时间4月3日星期六凌晨03:00打响，由莱万特和韦斯卡拉开周末狂欢的序幕。莱万特足球俱乐部的比赛观赏性已经延续数年，而韦斯卡这支升班马的状态在新教练帕切塔（Pacheta）接手之后似乎不太稳定，然而考虑到双方都是以进攻风格著称的球队，这场对决想必将会十分有趣。', 4, 530, 5, 1, 101, '2021-05-04 21:18:56', '2021-05-04 21:18:56');
INSERT INTO `news` VALUES (1002, '胡金秋31+14广厦大胜北京 许钟豪盖帽数追平姚明升至历史第4', '2020-21赛季CBA常规赛第50轮，北京对阵广厦。广厦打出24-2的绝佳开局，此后曾被北京队将分差追至4分，之后重新拉开分差，末节一度领先超过20分。最终，广厦以116-93大胜北京队23分，取得两连胜，北京队无缘连胜。许钟豪追平姚明，升至CBA历史盖帽榜第4位。', 101002, '胡金秋31+14广厦大胜北京 许钟豪盖帽数追平姚明升至历史第4\"	\"2020-21赛季CBA常规赛第50轮，北京对阵广厦。广厦打出24-2的绝佳开局，此后曾被北京队将分差追至4分，之后重新拉开分差，末节一度领先超过20分。最终，广厦以116-93大胜北京队23分，取得两连胜，北京队无缘连胜。许钟豪追平姚明，升至CBA历史盖帽榜第4位', 5, 433, 5, 0, 201, '2021-05-04 21:26:06', '2021-05-04 21:26:06');
INSERT INTO `news` VALUES (1003, '欧冠八强诞生：意甲全军覆没，皇马独撑西甲，英超三强PK拜仁', '三支意甲球队悉数被淘汰，西甲也只剩下皇马，三支球队晋级让英超成为今年欧冠大赢家。随着切尔西、拜仁的晋级，欧冠八强全部产生。', 102001, '三支意甲球队悉数被淘汰，西甲也只剩了皇马，三支球队集体晋级让英超成了大赢家。在16强战里，尤文、拉齐奥、亚特兰大三支意甲球队分别被波尔图、拜仁和皇马淘汰，继2018-19赛季之后再次集体止步16强。在2019-20赛季，也只有亚特兰大一支意甲球队晋级八强。连续三个赛季在欧冠的低迷表现，证明C罗、伊布的加盟，无法提升意甲整体的实力和战斗力。与意甲球队一样，西甲球队同样表现不佳，巴萨以2比5的总比分被巴黎淘汰，塞维利亚则以4比5的总比分遗憾出局，马竞则遭遇切尔西的双杀。淘汰了亚特兰大的皇马，则成了唯一一支晋级的西甲球队。相比之下，财力更为雄厚的英超球队抓住了机会，在利物浦拿到欧冠冠军之后，曼城本赛季又展示出了绝佳的状态。三支球队集体晋级八强，蓝月亮成为夺冠热门球队之一，英超在欧冠拿出了世界第一联赛应有的表现。西甲不会将皇马、巴萨和马竞踢出西甲联赛，三强仍然会在本赛季进行比赛。不过欧冠却可能变天，按照《图片报》的说法，拜仁、多特和波尔图有望&ldquo;复活&rdquo;，然后跻身欧冠4强行列，另外一个消息是，弗洛伦蒂诺有意组建新的世界杯。在欧洲超级联赛官宣成立后，欧足联给出了强硬的态度，并且举行了会议，切费林的说法是，这些俱乐部的球员将被禁止参加世界杯。但是西媒透露，弗洛伦蒂诺并不惧怕，并且计划组建新的世界杯', 6, 480, 60, 5, 101, '2021-05-05 20:30:56', '2021-05-05 20:30:56');
INSERT INTO `news` VALUES (1004, '托特纳姆热刺官方宣布：主教练穆里尼奥正式下课', '穆里尼奥', 102003, '自2019年11月加盟热刺之初，穆里尼奥签署了一份直到2023赛季结束的合同', 8, 200, 50, 1, 101, '2021-05-05 18:15:40', '2021-05-05 18:15:40');
INSERT INTO `news` VALUES (1005, '艰难逆转！辽宁淘汰广厦晋级四强', '12', 101002, 'CBA季后赛1/4决赛打响，辽宁迎战广厦。辽宁队在上半场落后的情况下，第三节依靠梅奥与朱荣振的爆发，单节净胜13分扭转局势。末节辽宁一度将领先优势拉大至两位数，最终，辽宁以105-98淘汰广厦，成功晋级四强，他们将在半决赛对阵浙江与青岛之间的胜者。', 1, 55, 1, 1, 101, '2021-05-04 21:18:30', '2021-05-04 21:18:30');
INSERT INTO `news` VALUES (1006, '揭秘中超开幕式 火神杯将“飞”进天体', '中超开幕式', 102006, '中超开幕式当晚，载有火神杯和一位工作人员的飞行器，将从位于天河体育场东门的网球场广场起飞，飞跃天河体育场上空，落在体育场内场。机舱内的工作人员将携带火神杯走出，把火神杯交给赞助商代表。赞助商代表再把火神杯交到在宣誓台上等候的容志行手中，由容志行把火神杯放到杯座上，完成今年开幕式的火神杯交接仪式。', 7, 40, 20, 1, 101, '2021-05-05 20:31:11', '2021-05-05 20:31:11');
INSERT INTO `news` VALUES (1007, '87分钟绝平！利物浦3连胜终结', '马内破荒造300球里程碑', 102003, '2020-2021赛季英超第32轮，利物浦客场1-1利兹联，马内打破球荒造红箭三侠300球里程碑，略伦特第87分钟绝平。打破球荒！马内最近10场联赛首次为利物浦进球，这是塞内加尔大腿本赛季英超第8球，其中有6球都是在客场攻入。值得一提的是，这是红箭三侠联手攻进的第300粒进球。', 1, 214, 1, 1, 101, '2021-05-04 21:43:37', '2021-05-04 21:43:37');
INSERT INTO `news` VALUES (1008, '506天后重回天体！广州德比拉开中超大幕，', '全华班恒大争冠or沦为平庸\r\n全华班恒大争冠or沦为平庸\r\n全华班恒大争冠or沦为平庸', 102006, '2021赛季的中超开幕，广州恒大成为继上赛季的大连人之后，又一支坐镇名义主场的球队。只不过与大连人相比，广州恒大是时隔506天再度回到天体作战，归化球员阿兰和高拉特更是阔别自己的主场2个赛季。拥有归化球员的“全化班”恒大，也正式迎来检验自己成色的时候，是继续与国安、上港争冠，还是如卡帅所言，新赛季只能完成更新换代这一个任务？', 1, 123, 1, 1, 101, '2021-05-04 21:19:33', '2021-05-04 21:19:33');
INSERT INTO `news` VALUES (1009, '曝三球已取下石膏恢复篮球活动，7-10天或复出！', '黄蜂稳前八再加码', 101001, 'NBA名记查拉尼亚在推特上报道，黄蜂队后卫拉梅洛-鲍尔骨折的右手腕已经痊愈，他已经被允许恢复个人的篮球活动。三球是在2021年3月21日对阵快船的比赛中受伤，此前的报道是他大概率要赛季报销，没想到仅仅30天他就已经恢复了，这个速度有些惊人了，不少球迷害怕他再度受伤，毕竟凯文-杜兰特才复出两场都受伤了。', 1, 332, 1, 1, 101, '2021-05-04 21:22:49', '2021-05-04 21:22:49');
INSERT INTO `news` VALUES (1010, '场均43.8分！恭喜库里再获大奖', '36记三分超5支球队＋一神迹历史唯一', 101001, 'NBA联盟官宣本赛季第17周东西部周最佳球员，尼克斯球星兰德尔首夺东部最佳，上周场均35.8分83篮板6.5助攻，带队4胜0负，已成纽约新王；勇士球星库里再次当选，场均狂砍43.8分太疯狂，当选西部周最佳真乃实至名归。上周库里带领勇士拿到3胜1负的战绩，场均狂砍43.8分5.8篮板和5次助攻，投篮命中率57.3%和三分命中率高达54.3%；库里今年第二次当选西部周最佳，职业生涯第16次当选；整个四月份，库里场均得分接近40分，打出极其疯狂的个人表现。', 1, 220, 1, 1, 101, '2021-05-04 21:21:16', '2021-05-04 21:21:16');
INSERT INTO `news` VALUES (1011, '官宣！杜兰特哈登因伤休战，篮网多达七人缺阵欧文将单核出战鹈鹕', '欧文将单核出战鹈鹕', 101001, '篮网官方公布了明天客场对鹈鹕的伤病名单，多达七名球员被贴上了OUT（缺席）的标签。杜兰特（左大腿挫伤）、哈登（右腿筋拉伤）、泰勒-约翰逊（右膝酸痛）、齐奥扎（右手骨折）、克拉克斯顿（健康安全协议）、佩里（健康安全协议）、丁威迪（右膝伤病）都不能出战这场比赛。这意味着欧文将单核带队来打这场比赛。自从哈登加盟篮网后，这支球队就很少有凑足核心阵容的时候。美媒列出了相关的数据，哈登加盟后，篮网三巨头（哈登、杜兰特、欧文）一起出战的比赛只有7场，篮网5胜2负；三巨头中两人出战，20胜6负；三巨头中一人出战，6胜4负；三巨头都没打的比赛，0胜1负。合计是31胜13负。', 1, 150, 1, 1, 101, '2021-05-04 21:12:11', '2021-05-04 21:12:11');
INSERT INTO `news` VALUES (1012, '快船达成禅师定律 过去30年仅4冠军队未做到', '输20场 前 赢40场！', 101001, '如今，快船已经成为了太阳和爵士之后，本赛季第三支拿到40胜的球队，他们在输掉20场比赛之前，赢下了40场，这个数据意味着快船正式的成为总冠军竞争者。在体育竞技中，有很多非正式的官方规则，比如说，禅师规则。菲尔-杰克逊认为，一支精英级别的球队会在常规赛输掉20场比赛之前拿到40场胜利，就像是三连冠时期的湖人，更遥远的就是90年代六次拿下总冠军的芝加哥公牛了。\r\n\r\n这是一条很少被人们提及的规则，2008年，当菲尔杰克逊提出这一说法的时候，他执教的湖人刚刚达成这一成就，虽然那年湖人没有拿到总冠军，但凯尔特人在输掉20场比赛之前也拿到了40场胜利，这样的例子不胜枚举。', 1, 135, 1, 1, 101, '2021-05-04 21:12:22', '2021-05-04 21:12:22');
INSERT INTO `news` VALUES (1013, '篮网签欧冠得分王最新进展！', '最快4月21日签约，为哈登欧文减压？', 101001, '根据名记Marc Stein的报道，篮网正在和前莫斯科中央陆军后卫迈克·詹姆斯进行深入谈判，预计最快将在明天和篮网签约。迈克·詹姆斯本赛季在欧冠篮球联赛场均得到19.3分5.7助攻，是中央陆军头号得分手。不过他和球队发生矛盾，并被中央陆军队内禁赛，他随后返回美国，想要回到NBA打球。在被禁赛之前，他本是这个赛季欧冠联赛MVP的热门候选。迈克·詹姆斯曾经在2017-18赛季效力太阳和鹈鹕，场均得到9.3分3.5助攻。随后他去了欧洲打球，2018-19赛季他效力米兰队，场均得到19.8分加冕欧冠联赛得分王，本赛季他效力中央陆军，球队在欧冠联赛的常规赛战绩排名第二，仅次于巴塞罗那。', 1, 156, 1, 1, 101, '2021-05-04 21:19:43', '2021-05-04 21:19:43');
INSERT INTO `news` VALUES (1014, '阿隆-戈登：只要约基奇在 掘金仍有机会冲冠', '只要约基奇在', 101001, '穆雷在和勇士比赛时遭遇十字韧带撕裂，导致赛季报销，失去队内的头号核心后卫，让掘金的季后赛前景蒙上阴影，不过阿隆-戈登表示，掘金依然是冠军的有力争夺者，因为他们还有尼古拉-约基奇。\r\n\r\n“我们依然觉得球队实力很强劲，因为我们队内还有约基奇，”阿隆-戈登在接受The Athletic记者Sam Amick采访时说道，“一旦你队中拥有像约基奇这样的球员，每场比赛都感觉能赢下来，所以大家都要站出来，每人都要全力争胜，只要我们做到这一点，我们有机会去达到想要的目标。”\r\n\r\n的确本赛季约基奇有这MVP级别的表现，场均26.1分，56.7%的投篮命中率，11个篮板8.8次助攻都是全队第一，而穆雷在得分、篮板、助攻全排全队第二。', 1, 143, 1, 1, 101, '2021-05-04 21:19:52', '2021-05-04 21:19:52');
INSERT INTO `news` VALUES (1015, '官宣！盖帽王右脚大脚趾第二跖趾关节部分撕裂', '将无限期缺战', 101001, '球队中锋特纳今日接受了核磁共振检查。结果显示他遭遇了右脚大脚趾第二跖趾关节部分撕裂，将无限期缺战。本赛季至今，特纳为步行者出战47场，场均出场31分钟，得到12.6分6.5篮板1助攻0.9抢断3.4盖帽。作为2015年第11顺位被步行者选中的新秀，这是特纳今日联盟的第6个赛季，25岁的他无论是个人技术还是身体状态都慢慢步入了职业生涯的黄金期。由于个人技术一年一个台阶的进步，球队和他在2018年签下了一份4年8000万美元的大合同，而这才是这份合同执行的第二年。\r\n\r\n作为2015年第11顺位被步行者选中的新秀，这是特纳今日联盟的第6个赛季，25岁的他无论是个人技术还是身体状态都慢慢步入了职业生涯的黄金期。由于个人技术一年一个台阶的进步，球队和他在2018年签下了一份4年8000万美元的大合同，而这才是这份合同执行的第二年。\r\n\r\n', 1, 122, 1, 1, 101, '2021-05-05 01:58:26', '2021-05-05 01:58:26');
INSERT INTO `news` VALUES (1016, '渡边在猛龙转正 ', '前国手社媒发问：我们的呢？我们的呢？\r\n', 101001, '昨天晚上，渡边雄太成功转正，猛龙把他的双向合同转为了正式合同。\r\n\r\n得知这一消息后，前国手焦健今天凌晨5点半在社媒发文，他写道：“渡边雄太正式签约NBA猛龙队。日本国家篮球队又多了一员现役NBA猛将！我们的呢？我们的呢？我们的呢？”\r\n\r\n本赛季，渡边雄太共出战39场比赛，场均上场13.4分钟能够得到4.0分3.3篮板，命中率44.8%，三分命中率40.0%。', 1, 111, 1, 1, 101, '2021-05-04 21:20:00', '2021-05-04 21:20:00');
INSERT INTO `news` VALUES (1017, '浙江创队史两纪录', '赛后更衣室疯狂庆祝 吴前要应周鹏之约杀入总决赛', 101002, '0-21赛季CBA季后赛四分之一决赛全部打完，最后出战的浙江稠州男篮对阵青岛男篮打出强大的统治力，他们最多领先对手36分，全场比赛没有给对手任何的机会，最终以111-98赢下比赛，轻松晋级到四强。此役对于浙江男篮来说是具有里程碑意义的比赛，这一战创造了两项队史纪录。首先，这是他们历史性地杀入四强，此前浙江队季后赛最好的战绩是第五名；其次，核心吴前在赛前举起了战神奖杯，这是浙江男篮队史上首个MVP球员。', 1, 116, 1, 1, 101, '2021-05-04 21:20:03', '2021-05-04 21:20:03');
INSERT INTO `news` VALUES (1018, '好消息！曝孙悦拒绝退役，马布里留人成功，将重返CBA赛场', '孙悦拒绝退役', 101002, '北控男篮输给青岛男篮而无缘季后赛，比赛结束之后很多球迷都认为这将会是孙悦职业生涯的最后一场比赛，毕竟之前李根在接受媒体采访的时候说出了孙悦即将退役的消息。就在回到北京之后，马布里与孙悦进行了沟通，同时北控男篮高层也希望孙悦能够继续帮助球队。据了解目前孙悦已经初步放弃了退役的想法，下赛季将重返CBA赛场。\r\n\r\n', 1, 118, 1, 1, 101, '2021-05-04 21:20:05', '2021-05-04 21:20:05');
INSERT INTO `news` VALUES (1019, '冯欣：感谢俱乐部、队友、教练和球迷', '会用更好表现回馈大家', 101002, '今天，广厦后卫冯欣更新微博动态，感谢俱乐部的信任，感谢队友和教练组的帮助和指导，感谢球迷的支持和激励。\r\n\r\n具体内容如下：\r\n\r\n【不负所爱，不负篮球】终场哨声响起的那一刻，有些遗憾有些不甘。\r\n\r\n首先感谢广厦俱乐部的信任，这是我来到广厦男篮的第一个赛季，共同成长的过程很美好，我也会继续努力的让自己和球队一起变得更好。\r\n\r\n感谢队友和教练组的帮助和指导，在新的环境里很快就让我感受到了温暖与团队的力量。\r\n\r\n这是一个漫长的赛季，感谢俱乐部所有的工作人员，是你们辛勤的付出为我们保驾护航。\r\n\r\n感谢所有支持我们，激励我们的球迷，未来，会用更好的表现回馈大家！', 1, 180, 1, 1, 101, '2021-05-04 21:12:30', '2021-05-04 21:12:30');
INSERT INTO `news` VALUES (1020, '赵岩昊：有很多不甘心下赛季我们一定会走得更远 ！！', '我们一定会走得更远 ', 101002, '广厦男篮昨日不敌辽宁男篮，止步季后赛8强。比赛结束后，赵岩昊更新社交媒体，和本赛季告别。\r\n\r\n“久久不能平静下来，赛季结束了。肯定有很多不甘心，但这也是生活的一部分！感谢教练和管理层对我的信任！感谢队友们的努力不放弃！感谢球迷朋友对我的支持！我会更加沉淀自己，下赛季我们将继续奋战！一定会走得更远！love forever。”赵岩昊在个人微博上写道。\r\n\r\n昨日的比赛，赵岩昊出战39分钟，得到17分4助攻，本赛季常规赛他场均17.8分3.4篮板3.4助攻，两分命中率55%，三分命中率40%。', 1, 109, 1, 1, 101, '2021-05-04 21:20:09', '2021-05-06 01:53:02');
INSERT INTO `news` VALUES (1021, '周琦回归辽宁？', '离队征兆初现，周琦发博怒喷管理层！', 101002, '在与山东队的比赛结束之后，周琦一瘸一拐的走出了体育馆，落寞的背影让人心疼。他尽力了，出场37分钟砍下16分10篮板5盖帽。第四节打到7分钟，周琦捂着脚踝后部跟腱的位置痛苦倒地，被队友搀扶回到更衣室的画面令全国球迷揪心。周琦也在比赛之后更新微博，内容为“还要继续画饼吗？还会信吗？”，不知道是不是在指责球队管理层。\r\n\r\n本赛季，周琦场均拿下20.7分11.9个篮板2.7个助攻1.1抢断和2.3个盖帽，各项基础数据都在同位置前列。甚至在进阶数据上，周琦对当选MVP的吴前完成了全面超越。这样的表现也让周琦入选了赛季一阵，让不少球迷为他没有当选MVP鸣不平。但是，周琦的赛季也几乎就在颁奖典礼之后结束了。因为是前四名，所以新疆队在季后赛第一轮轮空。没想到以逸待劳的新疆队在他们的季后赛首秀中，就被山东队淘汰，无缘下一轮。', 1, 103, 1, 1, 101, '2021-05-04 21:20:13', '2021-05-04 21:20:13');
INSERT INTO `news` VALUES (1022, '朱芳雨大手笔！周琦，王哲林打包拿回广东！顺道开启广东新王朝', '爆周琦下一站不是辽宁，朱芳雨提早行动，周琦+王哲林打包拿回广东+提早开启CBA广东新王朝，杨鸣+雅尼斯愕然，姚明无语。', 101002, '今早，粤媒更爆出爆炸性的新闻，粤媒透露周琦的下家不是辽宁，而是直奔广东！粤媒更爆出猛料说，朱芳雨吸取之前的教训，在引援上已走在前头！避免再次出现这个赛季引援处处碰壁的尴尬局面！\r\n\r\n粤媒还透露，朱芳雨这次动作很大！并非只招入周琦！而是把周琦和王哲林打包拿回广东！培养两人接班大哥阿联！顺道开启CBA广东新王朝！', 1, 145, 1, 1, 101, '2021-05-04 21:20:18', '2021-05-04 21:20:18');
INSERT INTO `news` VALUES (1023, '武磊18分钟0射门！', '西班牙人1-1无缘西乙6连胜，德托马斯失点球', 102002, '西乙第35轮焦点战，西班牙人客场对阵阿尔梅里亚。西班牙人一度0-1落后，德托马斯助攻迪马塔扳平，踢成1-1，无缘西乙6连胜！此役，武磊无缘首发，替补出场18分钟，没有任何射门和射正。比赛焦点\r\n\r\n1、武磊出场18分钟，只有7次触球，没有任何射门、射正和威胁传球\r\n\r\n2、德托马斯点球不进，连续3轮罚丢点球\r\n\r\n3、西班牙人错失联赛6连胜，但连续12轮比赛不败，领先马略卡3分，排在西乙第1', 1, 254, 1, 1, 101, '2021-05-04 21:25:43', '2021-05-04 21:25:43');
INSERT INTO `news` VALUES (1024, '曼联新核率先反对欧超！队内许多球员非常愤怒，巨头急招球星安抚', '曼联新核率先反对欧超', 102001, '欧洲超级联赛成立的消息，已经完全压倒曼联将英超落后分差缩小到8分的事情。各大豪门对成立欧超很有决心，然而，俱乐部球迷、名宿甚至现役球员，却对这一可能破坏足坛整体平衡的金元足球计划表达了反对意见。弗爵、加里·内维尔和里奥·费迪南德等传奇人物纷纷表示谴责，「红魔主义者」内维尔甚至为此怒斥自己心爱的球队「恶心」。而曼联新核布鲁诺·费尔南德斯，也成为英超六强中第一名表态反对欧超方案的球员。根据新的欧洲超级联赛计划，将有15支创始豪门固定不变，另外每个赛季特邀5支球队参赛，分为两组进行双循环比赛，排名小组前三的球队直接晋级，四五名踢附加赛争夺另外两个八强席位。\r\n\r\n目前加入欧超联的球队除了曼联，还有英超的曼城、切尔西、利物浦、热刺和阿森纳，以及西甲三强皇马、巴萨和马竞，意甲三大劲旅国际米兰、AC米兰与尤文图斯。共计12支球队，拜仁慕尼黑、巴黎圣日耳曼没有参与。\r\n\r\n豪门脱离欧足联的最大原因，不是为了更高水平的足球，而是一个字——钱。美国投行摩根大通提供35亿欧元的创始资金，只要加入就有份瓜分。按照计划，参赛队最高可以分到5亿欧元，最低也有1亿欧元进账。而欧冠的收入不仅不固定，而且最多也在1亿左右，像本赛季小组出局的曼联，只有6000万欧元收入。', 1, 210, 1, 1, 101, '2021-05-04 21:22:22', '2021-05-04 21:22:22');
INSERT INTO `news` VALUES (1025, '反转！巴萨皇马不会被踢出西甲', '欧冠2大剧情：拜仁或再战巴黎', 102001, '欧洲12家著名俱乐部成立了欧洲超级联赛，这成为了足坛的重磅新闻，而根据西班牙科贝电台的说法，西甲不会将皇马、巴萨和马竞踢出西甲联赛，三强仍然会在本赛季进行比赛。不过欧冠却可能变天，按照《图片报》的说法，拜仁、多特和波尔图有望“复活”，然后跻身欧冠4强行列，另外一个消息是，弗洛伦蒂诺有意组建新的世界杯。在欧洲超级联赛官宣成立后，欧足联给出了强硬的态度，并且举行了会议，切费林的说法是，这些俱乐部的球员将被禁止参加世界杯。但是西媒透露，弗洛伦蒂诺并不惧怕，并且计划组建新的世界杯，可见，欧足联遇到了巨大挑战。', 1, 146, 1, 1, 101, '2021-05-04 21:17:22', '2021-05-04 21:17:22');
INSERT INTO `news` VALUES (1026, '取消媒体开放，来中国4年首次出现，央视记者表达不满', '卡帅怒了！', 102006, '2021赛季中超首轮，广州队将对阵广州城队，也就是恒大VS富力。央视记者刘思远透露，当他和媒体同行抵达后，被告知媒体开放取消了，这是卡纳瓦罗来中国4年，第一次出现这种情况。新赛季，恒大的目标仍是中超冠军，他们也是夺冠热门。\r\n\r\n过往，卡纳瓦罗在恒大是春风得意，他的年薪达到1000万欧元，放在现在，也是中超第一年薪主帅。此外，2019年，卡纳瓦罗率领恒大拿到了中超冠军。但上赛季，卡帅的恒大丢掉了中超冠军，最终，冠军属于江苏队，但江苏队已经没有了。卡纳瓦罗遭遇尴尬，他一度被架空。但后来，卡纳瓦罗还是回来了。', 1, 95, 1, 1, 101, '2021-05-04 21:17:17', '2021-05-04 21:17:17');
INSERT INTO `news` VALUES (1027, '弗洛伦蒂诺：皇马曼城切尔西不会被踢出欧冠 球员也不会被禁赛 我保证', '皇马曼城切尔西不会被踢出欧冠 球员也不会被禁赛', 102001, '弗洛伦蒂诺：皇马、曼城、切尔西不会被踢出欧冠，其他加入超级联赛的俱乐部也不会被踢出国内赛事，百分百，我确定，这是不可能的。', 1, 160, 1, 1, 101, '2021-05-04 21:17:10', '2021-05-04 21:17:10');
INSERT INTO `news` VALUES (1028, '卡瓦尼进球后通知曼联决定转会！', '感谢索帅挽留，但去意已决不续约', 102003, '曼联锋霸卡瓦尼虽然最近连续进球，但他已经作出决定，不会与俱乐部续约。在破门得分，帮助球队3-1击败伯恩利之后，卡大佐告诉主帅索尔斯克亚，今夏他将选择自由转会。\r\n\r\n34岁的卡瓦尼最近7天内连续3场比赛为曼联进球，但一切也许是因为他已经明确了未来，心无杂念的缘故。31场比赛为曼联打入10球，成为C罗之后第一名进球数上双的红魔球员，卡瓦尼希望用更好的方式结束自己在老特拉福德的征程。\r\n\r\n曼联与卡瓦尼签署的是「1+1」合同，但激活一年优先续约权，必须双方都同意。索尔斯克亚希望留下卡瓦尼，但球员本人没有同意，意味着续约失败。卡瓦尼并不适应英格兰的生活，尤其是他的家人在目前的情况下无法前往英国与他会合。\r\n\r\n索帅虽然游说他说，等球迷回归，情况会完全不同，但卡瓦尼还有另一个要求，他希望更多出场。而如果不是马夏尔、拉什福德本赛季经常受伤，梅森·格林伍德陷入低迷，乌拉圭国脚的出场次数可能不会那么多。', 1, 111, 1, 1, 101, '2021-05-04 21:17:08', '2021-05-04 21:17:08');
INSERT INTO `news` VALUES (1029, '足坛一夜再变天，拜仁或在欧冠复活，当事人发声', '余震24小时！', 102001, '距离欧洲超级联赛组建过去24小时，足坛又发生了哪些变化呢？可以说，余震依然在继续。欧足联：将球员踢出世界杯\r\n\r\n欧足联官方的回应是：欧洲俱乐部赛事改革提案通过，2024年起欧冠将扩军为36队！欧足联掌门人切费林明确表示，“制裁所有参加欧超联赛的俱乐部，参与此比赛的球员将会被禁止参加世界杯和欧洲杯，同时他们不会被允许代表他们的国家队出战。”这样的表态，坐实了之前媒体的猜测，也就是说，选择欧超，将彻底告别欧战乃至国家队！\r\n\r\n欧足联法律小组开会研究，计划将皇马、曼城与切尔西踢出本赛季欧冠，那么只剩下巴黎1队还在欧冠，法甲豪门可以提前颁奖获得本赛季欧冠冠军了。当然，图片报提出另一种可能的结果：拜仁多特波尔图很可能复活，拜仁也再次强调不会加入欧洲超级联赛！而且现在，媒体已经盘算下赛季的欧冠球队了，一旦12家球队被踢出欧冠，那么按照目前的排名，包括比利亚雷亚尔、贝蒂斯、皇家社会、埃弗顿、利兹联、拉齐奥、罗马等队伍都将参加欧冠！', 1, 120, 1, 1, 101, '2021-05-04 21:17:05', '2021-05-04 21:17:05');
INSERT INTO `news` VALUES (1030, '连续2天，中国球迷迎来3个好消息！', '有利于国足进军世界杯', 102007, '西班牙人在西乙一场强强对话中，客场1-1逼平阿尔梅里亚，虽然无缘联赛6连胜，但在积分榜上依旧稳居榜首，比排名第3的阿尔梅里亚多出10分，直升西甲几乎板上钉钉。不出预料的话，下赛季武磊将重新出现在西甲赛场上。\r\n\r\n今天晚上，对中国球迷来说是一个欢乐的夜晚，因为今年的中超将正式打响。晚上8点，将上演两场大战，分别是揭幕战广州队对阵广州城，以及重庆对阵泰山。中超今晚开幕，对国足备战40强赛也非常有利，例如今晚的2场比赛，将会有多名国脚在比赛中得到真刀真枪的磨练，有利于国足在中菲和中叙这2场生死大战中获得胜利。中国球迷在关心中超比赛的同时，对国际足坛的动向也会非常关注。昨天，12支英超、西甲、意甲豪门宣布成立欧洲超级联赛计划，令世界足坛震惊。随后欧足联主席切费林就表态，欧洲足球机构的法律团队正在评估可能立即采取的法律和制裁措施。并警告说，参加欧洲超级联赛的球员，不仅不能参加欧冠和欧联，也不能代表自己的国家队参加欧洲杯和世界杯。', 1, 170, 1, 1, 101, '2021-05-04 21:16:59', '2021-05-04 21:16:59');
INSERT INTO `news` VALUES (1031, '澎湃：今年中超可能无球队降级 国脚不确定能否完整踢完本赛季', '能否完整踢完本赛季', 102006, '在新赛季中超联赛即将打响之际，澎湃新闻网发文质疑了今年中超的赛制，并认为世预赛的赛程与中超下半程存在着严重的冲突，若国足晋级了12强赛，可能国脚下半年就无法参加中超赛事。\r\n\r\n上赛季中超的赛制引发了不小的争议，像第一阶段成绩非常糟糕的津门虎，第二阶段第一场比赛就击败深圳，最终早早完成了保级。尽管今年的赛制进行了较大的变动，但同样也会引发这样的疑问。本赛季中超联赛没有直接降级的球队，联赛的倒数第一和倒数第二，将分别和中甲联赛的第三名和第四名进行两回合附加赛，取胜的球队也可以获得2022赛季中超参赛资格。哪怕今年30轮联赛全部输球，也只需要赢下最后两回合的附加赛，同样可以留在中超。因此很大概率，今年联赛会没有球队降级。\r\n\r\n最后16轮联赛将在今年下半年进行，不过考虑到世预赛的关系，国脚们能否完整参加今年的联赛，就要打上一个很大的问号。如果国足顺利打入12强赛，今年9月、10月和11月三个国际比赛日，中国队每个月都将参加两场12强赛比赛。如果国足最终参加12强赛，国脚们是无法同时兼顾联赛和世预赛，这样他们有很大概率无法参加剩余16轮联赛，这也势必会对联赛下半程的走势起到一些微妙的影响。', 1, 160, 1, 1, 101, '2021-05-04 21:16:55', '2021-05-04 21:16:55');
INSERT INTO `news` VALUES (1032, '拜仁高层感到被弗里克戏耍，赫内斯与鲁梅尼格将强硬应对！', '弗里克戏耍，赫内斯与鲁梅尼格将强硬应对！', 102004, '拜仁慕尼黑愿意放走弗里克的前提是得到一笔解约的费用，但是，弗里克心仪的新东家德国队已经明确表示不会为新教练支付转会费用，德国足协自己在2012年放萨默尔去拜仁慕尼黑的时候也没有要钱，这场谈判将会非常艰难。\r\n\r\n《图片报》还表示，拜仁慕尼黑高层必须要在和弗里克以及德国足协的对话中保持团结，在弗里克提前向媒体官宣自己的决定之后，被打的猝不及防的高层感到自己被弗里克欺骗了。赫内斯和鲁梅尼格已经有很长时间没办法在一件事情上达成一致了，但这一次，他们一致同意：你不能这样戏耍我们！', 1, 87, 1, 1, 101, '2021-05-04 21:16:49', '2021-05-04 21:16:49');
INSERT INTO `news` VALUES (1033, '两连胜PK两连胜！AC米兰迎意甲冲冠关键之战，央视现场直播！', '两连胜PK两连胜！', 102005, '本周意甲联赛迎来一周双赛，在联赛第32轮AC米兰将会坐镇主场，他们的对手是萨索洛队。在此前AC米兰在联赛排名第2位，萨索洛队则排在第8的位置。本场比赛对于两支球队而言，都将会是至关重要的一场对决，尤其是对主场作战的AC米兰而言，他们只有全取3分才能留住争冠希望。在上一轮联赛AC米兰主场战胜了热那亚队，球队取得了联赛两连胜。而他们的同城对手国际米兰上轮战平，领先优势缩水到了9分。联赛还剩最后7轮，AC米兰队仍有一丝希望冲击本赛季意甲冠军。自从今年1月份开始，AC米兰从未在联赛连赢3场，本场比赛他们必须的唯一目标就是冲击三连胜。', 1, 92, 1, 1, 101, '2021-05-04 21:16:43', '2021-05-04 21:16:43');
INSERT INTO `news` VALUES (1034, '雷比奇抽射世界波米兰险胜！伊布缺阵曼珠造乌龙！托莫里失误丢球', '最后8场，2到5位只有4分差距，也就是说最后冲刺阶段，米兰、尤文、亚特兰大和那不勒斯必然有一支队伍会掉出欧冠区。', 102005, '实从以欧冠区为目标来衡量的话，米兰的下半赛季拿的分数是达标的，并且得益于上半赛季的老本，米兰在30轮过后就已经基本拿到了过去几个赛季的下限分数。\r\n\r\n但这同时意味着最后冲刺阶段，各队的容错率都已经很低了，往往一场比赛不止价值3分那么简单。\r\n\r\n但之前我也说过很多次，现在米兰最大的麻烦还不是场上的问题，是场外的续约谈判问题。\r\n\r\n除了伊布似乎应该能确定续约一年之外，罗马尼奥利、恰尔汗奥卢、多纳鲁马这几个的续约几乎很难达成共识，联赛节骨眼上当然是先搁置，但对球员心情的影响多少，我们肉眼可见。这场迎战热那亚的比赛，罗马尼奥利已经伤愈进入大名单，但仍被皮奥利排除在首发之外，克亚尔和托莫里已经成为了米兰最后阶段的固定搭配，这对带着队长袖标的小罗马来讲是一个不好的信号。\r\n\r\n类似的情况还有几乎已经确定会被退货的达洛特，卡卢卢已经取代了在卡拉布里亚受伤期间他的替补右后卫位置，左边后卫当然依然是特奥，尽管他最近状态一塌糊涂。\r\n\r\n双后腰凯西和本4没有疑问，前面三个攻击手是标准配置的56号、恰10和雷比奇，最前面的中锋是莱奥——伊布因为上一场有些莫名其妙的红牌而停赛。\r\n\r\n最重要的：赛前意大利媒体报道，米兰已经知道了多纳鲁马和尤文图斯达成了预先协议，不管这消息是真是假，今天多纳鲁马依然首发了。', 1, 84, 1, 1, 101, '2021-05-04 21:16:41', '2021-05-04 21:16:41');
INSERT INTO `news` VALUES (1035, '中超山东泰山2比0重庆，新援首秀破门，郭田雨进球', '中超山东泰山2比0重庆', 102006, '2021赛季中超联赛广州赛区首轮，山东泰山队2比0击败重庆两江竞技，取得开门红。第52分钟，宋龙助攻莱昂纳多首秀破门，第66分钟，新援徐新送出助攻，郭田雨锁定胜局。山东泰山队控球和射门都占据明显上风，重庆全场只有一次射门，传球数据山东也是明显优势，这场比赛赢球理所应当。', 1, 91, 1, 1, 101, '2021-05-04 21:16:39', '2021-05-04 21:16:39');
INSERT INTO `news` VALUES (1036, '官宣！欧洲超级联赛正式停摆', '英超BIG6集体退出遭炮轰：懦夫', 102001, '超级联赛官方发布声明，暂停欧洲超级联赛，并且重申欧洲足球需要改变，同时确认将重新考虑这一项目。至此，持续不到50个小时的欧洲超级联赛，戛然而止。很快，欧洲各家中小俱乐部发声，FIFA带头表态，球迷们也积极反抗，所有的一切矛头直指12豪门。弗洛伦蒂诺代表12豪门，向全世界表达成立超级联赛的原因，喊话天下足球人，表示这是挽回损失的举措，甚至表示，足球必须适应年轻一代，目前这样的状况持续下去，足球将会消失。在巨大的压力下，曼城、利物浦、阿森纳、热刺、曼联、切尔西等队集体退出欧洲超级联赛，这个备受瞩目的赛事将要胎死腹中。为此引发的影响也巨大，伍德沃德官宣离开曼联，阿涅利也一度被曝离开尤文，但是他随后进行了否认。英超球队带头离开，皇马主持人Álvaro de la Lama直接开炮：“今晚是懦夫之夜，是机会主义者之夜！”值得一提的是，西班牙《世界体育报》报道，欧足联向英超六队提供了一笔巨款，让他们退出欧超联赛。如果属实，那么欧足联，成功分化了豪门内部。', 1, 98, 1, 1, 101, '2021-05-04 21:16:35', '2021-05-04 21:16:35');
INSERT INTO `news` VALUES (1037, '不走了！梅西决定与巴萨续约2年，阿圭罗“自宣”加盟', '梅西决定与巴萨续约2年', 102002, '虽然在国家德比中，在一场关乎联赛冠军争夺的重要比赛中输给了皇马，但此后巴塞罗那击败毕尔巴鄂竞技拿到国王杯冠军，科曼迅速将球队的状态拉回了正轨。在西甲联赛积分榜上，巴萨在30场比赛中取得了20胜5平5负的战绩，积65分排名联赛第3位，在少赛一场的情况下分别落后于排名第一的马竞5分，落后于排名第二的皇马2分。毫无疑问，联赛冠军的争夺已经处于白热化状态，上述三支球队都有望最终捧起冠军奖杯。在联赛的收官阶段，虽然相对于马德里竞技，巴萨有更好的状态；相对于皇马，巴萨有单线作战的优势，但对于科曼带领的这支球队而言，最后阶段的赛程却对他们来说非常困难。从赛程上来看，未来的4场联赛很可能将决定巴塞罗那本赛季的走势。首先，这4场比赛间隔的时间非常短，在10天时间内，巴萨将会打完这4场比赛，这对球队体能的要求非常高。其次，这4个对手中，除了格拉纳达之外，其他3支球队都不好对付。其中，比利亚雷尔虽然参加欧冠联赛已经几乎没有希望，但他们仍在努力确保欧联杯的参赛资格，他们领先身后球队的积分优势非常微弱。而对于另外两支球队瓦伦西亚和赫塔菲来说，他们分别排在联赛的第14位和第15位，仍然有降级的危险。梅西最大的问题是仍然没有完成续约。与球队的前任当家人巴托梅乌的关系不佳，是梅西迟迟未能完成续约的主要原因。上赛季结束后，梅西一度希望转会离队，但由于在离队条款上与巴托梅乌并未达成一致，梅西最终留了下来。本赛季，在拉波尔塔重新回到球队之后，梅西与球队的续约似乎正在朝着球迷们所希望的方向发展。此前有消息称，梅西的父亲，也是他的经纪人豪尔赫正在跟巴萨方面商谈续约事宜。而且似乎他们的谈判已经有了非常显著的进展。据西班牙六台转会专家伊斯特拉德消息，梅西已经决定与巴萨续约两年。据悉，在薪水方面，因为巴萨目前的财务状况比较困难，梅西将暂时降薪，待球队财务好转后，巴萨将为他涨薪。如果成功完成续约，等下次合同到期时，梅西已经35岁，距离他在巴萨退役又进了一大步。无论如何，以梅西目前所展现出的竞技水准以及对于巴萨的意义，他都配得上一份续约合同。除此之外，在阿圭罗的签约上巴塞罗那也进展明显。此前有消息称，巴塞罗那已经正式向阿圭罗方面提出了合同，双方已经达成了初步协议。据西媒La porteria消息，阿圭罗已经告知了他的家人和朋友们，自己将转会巴塞罗那，并将在那里效力两个赛季。从目前情况来看，阿圭罗转会的接近达成，或许间接促进了好友梅西的续约。', 1, 87, 1, 1, 101, '2021-05-04 21:16:33', '2021-05-04 21:16:33');
INSERT INTO `news` VALUES (1038, '蒋光太昨晚再现1次世界级解围：这防守动作不输任何外援！', '蒋光太昨晚再现1次世界级解围', 102006, '中超联赛昨晚迎来了新赛季的揭幕战，没有任何真正意义上外援的广州队，在主场2比2战平了广州城。而广州队主帅卡纳瓦罗也派出了多位国足的归化球员出场，其中进攻线上归化国脚阿兰取得了进球，而在防守端，虽然球队丢了2球，但是蒋光太依然发挥抢眼，1次世界级的解围让人不禁拍案叫绝，2个丢球也与他没有直接关系，让人再次看到了国足最强后卫的风采！这场比赛广州队迎来了“全华班”，虽然阿兰和蒋光太等人都是归化国脚，但是昨晚所有上场的球员都拥有中国籍，这也是近年来广州队中超比赛揭幕战唯一的一次。在比赛开始后不久，阿兰就用头球打进了新赛季中超第1球，而蒋光太第一时间上前与他进行了拥抱，这2位国家队的队友也将在未来出现在世预赛的赛场上，两人的感情可见一斑！而广州城昨晚更是给了广州队防线巨大的压力，蒋光太出色地完成了防守工作。在比赛第27分钟的时候，叶楚贵就接到队友精彩妙传，突入禁区，可以看到，广州队的大禁区位置已经失去位置，而蒋光太这次防守难度极大。但是蒋光太还是用一个世界级的放铲，将球干净破坏出禁区。可以说，蒋光太的这次启动速度和防守硬度都是大部分本土球员难以做到的！\r\n\r\n下半场比赛，广州队比分落后，球队更是大举压上进攻，后防线只剩下蒋光太来带领全队阻止对手的反击。在比赛第54分钟的时候，蒋光太再次展现了出色的防守意识，在这次广州城队前锋宋文杰的中路突破过程中，蒋光太用准确的判断，成功抢断对手，这次单挑防守蒋光太完胜中国本土前锋，而且干净利落，没有任何犯规动作！比赛进行到最后时刻，蒋光太依然体能充沛，在对方守门员送出的一次长传球进攻中，蒋光太再次用头球碾压对手，将球传给了身旁的队友高准翼，展现了出色的弹跳能力和身体素质！不得不说，蒋光太昨晚还是给了球迷不少的惊喜，他不仅是广州队目前最倚仗的球员，更是中国男足未来的后防核心，蒋光太这一晚的防守表现不输给任何外援！', 1, 87, 1, 1, 101, '2021-05-04 21:16:30', '2021-05-04 21:16:30');
INSERT INTO `news` VALUES (1039, '泰山新援首秀BUG级表现：113次触球＋4KP，断球后秀马赛回旋', '断球后秀马赛回旋', 102006, '凭借着莱昂纳多与郭田雨的进球，泰山在中超联赛首轮比赛中2-0取胜重庆，迎来开门红。本场比赛，球队新援孙准浩首发登场并打满了90分钟。作为备受球迷期待的强援，孙准浩在本场比赛中打出了Bug级的表现，一战征服了山东球迷。本场比赛，孙准浩作为拖后中场登场，承担的是防守端的保护以及进攻端的梳理工作。单从数据上来看，他的表现已经十分出色：113次触球，传球89次（成功率90%），送出4次关键传球，8次长传成功5次，12次对抗成功6次，1次抢断、4次拦截、1次解围、3次犯规。\r\n\r\n但从比赛的实际来看，“数据无法完全展示他的出色发挥”。而且，莱昂纳多首秀就能充分展示自己的射术，不仅仅是郭田雨、徐新等位置更加靠前的球员给了他支援，孙准浩的高位直塞也很重要。\r\n\r\n虽然对抗能力一般（12次对抗成功6次，50%），但是孙准浩对于球路的预判十分准确，而且，后续的动作十分连贯。', 1, 52, 1, 1, 101, '2021-05-04 21:16:26', '2021-05-04 21:16:26');
INSERT INTO `news` VALUES (1040, '拳王洛佩兹爆料：洛马琴科在输给他之后，精神有些“不正常”', '洛马琴科在输给他之后，精神有些“不正常”', 103008, '现无可争议的世界轻量级拳王特奥菲莫-洛佩兹（16-0,12KO）爆料，他听说老对手瓦西里-洛马琴科（14-2,10KO）在去年输给自己后，精神有些“不正常”。尚不清楚洛马琴科是真的存在神经问题？还是由于不敌洛佩兹导致的心情抑郁？\r\n\r\n洛佩兹说：“我听到了一些报道，说洛马琴科输掉比赛之后精神状态不太正常，我希望他能够好起来。在那一端，只要洛马琴科还在训练，保持注意力集中就好，这往往是问题所在，伙计，当你输掉比赛后，有时候情况并不好。”洛佩兹听起来似乎很担心洛马琴科，双方去年10月在拉斯维加斯邂逅，洛马琴科爆冷遭遇败绩，之后再未登台亮相。\r\n\r\n不久前的一次采访中，洛马琴科对于无法与洛佩兹复赛感到沮丧，他认为对方不想给他这样的机会，因为二番战上演的话，洛佩兹会被复仇。洛马琴科在与洛佩兹对抗中右肩受伤，赛后不久实施了手术，在一些人看来，洛马琴科失利是由于身体状况不佳导致，如果身体状况良好，较量可能是另外一种结果。有报道称，洛马琴科将于今年夏天对阵洛佩兹的老对手中谷正義，但尚未确定具体的时间、地点。大部分拳迷觉得，洛马琴科与洛佩兹较量中发力有些晚了，前六个回合几乎不出拳，后六个回合还算出彩，如果能够早一点这样，那极有可能赢得胜利，至于具体为什么这样？恐怕只有洛马琴科自己最清楚了。对于洛马琴科来说，从输给洛佩兹当中恢复过来往往是困难的，他显然想要一场复赛，无奈这不可能发生，洛佩兹觉得他再次与洛马琴科交锋没有任何好处，但你也可以说他担心输给对方。\r\n\r\n洛马琴科如果接下来真的要对阵中谷正義，他必须保持足够的动力，专注于训练，暂时忘掉与洛佩兹之战的郁闷，要知道中谷正義身材高大，与洛佩兹交锋时激战全场，容不得洛马琴科有丝毫懈怠。洛佩兹将于6月5日亮相美国迈阿密，对手是IBF强制挑战者小乔治-坎博索斯，这不是一场轻松的比赛，但洛佩兹并不在意，他表示会将目光放到6月22日格沃塔-戴维斯VS马里奥-巴里奥斯之战上，洛佩兹希望戴维斯胜出后，双方展开一场对决，如果真是这样，洛佩兹VS戴维斯将十分具有看点，估计可以被视为2021年拳坛最引人关注的大战之一。', 1, 54, 1, 1, 101, '2021-05-04 21:16:25', '2021-05-04 21:16:25');
INSERT INTO `news` VALUES (1041, '女排劲敌公布奥运名单！两黑人巨星意外落选，朱婷卫冕冠军迎良机', '朱婷卫冕冠军迎良机', 103002, '进入到东京奥运会周期之后，意大利女排强势崛起，意大利队现在已经成为中国女排的主要竞争对手。意大利女排在东京奥运会周期连续六次击败中国队，虽然中国女排后面又两次击败了意大利队，但是整体来看意大利女排的实力已经不比中国队差，中国女排想要在东京奥运会上拿到好成绩必须要击败意大利女排。中国女排之所以和意大利女排比赛时打得比较艰难，最主要的原因就是意大利女排有一位大杀器，她就是超级巨星埃格努。埃格努的扣球非常有杀伤力，除了球技以外，她的大力跳发也让中国女排难以招架。除了拥有埃格努以外，意大利女排还有一位黑人球员，那就是塞拉，塞拉和埃格努组成了黑风双煞组合，这两位球员让中国女排感到非常头疼。中国女排的球迷非常想知道意大利女排到底会派出什么样的球员出征东京奥运会，因为意大利队在东京奥运会上是夺冠大热门，她们的参赛阵容直接决定着意大利女排能否实现冠军梦想。近期意大利女排终于公布了她们的奥运会备战阵容，让球迷们感到有些意外的是两大黑人巨星埃格努和塞拉纷纷落选。2021年意大利女排国家队第一批12人集训名单，集训时间至5月2日。主攻(3人)：圭拉、索菲亚、梅尔利；副攻(4人)：马扎罗、卢比安、费德莉卡、弗尔兰；接应(2人)：恩瓦卡洛、明加尔迪；二传(1人)：博西奥；自由(2人)：费西诺、德.博托利。埃格努和塞拉没有参加本次集训也能解释得通，因为除了她们两个以外，其他的意大利队主力也没有参加本次集训，看来意大利女排也在着力培养年轻的选手。埃格努没有入选意大利女排的第一期集训，对于中国女排国家队的队长朱婷来说绝对是一个好消息，朱婷也迎来了卫冕的良机，为什么这么说呢？因为，如果埃格努不参加首期集训，那么也会减少她和队友的磨合，对于中国女排来说或许我们能够找到击败意大利队的办法，相信主教练郎平一定会深入研究意大利队。\r\n\r\n东京奥运会对于中国女排来说至关重要，中国女排想要在东京奥运会上拿到冠军必须要战胜强敌意大利队。意大利队和中国队都被分在了死亡之组，小组赛两支球队就会交锋，到底中国女排能不能实现卫冕冠军的梦想，球迷们非常期待。相信朱婷一定会带领中国女排在东京奥运会上取得好成绩。\r\n\r\n', 1, 67, 1, 1, 101, '2021-05-04 21:16:23', '2021-05-04 21:16:23');
INSERT INTO `news` VALUES (1042, '泰森悬了？', '约书亚聘请梅威瑟做顾问，或爆冷统一四大组织', 103008, '外媒披露，英国三冠王约书亚已经披露，他将邀请众多著名拳王辅佐自己，其中就包括五个级别的世界拳王梅威瑟，梅威瑟是历史上防守最出色的拳手之一，比赛中很少被对手重创，而约书亚为了自己以200%的状态打好和富里的比赛，防守是必不可少的环节。值得一提的是，梅威瑟的确是约书亚的支持者，在约书亚上一场和普列夫的大战现场，梅威瑟顶着英国疫情的风险，亲自去现场为约书亚庆祝胜利，从这一点来看，约书亚和梅威瑟的关系确实不一般，梅威瑟也会把自己的知识倾囊相赠，这对于约书亚击败富里，统一四大组织，无疑会提升取胜的把握。据了解，约书亚和富里的统一战预计在今年7月或8月初上演，目前定的比赛日期有三个，届时这场今年最重大的拳坛较量将与奥运会撞车，而比赛举办地很可能在沙特，因为沙特的土豪愿意出天文数字举办这场旷世大战，比赛也将在英国和美国进行付费直播，创造的纪录有望打破历史所有重量级拳赛的收益，至于约书亚能否爆冷战胜富里，让我们拭目以待。', 1, 34, 1, 1, 101, '2021-05-04 21:16:22', '2021-05-04 21:16:22');
INSERT INTO `news` VALUES (1043, '丁俊晖一轮游微博被攻陷！', '球迷愤怒：还打广告，你真让人失望透顶\r\n', 103007, '俊晖在第一阶段5-4领先的情况下，却在第二阶段与宾汉姆陷入苦战，最终还是以9-10落败，再次遭遇一轮游出局！至此，参赛的五位中国选手中，颜丙涛成为了闯入16强的唯一。丁俊晖的出局所引发的巨大争议，是其他选手输球所不能比拟的，因为丁俊晖堪称中国斯诺克一哥，是他吸引了很多中国球迷关注和热爱斯诺克这项运动，但当下丁俊晖的成绩无法满足球迷对他多年的期望值。当初很多人总认为随着小丁年龄的增长，他的经验和成绩会再上一层楼，但目前来看，丁俊晖已经大不如昔。在丁俊晖出局后，球迷们十分愤怒，他的微博再次被攻陷，“还特么打广告，你真让人失望透顶，这么多年的粉丝！”“等你啥时候缺钱再打世锦赛吧，任何比赛都一样，你已经没有过去那种赢得比赛的斗志了，你可以选择当个人生赢家，不一定非要拿大满贯！”“给点力吧，丁一轮，结婚后斗志全无，太安逸了过得！”“答应我，退役好吗？”\r\n\r\n丁俊晖可以说是年少成名，这也导致大家对他的要求更高、期望更多。但丁俊晖本人经常在各种关键比赛掉链子，如果一次两次的话还可能归结于他的运气不好与状态不行，但这么多年过去了，他在世锦赛上逐渐沦为了一轮游，大家却再难看到他在球桌上有进取的动力了，也就逐渐开始对他失去信心了。\r\n\r\n客观来讲，丁俊晖成名很早，也赚了很多钱，但他在婚后满足于现状，在心态上越发保守，如果持续下去，他这些年积累起来的人气很可能会就此消散，如果丁俊晖觉得自己状态实在不行了，也许真到了考虑退役的时候了。', 1, 99, 1, 1, 101, '2021-05-04 21:16:19', '2021-05-04 21:16:19');
INSERT INTO `news` VALUES (1044, '奥沙利文大失所望，期待的正式比赛丁俊晖爽约，中国一哥位置难保', '中国一哥位置难保', 103007, '对于中国斯诺克来说，本年度最重要的赛事世锦赛第四个比赛日，绝对可以称之为非常失望的一天。虽然在此之前，有两位中国选手田鹏飞、梁文博已经淘汰出局，但颜丙涛成为第二个晋级16强的选手，最重要的是，中国一哥丁俊晖当时的形势不错，以5：4领先宾汉姆，并且在第一阶段结束后，他还难得地露出了笑容。然而，到了第四比赛日，中国斯诺克选手遭遇重创。小将吕昊天以2：10的大比分被马克-艾伦横扫出局，其实吕昊天的出局是在预料之中，毕竟两人实力相差太大。丁俊晖的出局才是对中国斯诺克的重大打击，作为中国斯诺克运动的领军人物，丁俊晖被中国球迷寄予了太多的期望。但丁俊晖再一次让球迷们失望了，不仅如此，中国一哥的位置可能也不保。丁俊晖的签运并不算好，他首轮的对手为资格赛选手里面实力最强的宾汉姆，这也是首轮里面被认为是实力最为接手的比赛。在这个焦点赛中，说实话，丁俊晖打得并不算太差。虽然宾汉姆开局就来了一个2：0，但丁俊晖在第一阶段结束时打出了自己的状态，以5：4反超比分。第二阶段一开始，又是宾汉姆先进入状态，将比分追到5：5。不过，丁俊晖连轰单杆87分和79分，再下两城，以7：5领先。没想到，在关键的第13和14局中，丁俊晖接连出错，让宾汉姆抓住了机会。到了第15局，丁俊晖再次出现失误，打丢即将超分的底袋黑球，结果让宾汉姆实现了反超。不过，丁俊晖此后在第16局和第18局，打出了两杆50+，成功将比赛打到了决胜局。决胜局中丁俊晖再次出现失误，单杆45分的情况下左侧底袋大角度红球不进，又给了宾汉姆机会，高手过招，这样的失误就是致命的。结果，丁俊晖就此告别世锦赛，上演了一轮游。实际上，这场对决，丁俊晖打得并不差，10：9的比分也很好地说明了这一点。只是丁俊晖在关键场次时的表现，确实有些犯晕，给了对手机会。其实丁俊晖的绝对实力很强，单论技术而言，他仍然可以排在世界前几名。但是他在比赛的专注度，攻防两端的表现确实相比巅峰时期下降不少，这也是他近几年在比赛中总给人感觉差了那么一口气的原因。如果丁俊晖再不调整好状态，那么他与80后的几名球员的差距将会越来越大。输了比赛，不仅让丁俊晖长期以来的世锦赛冠军梦碎，而且他的世界排名，不出意外将会下滑。目前排在他身后的为中国选手颜丙涛，目前这位新科大师赛冠军已经成功打进了16强，只要接下来颜丙涛发挥出色，那么在克鲁斯堡就可能完成对丁俊晖的反超，在世界排名上成为新的中国一哥。而丁俊晖在2019年底英锦赛夺冠的奖金被扣除后，他的世界排名将会进一步下滑，甚至有跌出前16的可能。如果真是这样，那么明年的世锦赛，他将会去打资格赛。丁俊晖的失利，失望的不仅仅是中国球迷，还包括第一个打进16强的奥沙利文。奥沙利文曾表示，只有在8强才是世锦赛真正的开始。而他预测的8强对手中，就是丁俊晖，并且他还特意强调了丁俊晖，认为他不可低估。只可惜，丁俊晖爽约了。', 1, 67, 1, 1, 101, '2021-05-04 21:16:16', '2021-05-04 21:16:16');
INSERT INTO `news` VALUES (1045, '网球的由来，你知道么？', '网球的由来', 103001, '网球运动的起源可以追溯到12～13世纪法国传教士在教堂回廊里用手掌击球的游戏。法语称这种游戏为“jeu depaume”，就是用手掌击球的意思。以后这种游戏传入法国宫廷，成为王宫贵族娱乐消遣的一种活动。1873年温菲尔德改进了早期的网球打法，使之成为能在草坪上进行的一项运动，取名为草地网球。1、网球运动的起源可以追溯到12～13 世纪法国传教士在教堂回廊里用手掌击球的游戏。法语称这种游戏为“ jeu depaume” ，就是用手掌击球的意思。以后这种游戏传入法国宫廷，成为王宫贵族娱乐消遣的一种活动。\r\n\r\n2、后来，法国国王路易五世把网球定为王室贵族的专门活动，禁止平民百姓参加。游戏的方式是用一根绳子将游戏场地隔成两个半区，游戏双方在自己的场区内将一种塞满头发或绒毛的小布球击到对方场区内，对方又击回来，如此往返进行。这种游戏最初是在室内进行的，后来又移到户外进行。\r\n\r\n3、14 世纪中叶，法国王储将这种游戏的球赠送给了英国国王亨利五世。于是这种游戏又在英国王宫中兴起。由于网球最初是用埃及坦尼斯镇所产的最有名的绒布，斜纹法兰绒制作的，英国人就将这种球称为“ Tennis” 。也就是说，网球运动的得名是缘于盛产绒布的埃及坦尼斯镇。\r\n\r\n4、15 世纪这种游戏由原来的用手击球改进成了用椭圆形的球拍来击打，场地中间的\'绳子也改成了防止球从绳以下部位穿过的球网。\r\n\r\n5、1873 年英国人M · 温菲尔德改进了早期的网球打法，使之成为夏天能在草坪上进行的一项运动，取名为“草地网球” ，并出版了一本枟草地网球枠手册，制定出了最早的网球运动规则。M· 温菲尔德因此被人们称为近代网球运动的创始人。\r\n\r\n6、1875 年英国的板球俱乐部制定了网球比赛规则。并于1877 年7 月在英国的温布尔登举行了第一届草地网球比赛。后来该俱乐部把网球比赛场地定为长23.77 m 、宽8.23m 的长方形，球网中央高度为99cm ，每局采用15 、30 、40 等记分方法。 1884 年英国伦敦的玛丽勒本板球俱乐部把球网中央高度改为91.4cm 。至此，现代网球运动正式形成。', 1, 33, 1, 1, 101, '2021-05-04 21:16:14', '2021-05-04 21:16:14');
INSERT INTO `news` VALUES (1046, '六十年峥嵘岁月，细数中国网球辉煌历程', '中国网球辉煌历程', 103001, '自新中国成立以来，全国体育工作愈发受到重视，网球作为一项备受人们喜爱的运动，在华夏大地上的热度不断升级。国际体育交流有着其重要的历史意义，在世界各项综合性运动会的赛场上，从奥运会、亚运会到大运会、军运会，中国健儿为国家奋战的身姿为我们留下了深刻印象。\r\n\r\n此外，中华人民共和国全国运动会也为中国网球的发展做出了不可磨灭的贡献，中国网球健将的成长在每届全运会上都有所体现。我们汇总了中国网球运动员在各项综合性运动会的耀眼战绩与历届全运会的奖牌归属，带你走进中国网球六十年的辉煌历程。2004 年雅典奥运会：孙甜甜 / 李婷夺女双金牌 2008 年北京奥运会：郑洁 / 晏紫夺女双铜牌\r\n\r\n2004 年雅典奥运会\r\n\r\n女双金牌：孙甜甜 / 李婷\r\n\r\n2008 年北京奥运会\r\n\r\n女双铜牌：郑洁 / 晏紫\r\n\r\n亚洲运动会\r\n\r\n1974 年德黑兰亚运会\r\n\r\n男团银牌：许梅林、吕正义、王福章、高宏运\r\n\r\n女团银牌：张荣华、姜丽华、严大翠、郭汉琴\r\n\r\n混双银牌：许梅林 / 张荣华\r\n\r\n1978 年曼谷亚运会\r\n\r\n女单银牌：陈娟\r\n\r\n男双银牌：许梅林 / 顾明华\r\n\r\n男团铜牌：许梅林、孙春来、王福章、顾明华\r\n\r\n女双铜牌：余丽桥 / 陈娟\r\n\r\n1982 年印度新德里亚运会\r\n\r\n女团银牌：余丽桥、王萍、段丽兰、朱晓云\r\n\r\n男团铜牌：刘树华、马克勤、尤伟\r\n\r\n男单铜牌：刘树华\r\n\r\n女单铜牌：余丽桥\r\n\r\n男双铜牌：刘树华 / 马克勤李心意在1986年亚运会上夺金\r\n\r\n1986 年韩国汉城亚运会\r\n\r\n女团金牌：李心意、钟妮、段丽兰、濮秀芬\r\n\r\n女单金牌：李心意\r\n\r\n男团银牌：刘树华、尤伟、马克勤、谢昭\r\n\r\n男双银牌：刘树华 / 马克勤\r\n\r\n混双银牌：钟妮 / 尤伟\r\n\r\n男单铜牌：刘树华', 1, 44, 1, 1, 101, '2021-05-04 21:16:13', '2021-05-04 21:16:13');
INSERT INTO `news` VALUES (1047, '常见的网球运动损伤及防范策略', '常见的网球运动损伤', 103001, '很多业余网球爱好者都受到过伤病的困扰，年龄越大、球龄越长，受伤病的几率也就越高。有人统计过，在受伤的部位中，有40%发生在下肢，25%在上肢，20%在腹背部，臀部头部和眼睛等其余部位的比例为15%。\r\n\r\n以上是按照比例由高到低排序的，类似地，我们也将导致网球伤病的主要原因由高到低进行排序，逐一概述如下。第一个原因，过劳\r\n\r\n在导致伤病的所有诱因之中，运动量过度被排在了首位。在每个业余俱乐部里，你总能找到几位天天都要打球，得空就要打球的“球痴”。打球过量会使肌肉、神经系统处于过度紧张状态，让身体长期承受较大压力；打球过频则会让身体来不及恢复，进而导致肌肉关节、技术动作和精神状态出现一系列问题。\r\n\r\n人贵有自知之明。如果60岁之后还不服老，还像年轻时那样拼命，还不能合理控制打球的强度和频度的话，出现伤病是迟早的事情。\r\n\r\n就算你是精力体力都很旺盛的年轻人，你也要“悠着一点”。无论从事何种体育锻炼，你都要保证每周至少休息一天。每个业余网球爱好者都应该在休息和锻炼之间找到适合自己的最佳平衡点，不论你是年轻人还是老年人。第二个原因，锻炼方式太过单一\r\n\r\n在业余网球爱好者中，有相当数量的人锻炼方式仅限于网球，尤其是对网球高度上瘾的人，更不愿意将时间花在网球之外的运动项目中。而我们都知道，职业网球运动员的训练项目有很多，网球场仅是他们诸多训练场所中的一个，打网球也仅是他们众多训练项目中的一个。如果缺乏柔韧性、耐力和肌肉力量的训练，你在打网球时受伤的概率就会明显高于其他人。第三个原因，技术存在缺陷\r\n\r\n错误的、不合理的技术动作是导致手腕、肘部和腰腹受伤的直接原因，如果再与前文所述的过劳累积在一起，受伤的概率就会成倍增加。就算是职业球员，他们的技术动作、动力链条或多或少也都存在缺陷，更不消说打球动作五花八门的业余球员了。职业球员退役的直接原因大多是由于伤病，比如莎拉波娃的肩伤、李娜的膝伤、阿加西的背伤等等，即便是拥有一流的体能师和训练师，这些网球巨星们仍无法避免伤病的发生和累积，更不用说我们这些普通的业余球员了。\r\n\r\n从这一点来说，任何时候改动作都是值得的。在经验丰富的教练指导下，不断改进你的技术动作，优化你的动力链条，这不仅能提升你的技术水准，更可以显著降低受伤的风险。第四个原因，不合适的装备\r\n\r\n我在前天的文章里说过，不合适的球拍或许是网球肘的致病诱因之一。如果把这个话题展开来细说的话，不合适的球拍主要是指球拍重量、硬度、平衡点分布、握柄尺寸、线床类型、拍线类型及穿线磅数，这些都可能是导致伤病的具体诱因。\r\n第五个原因，急性意外损伤\r\n\r\n急性意外损伤一般具有不可预见性，往往是突然发生的，比如小腿肌肉拉伤、踝关节扭伤等等。我两次小腿肌肉拉伤都是发生在冬天，发生在打球开始阶段，这表明打球之前的热身非常重要，尤其是在气温较低的冬天。小腿肌肉拉伤后，我开始重视起打球前后的热身和拉伸，越来越体会到做这些功课的必要性。\r\n\r\n另外，像踝关节扭伤等意外事件其实也是可以预防的，比如你在打球之前一定要清理掉脚边的网球，否则一旦踩上去的话，很可能就会崴脚。再比如，一双合适的网球鞋也非常重要，至于正确合理的步法的重要性更是无需言说。为了最大限度地预防意外伤害的发生，你必须要时刻保持小心谨慎，对于难度极大超出自己能力范围的球，要权衡好利弊，懂得放弃。最重要的一点就是，练好体能、增强肌肉力量、提升柔韧性和改进步法，这些对于预防意外伤害非常有帮助。虽然练了之后不一定能显著提升打球水平，但只要练习，你总归会获益的。\r\n此外，不合适的球鞋也是造成伤病不容忽视的诱因。对于球龄不长的球友来说，他们往往忽视了一双合脚的网球鞋的重要作用。如果临时起意去打网球，有的人甚至来不及换上网球鞋，直接穿着休闲鞋、羽毛球鞋甚至跑步鞋就来打网球。网球运动对步法要求很高，需要急跑急停，频繁地向各个方向移动，穿的鞋子要具备良好的减震性、包裹性和支撑性，而其他球鞋很难适应打网球的需要。\r\n\r\n就个人经验来说，我都是在车里放一双网球鞋，以备不时之需，如果哪一天穿着跑步鞋去网球场，我宁愿站在边上看看，也绝不会穿着跑步鞋上场打球。', 1, 56, 1, 1, 101, '2021-05-04 21:16:11', '2021-05-04 21:16:11');
INSERT INTO `news` VALUES (1048, '龚翔宇24岁迎来新挑战，撑起女排右翼进攻，东京奥运会让郎平放心', '龚翔宇24岁迎来新挑战', 103002, '中国女排即将远渡东洋参加奥运测试赛，目前正在宁波北仑挥汗如雨进行封闭集训。而这一天对于江苏女排龚翔宇意义，迎接第二个本命年24岁生日，也迎来她各项技术日臻成熟、挑起中国女排大梁的新挑战。如今老将曾春蕾已经退役生娃，龚翔宇是目前经过大战检验的中国女排唯一成熟接应，只有在东京奥运会上稳定发挥，才能让郎平主教练真正放心！说起龚翔宇，虽然其地位比不了领导核心朱婷队长，但也是七大核心成员之一，在中国女排内不可或缺，作为全面型接应在进攻、拦网、一传、防守和小球串联等环节都比较过硬，绝大部分时间都能撑起中国女排的右翼进攻，因而深得郎平主教练信任！郎平若想最大限度发挥朱婷和袁心玥的攻击力，就必须尽量依赖龚翔宇发挥她出色的保障作用，可以说龚翔宇是中国女排建立攻防体系的基础。然而，龚翔宇成长经历并非一帆风顺，从国少打替补二传开始、改过替补副攻、替补自由人和替补主攻，最终被徐建德慧眼识珠，改成接应二传并迅速站稳脚跟，随后被郎平主教练重点发掘培养，并幸运地赶上了里约奥运会，成为中国女排最年轻的世界冠军之一！或许在2016年还有人质疑她占了名额，不会接发球等等，如今短短五年时间，她已经成长为中国女排当下最好和最全面的接应，在中国女排中的作用俨然堪比陈忠和的爱将周苏红！\r\n转眼之间龚翔宇又过了两年的成长和成熟期，24岁正是她精力体力最旺盛的时刻，就在即将迎来东京奥运会时，龚翔宇的成长进步也越发引起各界关注，因为这一次她没有更好、更全面的替补接应人选，她就是中国女排接应位置的大姐大！放眼东京奥运会，龚翔宇迎来的是机遇也是挑战，对于精力体力和竞技状态都处于巅峰状态的她，急需要通过一个完整的系列赛事，奠定她接应二传“大姐大”的核心地位！借此祝福龚翔宇24岁生日快乐之际，笔者真心期待她东京奥运会完美发挥，打出信心、打出士气，成为郎平主教练放心的全面接应，真正扛起中国女排的右翼进攻！\r\n', 1, 76, 1, 1, 101, '2021-05-04 21:16:08', '2021-05-04 21:16:08');
INSERT INTO `news` VALUES (1049, '爆料，天津女排再传好消息，引进新外援，世界级接应即将加盟！', '世界级接应即将加盟！', 103002, '最近随着中国女排备战奥运会到了最关键的时期，不少国际方面的好消息，也频频地传来，先是在五月一日开始的奥运会测试赛，接着则是将于月底开始的意大利进行的世联赛，而且之前还传出了今年和明年的世俱杯，会继续在中国举办得好消息，可以说是熬过了疫情之后，女排的春天终于到了！而面对这么密集又丰富的国际比赛，以及国内联赛和奥运会等等得挑战，各个女排俱乐部的压力也是比较大的，比如天津女排等国内豪门，更是担心一个不慎就跌落豪门的位置！不过对于天津女排来说，最近好消息也是不断，先是因为全运会预选赛，有几位球员的紧张比赛，所以小将王艺竹迎来了机会，被郎平选进了第三次集训，虽然时间不长而且还只是一次磨练，并没有任何参加奥运会的机会，但是对于一位缺乏认可和历练的成长期球员来说，也足够了，除此之外袁心玥的加盟也使得天津女排如虎添翼，而最近更是有传闻，天津女排的高层，为了部署接下来的备战安排，目前正在积极的接触国外的一些优秀的球员！早在上个月就有消息传出，说是天津女排在接洽埃格努，不过因为这个咖位还有各种原因，球迷们并不会当真，不过就在最近又有传闻传出了，据说是另外一位世界级别的接应手，来自费内巴切的瓦尔加斯，已经和天津相谈甚欢，打算签协议了，不过并不是长期的协议，而是暂时的租界，在和本国比赛不冲突的情况下，她来到天津完成一些任务，类似之前的卡里罗！\r\n\r\n众所周知，天津女排的接应位置确实不是很强，不管是之前有过尝试的王艺竹，还是需要养伤的陈博雅，目前都存在着或多或少的问题，所以引进外援已经成了天津应对名年联赛或者世俱杯的一大手段了，不过到底能不能洽谈成功，还需要等待官方的通知，', 1, 34, 1, 1, 101, '2021-05-04 21:16:06', '2021-05-04 21:16:06');
INSERT INTO `news` VALUES (1050, '要跑个寂寞！这赛事能搞成？苏炳添加特林领衔中美缺席接力世锦赛', '世界田联官网公布5月1日-2日在波兰的西里西亚举行的世界田联接力世锦赛参赛阵容。', 103005, '在大疫情下，这次世锦赛有来自37个国家的127支队伍参赛，但田径世界霸主美国队、苏炳添、谢震业领衔的中国队缺席。英国队、日本队虽然派队伍参赛，但派遣的都是替补，这两支男子4X100米接力很强的队伍也是主力尽藏。这是要跑个寂寞吗？世界田联官网的最新公告，只是告诉大家哪些大牌要参赛，但我们熟悉田径的朋友立即就明白，哪些大牌运动员没有参赛。加特林、莱尔斯等名将领衔的美国队，跟中国队一样彻底就没报名。日本队去了一帮替补，山县亮太、萨尼布朗、桐生祥秀等三名男子100米单项破10秒的一线高手均缺席。有意思的是，日本队男子4X100米接力的主力阵容为：23岁坂井隆一郎（10秒12）、22岁水久保漱至（10秒14）、21岁布鲁诺（10秒20）、17岁柳田大辉（10秒27）。桐生祥秀的东洋大学师弟宫本大辅，只能当替补。\r\n\r\n英国队只参加男女4X400米接力，他们的4X100米接力主力一个都没去。法国队虽然报名参加这次的男子4X100米接力，但绝对主力维考特没有在参赛阵容中。牙买加队这次给足了东道主面子，男女阵容都是几乎全主力出战、名将如云。女子方面是由里约奥运会女子100米、200米双料冠军汤普森领衔，她在2017年以全国纪录的成绩帮助牙买加夺得4x200米的冠军，然后在2019年获得同一项目的第三名。汤普森的队友谢里卡-杰克逊，是世界和奥运会400米铜牌得主，将参加女子4x400米比赛。\r\n\r\n男队方面，前百米世界纪录拥有者38岁的阿萨法-鲍威尔领衔，还有牙买加队参加过北京奥运会的内斯塔-卡特，这位仁兄的药检在2016年复查的时候呈阳性，将博尔特2008年北京奥运会的那块接力金牌搞掉了。35岁的内斯塔-卡特还在跑，可见牙买加男子短跑凋零有多厉害。年已经三次跑进10秒大关的南非飞人希姆宾，将领衔南非队参赛，南非队曾在多哈世锦赛上跑出37秒65的新全国纪录，估计这次他们很有可能再破纪录。\r\n\r\n近日土耳其单日新增6万多例、法国新增3万多例、荷兰新增1.7万多例、德国新增6967例，接力世锦赛主办国波兰单日新增1.7万多例，中国队选择不去参赛也是出于对运动员安全的考虑。4月24日，苏炳添、何宇鸿、葛曼棋等国家队短跑名将将参加田径分区邀请赛广东肇庆站的比赛。', 1, 88, 1, 1, 101, '2021-05-04 21:16:04', '2021-05-04 21:16:04');
INSERT INTO `news` VALUES (1051, '1分57秒73，18岁世界田径800米天才少女出世 奥运会有望冲击金牌', '在美国迈克尔·约翰逊田径邀请赛女子800米比赛中', 103005, '18岁美国天才少女阿辛姆迎来出色发挥，以1分57秒73夺得冠军，创造今年世界最好成绩，职业生涯首次突破2分大关，打破北美青年纪录，排名世界青年历史第八位，该成绩放在多哈世锦赛稳夺金牌，阿辛姆具备在即将到来的东京奥运会冲击金牌的潜力。世界青年纪录水平很高，是肯尼亚名将杰利莫在2008年苏黎世大奖赛跑出的1分54秒01，这也是过去20年来最好的成绩，排名田径历史第三位，当年19岁杰利莫四次打开1分55秒，夺得北京奥运会金牌。\r\n\r\n杰利莫巅峰期并不长，此后没有亮眼的大赛表现，南非双性运动员塞门娅强势崛起，成为过去十年女子800米第一人，夺得2012伦敦、2016两届奥运金牌，柏林、大邱、伦敦三届世锦赛冠军。塞门娅是一位硬实力很强的名将，有她参加的比赛整体竞争水平都很高，带动其他选手的发挥，前三名通常能打开1分57秒。现在仅是4月份，世界田径室外赛季的初始期，本场比赛也仅是阿辛姆的赛季首秀，田径运动员最好状态往往是在下半年6-9月展现出来，意味着随着状态不断得到调整，阿辛姆在未来几个月有望继续取得成绩突破，跑进1分57秒是非常可能的事情。塞门娅无缘奥运会，目前世界田坛还没有实力更强的选手，阿辛姆保持好的竞技状态，毫无疑问将成为东京奥运会夺冠热门，当然，对于只有18岁的阿辛姆而言，初出茅庐青涩稚嫩，大赛能力和大赛经验有待历练和积累，这是她晋级成为世界一流高手的必经之路，期待着这位年轻天才小将接下来的表现。', 1, 67, 1, 1, 101, '2021-05-04 21:16:01', '2021-05-04 21:16:01');
INSERT INTO `news` VALUES (1052, '明日预报名！', '2021雄安半程马拉松将于5月9日鸣枪起跑', 103005, '雄安马拉松作为雄安新区规模最大的全民健身赛事，自2017年至今已成功举办四届，成为河北地区具有代表性的路跑赛事之一。2019年，雄安马拉松荣膺中国田径协会“金牌赛事”称号，在全国范围打响了赛事知名度，成为代表雄安新区城市形象的一张名片。\r\n\r\n为填补雄安新区春季赛事的空白、让更多人享受到体育运动的魅力，雄安马拉松春季赛事雄安半程马拉松全新启航！\r\n\r\n本届赛事将于5月9日8:00鸣枪起跑，赛事预报名工作于4月21日10:00正式启动，抽签结果将于4月底公布。本次比赛设半程马拉松（21.0975km）1000人，健康跑（约7公里）1000人。赛事将严格执行中国田径协会金牌赛事要求和标准，努力打造成为雄安新区的第二个金牌赛事。', 1, 38, 1, 1, 101, '2021-05-04 21:15:59', '2021-05-04 21:15:59');
INSERT INTO `news` VALUES (1053, '【河北体育人物志】用实力成就非凡——王涵', '王涵', 103006, '王涵，1991年出生于河北保定，中国女子跳水队运动员。\r\n\r\n王涵还不到4岁时就在保定市体育幼儿园开始练习体操，9岁到河北省体操队集训时被跳水教练、郭晶晶的教练李芳选中，成为河北省跳水队的一员。\r\n\r\n自2008年进入国家队之后，王涵展现出了惊人的天赋，在刘恒林的指点之下，她迅速成长为拔尖运动员，成为国家队重点培养的跳板主力。\r\n\r\n在2009年罗马世界游泳锦标赛上，首次亮相世界大赛王涵就夺得了女子一米板项目的铜牌。\r\n\r\n2009年罗马游泳世锦赛中，王涵获得女子一米板铜牌；跳水大奖赛深圳站中获得女子三米板亚军、与何姿获得双人冠军。\r\n\r\n2010年跳水大奖赛德国站中，王涵与屈琳夺得女子双人三米板冠军、并获得单人三米板季军；在俄罗斯站比赛中，再次与屈琳获得女子双人三米板冠军和单人三米板冠军；加拿大站中获得女子单人冠军、与屈琳获得双人三米板冠军。\r\n\r\n2011年1月25日，王涵入选“河北省十佳运动员”。\r\n\r\n2018年8月6日，王涵入选第十八届亚运会中国体育代表团名单。\r\n\r\n2019年7月1日，王涵入选游泳世锦赛18人参赛名单。\r\n\r\n亚运夺冠\r\n\r\n广州亚运会跳水比赛在2010年11月22日开赛，首个项目女子双人3米板比赛中，施廷懋/王涵，以总成绩315.60分夺冠并闯进决赛。施廷懋和王涵虽然是首次搭档，但她们的个人能力强，配合默契，首战就取得了骄人的成绩。\r\n\r\n中国组合在比赛中鲜有对手，只要发挥正常，拿下金牌不成问题。第一轮规定动作，施廷懋/王涵发挥出色，轻松得到52,80分排在第一。\r\n\r\n第二轮施廷懋/王涵的301B，继续发挥出超高水平，各个技术环节都毫无瑕疵，获得54分，两轮累计106.80分进一步扩大了优势。第三轮开始自选动作的较量中，施廷懋/王涵难度系数3.0的405B，以超高质量完成了动作取得了72分。\r\n\r\n第四轮施廷懋/王涵选择5152B，全套动作几近完美，得到68.40分高分。最后一轮，施廷懋/王涵205B，取得68.40分，总成绩315.60分夺冠。\r\n\r\n2018年8月31日，雅加达亚运会跳水女子1米跳板决赛中，王涵以总成绩323.55分夺得冠军。同年9月1日，2018年雅加达亚运会跳水女子3米板决赛，王涵以总成绩383.40分获得亚军。\r\n\r\n2019年7月15日的国际泳联世界锦标赛在韩国光州展开争夺。在女子双人3米板决赛中，施廷懋/王涵以342分遥遥领先于第二名夺冠，为中国队实现世锦赛该项目十连冠。\r\n\r\n2020年10月6日，全国跳水冠军赛暨东京奥运会、世界杯预选赛举行，这场比赛在河北奥林匹克体育中心举办，首次在“家门口”比赛的王涵，在兴奋之余还略带一丝紧张。最终在女子单人三米板决赛上，王涵一路领先，最终以398.65分的总成绩获得冠军。赛后记者采访时，王涵笑着对记者说道：“这次3米板总分没有超过400分，虽然对这次比赛的结果满意，但是将近一年没有比赛，与之前比赛的发挥相比还是有些许的不如意，在比赛中更看重的是不同难度动作的发挥情况与总分，下一步目标就是拿到东京奥运会的入场券。”\r\n\r\n2021年1月24日，2021年中国跳水东京奥运会和世界杯选拔赛的第二站，王涵以412.20分的成绩获得女子三米板比赛冠军。3月12日，在2021年中国跳水明星赛暨第十四届全运会跳水测试赛女子双人三米板决赛中，王涵/施廷懋组合以340.8分的总成绩夺得冠军。', 1, 33, 1, 1, 101, '2021-05-04 21:15:57', '2021-05-04 21:15:57');
INSERT INTO `news` VALUES (1054, '73小时无限极挑战！', '在南极完成铁三', 103006, '3.8公里游泳，180公里自行车，再加上42公里的全程马拉松，完成这样一个超长距离的铁人三项（简称“大铁”），对于绝大多数人来说是不可能完成的任务。但有人却还要追求更惊人的无限极挑战——在南极洲完成铁三。\r\n\r\n丹麦人安德斯·霍夫曼就实现了这样一个无限极挑战——他经过了近73小时堪称炼狱的旅途，终于成为了首个达成目标的人，在极限运动历史上留下了自己的名字。“这次挑战所想要传达的东西就是，我们应该打破内心的限制。”他在近日接受BBC采访时表示，“这样的精神可以应用到生活的每一个方面。”安德斯·霍夫曼虽然也完成过长距离铁三的比赛，但并非专业的铁三运动员，也不是铁三这项运动的狂热爱好者，“我也不那么喜欢寒冷的天气。”但之所以起了在南极洲完成铁三这样一个令人直呼不可能无限极产品的想法，只是源于一次冲动。他观看了一段关于极限运动风筝冲浪的影片，自己又正好从事体育科技方面的工作，于是就产生了在极限运动历史上也留下自己名字的想法。“我喜欢的是那种对精神的磨练，以及战胜困难的意志力。“我花了73小时不到的时间，感觉很超现实，我好多次感觉这条路永远也走不完，所以到走完的时候还不敢相信。”在霍夫曼看来，他挑战自身完成这些无限极产品的传奇经历，或许也可以在这个特殊时期激励人们。霍夫曼表示，“我们生活中有太多不确定因素，目标也常常显得遥远。但是把目标分解成一个个小目标，不被距离的遥远吓倒，就能帮助你抵达目的地。”', 1, 122, 1, 1, 101, '2021-05-04 21:18:11', '2021-05-04 21:18:11');
INSERT INTO `news` VALUES (1055, '有望提前4轮夺冠！曼城2-0胜水晶宫 客场11连胜刷英超纪录', '北京时间5月1日晚，2020-21赛季英超联赛第34轮，曼城客场2-0击败水晶宫，取得各项赛事4连胜。同时他们获得英超客场11连胜，也创造了单赛季英超客场连胜的纪录。', 102003, '曼城或提前夺冠：在击败水晶宫之后，曼城取得各项赛事的4连胜，最近6轮英超他们获胜5场，同时也迎来英超客场的11连胜，创造英超单赛季客场连胜纪录。如果本轮曼联输给利物浦，曼城就将提前宣告获得本赛季的英超冠军。', 0, 250, 0, 0, 101, '2021-05-04 21:15:50', '2021-05-04 21:15:50');
INSERT INTO `news` VALUES (1056, '6-8到10-9！斯诺克90后名将晋级将战罗伯逊，世锦赛16强确定九席', '世锦赛16强确定九席', 103007, '斯诺克世锦赛继续在谢菲尔德的克鲁斯堡大剧院进行，14号种子利索夫斯基出战，同两届世锦赛亚军得主卡特争夺16强席位，继马奎尔不敌杰克.琼斯出局后，谁会成为第二位出局的种子球员受到外界球迷的关注，这一场势均力敌，胜负要看两人临场发挥。0后名将利索夫斯基在这个赛季表现突出，三次打进排名赛决赛，但可惜都遇到世界第一特鲁姆普获得亚军，目前世界排名前十六的选手，只有利索夫斯基和吉尔伯特没有冠军，吉尔伯特在首轮10-4战胜克里斯.韦克林晋级，但是利索夫斯基抽到的对手相对而言要强太多。\r\n\r\n机长卡特目前世界排名第23位，外界球迷对他印象最深的就是两次打进世锦赛决赛，都输给奥沙利文获得亚军，实际上卡特还有四个排名赛冠军，去年他替补奥沙利文参加大师赛，最终打进决赛不敌宾汉姆，2019年世锦赛首轮，卡特10-6战胜利索夫斯基，因此更被看好。就这样利索夫斯基6-8落后，最终10-9逆转卡特，成为第九位晋级16强的参赛选手，接下来他将对阵罗伯逊，从这场比赛来看，利索夫斯基险些在首轮出局，还好关键时刻卡特没能顶住压力，不过目前的利索夫斯基想赢罗伯逊，希望还是比较渺茫，两人实力有明显差距。', 1, 120, 1, 1, 101, '2021-05-04 21:15:29', '2021-05-04 21:15:29');
INSERT INTO `news` VALUES (1057, 'kpl：B组五支队伍谁能绝境逢生？QG直奔卡位赛，RNGM想拿首胜！', 'RNGM想拿首胜！', 103003, '2021年kpl春季赛常规赛正式进入到第二轮的比拼，这轮比赛对于B组的队伍来说，也是至关重要，因为一个不小心，他们就将提前结束这个赛季，所以每一步，都不能出现差错。对于B组来说，五支队伍都已经退无可退了，现在他们所有的希望都寄托于卡位赛，如果能排到B组的前二，那么就有机会打卡位赛，也是有机会进入到A组，搏一下季后赛的名额。五支队伍，谁能绝处逢生？抓住最后的机会，让我们拭目以待！WE这一次是从A组掉到B组的，原本在季前赛的时候，WE的状态就不怎么好，不过后面垫底的队伍足够，他们也是拿下了A组的名额。如今在A组失利，WE来到B组，说实话，WE真的没比B组的队伍强多少。比如他们和VG等队伍，都是半斤八两的存在。可以说，WE已经完美的融入到了B组。在第二轮中，WE想要突围，还是有一定的难度。其实在第一轮的时候，WE是有机会的，但是因为他们的迷之轮换，输掉了比较关键的比赛，而现在到了B组，WE也是需要加油了！VG和EDGM是B组原本的队伍，这两支队伍一直以来也是大多数时候，都在垫底。这个赛季，EDGM状态有所回暖，并没有像之前那样被人碾压，虽然还是没能突破，不过却让人看到了希望。而VG当初也是差了一点，就能进入A组的，所以说，这两支队伍，将会成为B组最大的变数。他们可以很强，也可能会问题不断，更多的是要看他们临场的状态。到底他们是强还是弱，一切都充满未知！掉到B组之后，QGhappy是非常遗憾的，同时也是有很多的不甘。不过事已至此，也是没有什么办法。对于QGhappy来说，如何在B组重新杀回去，才是当务之急。因为赛制，QGhappy没办法像MTG和RW侠那样，直接凭借着B组前二晋级，他们的目标是卡位赛。众所周知，只有前二的队伍有资格打卡位赛，QGhappy的目标也正是这个。然而从QGhappy的状态来看，别说卡位赛了，想拿B组的前二，都有点悬！', 1, 111, 1, 1, 101, '2021-05-04 21:18:19', '2021-05-04 21:18:19');
INSERT INTO `news` VALUES (1058, 'RNG刚夺冠就出事，他因为签证问题去不了冰岛，夺冠难度增加', '夺冠难度增加', 103003, '这两天关于电竞圈的消息是很多人在讨论的，估计大家也多少也有在参与这些话题，就目前的情况看，RNG夺冠还是很多人开心的，因为他们的实力确实不错，除了上单位置弱了一些，其他位置都是顶尖水平，感觉能创造出很多出乎意料的东西，所以让他们去msi上和其他赛区的战队碰一碰，感觉还是非常不错的，希望他们能打出效果。我们说回战队里面的情况，从这次春季赛的情况来看，几位新人真的让人感到很惊喜，在第二局和第三局中，本来一开始上路已经出现了巨大劣势，牛宝眼看就要起飞了，结果被RNG的中单和AD活生生的按了回去，这两场比赛也彻底击溃了FPX选手们的心理防线，导致第三局被碾压击败。最终RNG拿到了冠军，他们也很懂，拿到冠军后就马上开播，收到了很多的礼物，现在这段时间确实可以放松一下，因为距离msi的比赛还没有那么快，选手们现在放松几天，然后开始进行魔鬼训练，所以这几天大家还是能和选手们互动的。不过最近关于RNG的节奏也是挺多的，比如他们刚夺冠就被爆料出事了，爆料的人还是圈内大V，他之前爆料的很多事情都是真的，所以这次爆料的真实性也挺高的，那就是教练tabe去不了msi，他因为签证出了问题，所以去不了冰岛，自然就无法参加msi的比赛，BP也要换人做，这无疑是让RNG夺冠难度增加。现在很多人在讨论这件事，如果教练真的去不了，那确实影响还蛮大的，但是相信RNG一定有自己解决的方法，现在网络这么发达，完全可以做到远程开会，分析面对的选手，当然了，场上BP还是很考验临场发挥的，所以一切都还是未知数。', 1, 156, 1, 1, 101, '2021-05-04 21:15:26', '2021-05-04 21:15:26');
INSERT INTO `news` VALUES (1059, 'LPL三大解说饭局谈起双败赛制，表示赛制是公平的，但细节需要注意', 'LPL三大解说饭局谈起双败赛制，表示赛制是公平的', 103003, '最近管泽元，记得和米勒组织了一场饭局，在饭局中三大解说开始讨论起本次春决的结果。他们先是讨论了一下RNG夺冠的状态，表示这个状态确实太好了，再加上FPX状态有点差，没办法接下RNG的攻势。随后三大解说讨论起了双败赛制的问题，记得直接现场发问：双败赛制到底公不公平？其他两个解说各持意见，管泽元认为赛制是公平的，而米勒认为，赛制改革是很重要的，但现在的赛制并不是完全公平的。三大解说也开始讨论起之前有双败赛制的例子。欧美那边的双败赛制基本上是败者组打完的第二天就直接要打决赛，这个时候正好是心力交瘁的时候，对胜者组来说十分有利。但RNG和FPX的比赛是休息了一整周，这对RNG来说是养生休息，但对FPX来说，他们休息时间过长，导致手感没办法保持火热状态。FPX作为胜者组晋级的战队，他们在决赛本应该有着更多的优势，但除了优先选边权之外，FPX并没有任何优势，这一点也是现在解说和观众们都质疑的一点。毕竟作为胜者组晋级的战队，理论上在今年之前都已经是冠军了，但就因为这个赛制的问题导致他们还需要打一场决赛，而且在决赛中没有任何BUFF加成，这一点非常不合理。米勒表示，FPX其实在决赛是处于劣势的。首先武汉并不是FPX的主场，这一点就没有任何优势。再加上FPX早就已经打完了胜者组比赛，RNG紧随其后打完比赛，FPX休息时间比RNG更长，手感也冷得比RNG更快。这一点从比赛中也能看出来。而且很久以前的LOL也有过双败制度，当时胜者组晋级的在决赛会有先积一分的优势，这对败者组出来的战队也是一个非常大的考验。他们必须要顶住任何压力，在对手占尽优势的基础上击败对方，才有资格接受冠军荣耀。\r\n\r\n但这一次RNG并没有任何的压力，对他们来说，最大的压力就是所谓的复活甲已经没了，只要在败者组失利一次就会止步于败者组，没办法进入春决。但换个角度思考一下，以前的赛制不也是这样吗？所有战队都只有一次机会，但这一次明显是给了RNG多一次冲击春决的机会，这种压力难道以前就没有吗？整体来说，三大解说都表示双败赛制其实是很好的赛制，但细节方面肯定是需要注意的。胜者组在晋级之后居然一点特权都没有，这一点对胜者组晋级的战队来说并不公平。如果说以后官方会调整成胜者组晋级战队先积一分，或者ban位默认败者组战队空ban一个，只要是能够体现胜者组晋级的价值，那么任何战队努力想要从胜者组突围就有了更大的意义。各位观众怎么看待这一次三大解说的饭局谈话呢？', 1, 160, 1, 1, 101, '2021-05-04 21:14:50', '2021-05-04 21:14:50');
INSERT INTO `news` VALUES (1060, '面对“大考” 年轻团队玩转“雪上F1”', '年轻团队玩转“雪上F1”', 103004, '北京延庆国家雪车雪橇中心拥有中国首条雪车雪橇赛道，赛道拥有世界唯一超过360度的回旋弯，设计最快时速约135公里，冬奥会比赛中速度最快的雪车项目将在这里举行，它也被誉为“雪上F1”。同时，雪车雪橇赛道也是本届冬奥会中设计难度最高、施工难度最大、施工工艺最为复杂的新建比赛场馆之一，而这里的运行团队是一支年轻的队伍，他们面临着艰巨的实战考验。\r\n\r\n在记者探访的多个冬奥场馆运行团队中，国家雪车雪橇中心团队虽然比较年轻，但是他们担负的使命可不轻，毕竟这个项目以往在国内还没有正式举办过，完全是一个新事物，而且一下子就要面对冬奥“大考”，他们的压力可想而知。团队平均年龄30岁 一人多岗、群馆兼职\r\n\r\n据国家雪车雪橇中心场馆主任林晋文介绍，他们这支运行团队于去年11月正式组建，集中办公，组成成员有来自冬奥组委各部门的工作人员，也有从体育总局、媒体、电力、通信等专业公司属地抽调的人员以及实习生等等，坚持一人多岗，群馆兼职，最大限度降低人力成本，这样也有利于进行疫情防控。这支团队平均年龄30岁，很年轻，朝气蓬勃，干劲十足。25人组成医疗团队 均有重大赛事医疗保障经验\r\n\r\n林晋文表示，在测试活动中，场馆设施运行良好，冰务管理保障有力，医疗团队力量精干，通过本次测试活动使服务水平不断得到提升。医疗保障团队由25名业务能力强、有重大赛事医疗保障经验的医务人员组成，分别来自北京大学人民医院和北京急救中心。之前运行团队就组织进行了多次赛道救援和防疫情况下的受伤救治等应急演练。测试活动期间，医疗经理、医疗官、运动员医疗站医护人员主动与三个项目的国家队队医进行学习、交流。大家讨论了包括运动员受伤特点、容易发生事故的点位、国际大赛时翻车受伤案例以及救援经验等细节内容，均表示受益匪浅，这样在今后的赛事活动中就会为运动员提供更加高效的保障服务。测试：首办雪车雪橇赛“模拟考试”顺利达标\r\n\r\n按照“简约、安全、精彩”的办赛要求，国家雪车雪橇中心场馆运行团队于2月16日至26日圆满完成了“相约北京”冬季体育系列测试活动——2020/2021赛季全国雪车、钢架雪车、雪橇邀请赛3个项目的测试任务。此次测试活动在赛程制定方面充分参考2022北京冬奥会赛时竞赛时间并结合场地实际情况，合理规划了本次比赛赛程，部分运动员取得了突破训练水平的竞赛成绩，达到了测试预期目的。\r\n\r\n这次测试活动为期11天，共举办了男子双人雪车、女子双人雪车、女子单人雪车、四人雪车、男子单人雪橇、女子单人雪橇、双人雪橇、雪橇团体接力、男子钢架雪车、女子钢架雪车等10个小项比赛。特别值得一提的是，此次比赛是我国本土首次举办的雪车雪橇比赛，在我国冰雪运动史上具有里程碑的意义。\r\n\r\n林晋文在接受记者采访时表示，本次测试活动，国家雪车雪橇中心场馆运行团队成员之间的配合非常紧密和默契。在测试活动期间，共设置了26个业务领域，其中场馆群层面设置13个，场馆层面设置13个。通过第一次进入场馆实际操作，各个业务领域对于各自承担的职责任务有了更加清晰的认识和梳理。与此同时，更为重要的是各个业务领域之间的衔接和配合在实际工作中得到了充分磨合，整个场馆团队协同配合的意识得到了强化，场馆运行的合力得到进一步凝聚和提升。\r\n\r\n比如，在通讯集群方面，各业务领域对于集群通话的流程、操作更加清晰规范；在颁奖环节，颁奖业务领域在和成绩处理以及运动员组织方面的联系配合更加熟练顺畅。通过现场实战操作，发现和解决了很多桌面推演以及各业务领域单独组织的演练中没有暴露出的一些问题，而这些问题都是上下游业务领域存在交叉联系、需要互相配合的，各业务领域的大局观、整体观以及配合意识、协同意识通过此次测试都得到了锻炼和提高，为以后的测试和比赛积累了难得的实战经验。\r\n\r\n工程：高难度山林场馆4年建成 获赞“最出色的认证”\r\n\r\n历经4年的艰辛建设，北京2022年冬奥会延庆赛区国家雪车雪橇中心于去年年底完工，山林场馆蓝图变为现实。北京冬奥会期间，国家雪车雪橇中心将承担雪车、钢架雪车、雪橇三个项目的全部比赛，一共产生10枚金牌。\r\n\r\n国家雪车雪橇中心位于北京延庆区西北部，距离奥运村1000米，行车5分钟，这里建有中国首条雪车雪橇赛道，赛道拥有世界唯一超过360度的回旋弯，设计最快时速约135公里，冬奥会比赛中速度最快的雪车项目将在此举行，被誉为“雪上F1”。同时，雪车雪橇赛道也是本届冬奥会中设计难度最高、施工难度最大、施工工艺最为复杂的新建比赛场馆之一。赛道则分为54个制冷单元，全长1975米，垂直落差超过121米，由16个角度、倾斜度都不同的弯道组成。随着赛道的建成，它成为了世界第17条、亚洲第3条、国内首条雪车雪橇赛道，未来一些高水平国际赛事，包括世界杯、世锦赛等都可能来到北京，推动车橇项目在中国乃至亚洲的发展。\r\n\r\n2020年11月，国家雪车雪橇中心场地预认证活动顺利进行，国际雪车、雪橇联合会官员和相关运动员、教练员，通过赛道检查、自身滑行测试、观摩国家队滑行、察看检验设施设备等方式，对国家雪车雪橇中心场地进行了全面的测试认证，对赛道设施、制冰质量、灯光系统、广播系统、计时计分系统、塔台指挥系统、医疗站设置、运行组织等各方面给予高度评价，同时也提出一些具体改进建议，北京冬奥组委及时进行了调整并获得认可。\r\n\r\n国际雪橇联合会专家瓦尔特表示：“自从20世纪80年代以来，我参加了所有雪车雪橇场地认证，这次不仅仅是很棒，可以说是非常棒，是我见过的最出色的认证！”\r\n\r\n对于赛后可持续利用问题，国家雪车雪橇中心也做了充分准备，该中心利用比较平缓的部分赛段，设计了青少年训练出发区和大众体验出发区，冬奥会结束后，该场地在满足专业比赛要求之外也将用于大众的冰雪项目体验等。另外，屋顶的观光廊道还可眺望赛区全貌。', 1, 33, 1, 1, 101, '2021-05-04 21:14:45', '2021-05-04 21:14:45');
INSERT INTO `news` VALUES (1061, '建设工程量最大、技术难度最高，这个冬奥场馆占据两个“最”', '冬奥场馆占据两个“最”', 103004, '“百年京张路 筑梦冬奥行”网络主题活动来到位于张家口市崇礼区的国家跳台滑雪中心。  国家跳台滑雪中心是张家口赛区冬奥场馆建设工程量最大、技术难度最高的竞赛场馆，主体建筑设计灵感来自中国传统饰物“如意”，被形象的称为“雪如意”。国家跳台滑雪中心由顶峰俱乐部、出发区、滑道区、看台区组成，设置8个比赛项目，产生8枚金牌。\r\n\r\n项目负责人介绍，跳台工程施工难度极高。跳台工程东西长约500米，由顶部的顶峰俱乐部、中部的滑道、底部看台组成，顶部最高点与底部地面的落差达到160多米，整个工程架于自然山谷之间，施工难度巨大。为了保证施工，施工伊始更多的是在为“造地”创造条件，即对现有地形进行改造，以便更好的满足未来施工。现场看到的新建的多条上山道路就是为了施工而准备的。而跳台本身造型又极具特点，将中国传统吉祥饰物如意进行转译，与跳台赛道结合，形成“雪如意”，这是冬奥会中的标志性工程。如意顶部是直径', 1, 16, 1, 1, 101, '2021-05-04 21:14:43', '2021-05-04 21:14:43');
INSERT INTO `news` VALUES (1062, '许勤在张家口市检查调度：高标准高质量推进冬奥筹办工作', '许勤', 103004, '河北省省长许勤在张家口市崇礼区检查调度冬奥筹办工作，强调要深入学习贯彻习近平总书记关于冬奥筹办工作的重要论述，全面落实第24届冬奥会工作领导小组全体会议精神和北京冬奥组委部署，按照省委、省政府工作安排和王东峰书记要求，强化问题导向目标导向，高标准高质量推进冬奥筹办工作。\r\n\r\n许勤来到张家口赛区冬奥会指挥调度中心，检查指挥体系运行情况，要求中心从现在起保持常态化值班状态，工作人员下沉一线，加强软硬件设备调试，完善与其他信息化系统平台的互联互通，确保赛时运行高效有力。在太子城冰雪小镇，他详细了解国宾山庄、洲际酒店、颁奖广场、高铁枢纽等处配套设施建设和装修工作进展，强调要在确保质量和安全的前提下加快进度，强化日常督导巡查，从细节上查漏洞补短板，不放过任何一个角落。奥运村正在进行内部装饰，许勤走进样板间查看户型布局、家具配置等，强调要进一步优化设计，注重色彩搭配，提升建筑整体品质；要从入住人员角度出发，充分考虑疫情防控需要和残疾人运动员需求，提供精细化管理和人性化服务。许勤还到国家跳台滑雪中心和国家冬季两项中心，检查场馆运行和赛后利用等工作。他说，要坚持中国特色、国际视野，加密巡查检查，把高质量要求贯穿到每个环节，尽快优化灯光照明方案，装修设计注重与周边自然环境相协调，注重与赛后场馆市场化运营的衔接。随后，许勤主持召开省冬奥领导小组指挥调度会议，对项目建设、服务保障、城市运行、指挥体系等事项逐一调度。他指出，当前，冬奥筹办工作已经进入全力冲刺、决战决胜的关键时期，大家要提高政治站位，坚持大局观念，增强责任意识、使命意识，落实“四个办奥”理念和“简约、安全、精彩”办赛要求，把思想认识再强化，把组织体系再强化，把协调机制再强化，进一步做实做细冬奥筹办各项工作。要抓好场馆和配套设施完善提升，加快室内外装修进度，同步推进无障碍环境建设。要提高服务保障质量，认真总结测试活动经验，优化安保、交通、住宿、餐饮、医疗、志愿等工作方案。要完善城市运行保障体系，学习借鉴北京等地做法，细化应急预案，做好水电气热供应、交通运行保障等工作。要全面排查问题隐患，建立任务清单、责任清单，明确时限整改到位。要加强组织领导，压实各方责任，实行日巡查、周调度机制，强化督导检查，扎扎实实完成各项筹办任务。\r\n\r\n副省长严鹏程、省政府秘书长朱浩文参加活动。\r\n\r\n', 1, 56, 1, 1, 101, '2021-05-04 21:14:40', '2021-05-04 21:14:40');
INSERT INTO `news` VALUES (1063, '河北将开展社区冰雪运动会暨乡村冰雪体验活动', '记者从河北省体育局了解到，为扎实推进群众性冰雪运动进一步下沉基层，2021年河北将把重点放在群众“家门口”，在全省范围推出社区冰雪运动会和乡村冰雪体验活动，助力实现“到北京2022年冬奥会开幕前全省3000万人参与冰雪运动”目标。\r\n\r\n', 103004, '本次活动将于5月启动，持续到2022年2月北京冬奥会举办，跨年度推进。各地将根据社区、乡村实际情况，充分利用周末、节假日、晨晚锻炼，特别是青少年寒暑假等时间节点来举办。活动地点上，将选择各地社区所辖、乡村周边的滑冰馆、滑雪场、公园、商场、小区广场、学校操场等。\r\n\r\n河北省体育局副局长唐青介绍，活动将以社区趣味冰雪运动会、乡村冰雪体验活动、“冰雪大篷车”项目推广、城区中心公园四季冰雪运动区项目普及、线上冰雪运动展示与比赛为核心内容，通过省市县分级举办启动仪式等措施，推动活动有序高效深入基层开展、实现群众性冰雪运动融入日常。\r\n\r\n唐青表示，针对乡村人口特点，活动倡导以陆地冰壶、桌上冰壶、陆地冰球射门、仿真滑雪等简约体验式项目和冰雪项目表演、冰雪知识文化展览、微型冰雪运动器材展、视频宣讲等内容为主，组织农村群众了解、认识冰雪运动，让全省喜迎冬奥的氛围在乡村浓厚起来。', 1, 65, 1, 1, 101, '2021-05-04 21:14:38', '2021-05-04 21:14:38');
INSERT INTO `news` VALUES (1064, '习近平时间丨冬奥情缘', '冬奥情缘', 103004, '北京冬奥会和冬残奥会冰上项目测试活动4月1日至10日在京举行\r\n\r\n回首中国的冬奥之路\r\n\r\n从索契到洛桑 从崇礼到北京\r\n\r\n中国国家主席习近平对冬奥的各项工作一直非常关心\r\n\r\n一路引领\r\n\r\n国际奥委会主席巴赫曾说\r\n\r\n就推动奥林匹克运动而言\r\n\r\n习近平主席是当之无愧的冠军', 1, 45, 1, 1, 101, '2021-05-04 21:14:36', '2021-05-04 21:14:36');
INSERT INTO `news` VALUES (1065, '竞技水平提升 群众广泛参与——冰雪运动，亮出“双飞翼”', '亮出“双飞翼”', 103004, '随着冬奥脚步越来越近，冰雪运动早已走出山海关、跳出冰雪季，形成了东南西北遥相呼应、冬夏两季各具特色、冰上雪上全面开花的新格局。竞技水平提升与群众广泛参与，仿佛振动的双翼，正带动冰雪运动行稳致远。\r\n\r\n春日里难觅冰雪踪迹，但冰雪运动的热度却并未减弱，无论是国家队备战北京冬奥会的专注，还是群众参与冰雪运动的热情，都记录与见证着冰雪运动的快速成长。竞技水平提升与群众广泛参与，仿佛振动的双翼，正带动冰雪运动行稳致远。\r\n\r\n如今，随着冬奥脚步越来越近，冰雪运动早已走出山海关、跳出冰雪季，形成了东南西北遥相呼应、冬夏两季各具特色、冰上雪上全面开花的新格局。\r\n\r\n冬奥备战“冲刺年”，享有更多保障\r\n\r\n虽然冰雪运动新赛季受疫情影响未能如期开启，各项目国家集训队的备战却一刻不曾放松。\r\n\r\n花样滑冰国家集训队在日前结束的世锦赛上与来自全世界的高手同场竞技，检验训练效果；雪上项目多支队伍也积极开展队内对抗赛，以赛带练、以赛促练。冬奥备战已进入“冲刺之年”，各队都在积极积蓄能量，锤炼精兵。\r\n\r\n随着天气转暖，单板U型场地、自由式滑雪U型场地、自由式滑雪雪上技巧等多支队伍已转场到新疆可可托海国际滑雪场，以延长雪上训练时间。部分正处于短暂调整期的队伍也将结束休整，重新投入到下一阶段的训练中。\r\n\r\n近两年不断加强的科技投入和科研保障，让队伍在春夏季节有了更多模拟雪上训练的选择。在河北涞源，跳台滑雪科训基地的两个跳台滑道均属四季滑道，全年运行不受气温影响，冬夏同轨。风洞实验室则具备水平、起跳、飞跃3个试验段，可满足运动队的全年使用需求。在吉林北山，雪洞滑雪场犹如一个可四季运转的冰箱，为越野滑雪队提供了反季节训练保障。在秦皇岛训练基地，夏季的水池训练高度还原了雪上的竞技状态，可以让自由式滑雪空中技巧运动员不断锤炼起跳和腾空动作，冲击新难度。\r\n\r\n随着雪季结束，进入“冲刺”阶段的各集训队面临着来自内外的“双重”挑战：对内，要选拔并锤炼精兵，不断提升竞技水平和参赛能力；对外，各项目的资格赛体系尚未完全确定，形势的不断变化考验着队伍的应变能力。\r\n\r\n国家体育总局冬季运动管理中心副主任洪平说，冬奥会北京周期的备战思路，从原来强调大运动量训练，到现在更注重追求科学高效，“要根据不同运动员的身体实际，有针对性地调整运动量和运动计划”。此外，科技含量高的医疗、康复、营养等保障工作的建立健全也在为队伍训练保驾护航。\r\n\r\n项目普及“四季歌”，唱响更多地方\r\n\r\n不久前，冰雪运动走进了重庆渝北区数据谷中学校，同学们扮演历届冬奥会主办方代表方阵创意入场，一场趣味十足的冰雪互动活动与该校田径运动会同步上演。冰雪体验活动包含了陆地冰壶、陆地冰球、冰雪观影、电竞滑雪、冰雪装备、冰雪知识竞猜、冰雪主题打卡等7个不同的体验项目。在冰雪教练的指导下，很多第一次接触冰雪运动的同学很快沉浸在各个项目的体验中。\r\n\r\n“带动三亿人参与冰雪运动”，是北京携手张家口申办2022年冬奥会时，向国际社会作出的郑重承诺。如今，以筹办北京冬奥会为契机，冰雪运动“南展西扩东进”正着力实施，更多群众参与冰雪运动，更多地方唱响了冰雪旋律。\r\n\r\n地处闹市的北京国贸溜冰场已有20多年的历史。一到中午，不少附近的上班族经常结伴前来，放松身心。冰场还是孩子们向往的乐园，放学后背着书包来练习滑冰的小朋友也是一道亮丽风景。2018年来到冰场担任花样滑冰教练的宁芳很有感触，“喜爱滑冰的人越来越多了。”据介绍，目前冰场的客流量已经恢复接近2019年的水平，每周有10多个青少年固定跟着她练习花样滑冰。\r\n\r\n吉林冰雪资源丰富，冰雪运动备受群众喜爱。近年来，全省大力开展“冰雪运动进校园”活动，目前已有557所冰雪运动特色学校。吉林省教育厅体育卫生与艺术教育处处长胡仁友表示，冰雪运动充满魅力，深受孩子们喜爱。为了让冰雪运动唱响“四季歌”，全省不少中小学还开设了轮滑课和越野滑轮课。“这些项目和冰雪运动相似度高达70%以上，可以在无冰无雪时作为替代项目开展。”胡仁友说。\r\n\r\n运动发展“青春范”，吸引更多少年\r\n\r\n日前，首届上海市青少年滑雪公开赛圆满落幕。比赛采用一台台室内滑雪模拟机替代雪道，150名参赛选手展开激烈比拼。值得一提的是，本次赛事8名裁判员全部为一级以上裁判员，其中有2名国家级裁判员。裁判长刘仁辉在2月份的“相约北京”高山滑雪系列赛事中担任竞赛长。\r\n\r\n“除了在裁判员上力求专业，我们还在体育展示、氛围布置上下足功夫，为的就是激发青少年的比赛热情，让他们感受浓浓的仪式感。”上海市冰雪运动协会副秘书长徐越说。\r\n\r\n如今，越来越多的冰雪专业人才正利用他们的专业知识和资源，为项目推广打开新局面。4月15日，“体教融合·植根计划”首个实验基地落户北京石景山区景山学校远洋分校，中国滑冰协会主席、中国国家短道速滑队原主教练李琰专门赶来为该校授牌。此次启动的“植根计划”从两个层面着手：一是出版《滑冰技能等级标准》《滑冰教练员教程》等教材，规范校园的滑冰教学，帮助孩子们打牢基础；二是搭建“上层”赛事体系，创办全国滑冰小学校际联赛、全国滑冰U系列中学校际联赛，给爱滑冰的孩子提供更多参赛机会。\r\n\r\n在李琰看来，在群众体育层面，专业人士能做的工作很多，“竞技体育与群众体育在某些层面‘对接’，可以起到互促双赢的效果”。近些年，为了推广滑冰运动，中国滑冰协会做了很多基础性工作。此前，协会帮助石景山区体育教师上冰雪，在短道速滑世界冠军赵楠楠的指导下，超过200名体育老师掌握了基本的滑冰技能，让“冰雪运动进校园”有了更多助力。\r\n\r\n李琰认为，协会应成为竞技体育与群众体育、体育和教育之间的桥梁。“现在不以金牌多少做衡量，我能够看到孩子们的笑脸，看到他们参加体育运动时天性的释放，看到他们摔倒后再爬起来的那股意志力，我们现在的工作是‘前人栽树，后人乘凉’，意义更大。”', 1, 98, 1, 1, 101, '2021-05-04 21:14:34', '2021-05-04 21:14:34');
INSERT INTO `news` VALUES (1066, '恭喜！纳达尔仅丢三局轻松晋级，谈观众再和德约科维奇针锋相对', '纳达尔仅丢三局轻松晋级', 103001, '蒙特卡洛网球大师赛继续进行。男单第二轮，赛会11冠王纳达尔仅丢三局，以6-1、6-2击败德尔波尼斯，轻松晋级16强，下一轮对阵迪米特洛夫。有意思的是，纳达尔表示，没有观众实在太遗憾了，而德约科维奇持不同的态度，他认为观众的缺失为球员们创造了特别好的训练条件，两大巨头“针锋相对”，竞争进入白热化。\r\n\r\n蒙特卡洛大师赛是新赛季首个高级别红土赛事，德约科维奇和纳达尔复出参赛，世界排名第二的梅德维德夫意外退赛，费德勒和蒂姆等高手并未报名。很显然，分居不同半区的德约科维奇和纳达尔是夺冠热门，两人有望会师决赛。德约科维奇率先完成首秀，他以2-0淘汰辛纳。随后，纳达尔在第二轮完成首秀，面对实力远不如自己的德尔波尼斯，纳达尔仅丢三局，以6-1、6-2轻松获胜，晋级16强。这是纳达尔第17次出战蒙特卡洛大师赛第二轮比赛，他保持17战全胜的骄人战绩。赛会11冠王纳达尔将在下一轮对阵迪米特洛夫，后者以7-6、6-4淘汰了查迪。有意思的是，纳达尔在接受采访时表示，训练后无法在这里看到观众，实在是太遗憾了。而德约科维奇则持不同意见，他认为，观众的缺失为球员们创造了特别好的训练条件，两大巨头再一次“针锋相对”，竞争进入白热化。其实，从现实条件看，空场比赛当然是最佳选择，至于两人的表态可能是基于不同的出发点，没有对错之分。', 1, 77, 1, 1, 101, '2021-05-04 21:14:32', '2021-05-04 21:14:32');
INSERT INTO `news` VALUES (1067, '法网击败纳达尔夺冠，蒂姆相信休息18天后能够做到', '法网击败纳达尔夺冠', 103001, '美网冠军蒂姆最近宣布，由于“还没有准备好”，他将推迟他的红土巡回赛，但他有信心在法网时达到自己的巅峰。\r\n\r\n18天身心恢复计划\r\n\r\n现世界排名第四的蒂姆不会出现本赛季首项红土大师赛蒙特卡洛大师赛上，他认为只有在自己恢复到100%的状态时，才有理由重返巡回赛。现年27岁的蒂姆经历了一个喜忧参半的赛季开局，目前战绩5胜4负。他的最佳表现是在澳大利亚网球公开赛上，在进入第四轮后输给了迪米特洛夫。之后转战中东赛季也表现不佳，在多哈的1/4决赛中输给了阿古特，在迪拜的首场比赛中又输给了哈里斯。在迪拜之后，蒂姆选择离开网球以恢复精神和身体方面的疲劳。奥地利人最近几个月一直受到脚部间歇性疼痛问题的困扰。在最近采访中，蒂姆透露他总共已经有18天没有打网球，他认为这是他未来几个月在赛场取得佳绩的有益沉淀。他说:“我什么也没做，只是从这一切中抽身出来，很少看网球比赛，只是有一些全新的想法。”但是，蒂姆也表示自己并没有完全远离网球，“我认为这是最重要的事情，因为即使你休息一下，但你仍然在研究结果，或者观看迈阿密的比赛，我认为你不能真正摆脱它，但这是必须做的…一个星期什么都不做，甚至不去想网球，不看网球，然后在某个时候你又会感到饥饿。”蒂姆希望能够在法网公开赛前重新找回对成功的渴望，他曾多次接近法网冠军。蒂姆在罗兰加洛斯已经连续五次进入四分之一决赛或者更好，虽然美网成就了个人首个大满贯冠军，但是他在罗兰加洛斯赢得的比赛比其他任何大满贯都多。他在2018年和2019年连续两年打进决赛却都输给了纳达尔。“我还没准备好去蒙特卡洛。目前的水平是如此之高，只有从100%开始才有意义。所以我决定跳过蒙特卡洛，从贝尔格莱德开始。”蒂姆解释了他接下来几周的计划。贝尔格莱德公开赛将于4月19日开幕。显然，蒂姆的100%恢复是具备和红土天王纳达尔竞争的实力。在澳洲赛季的时候蒂姆就放出豪言要在法网击败纳达尔，而现在休息了18天后他更加坚定了信心，“最大的目标当然是法国网球公开赛。我确信在那里如果我聪明地工作，我将处于最佳状态，无论是在身体上还是在比赛中。”\r\n\r\n在蒂姆的职业生涯中，蒂姆已经获得了17个ATP冠军，愿他法网好运！', 1, 78, 1, 1, 101, '2021-05-04 21:14:29', '2021-05-04 21:14:29');
INSERT INTO `news` VALUES (1068, '官宣！2021法网延期举行！连续两年推迟，恐影响温网备战！', '恐影响温网备战！', 103001, '根据法网官方发布的最新消息，2021赛季法国网球公开赛将会延期举行，新的比赛时间为5月24日到6月13日，其中正赛比赛时间为5月30日到6月13日。受到法国疫情反弹的影响，法国再次进入了全国封城。在此前就曾经有消息称，今年法网将会延期举行。如今官方公布了最新的消息，法网将会推迟一周举办。这将会是法网连续第2年推迟，在去年法网就曾经因为疫情推迟到了9月份。值得一提的是，在今年初的澳网也曾经因为疫情原因推迟，如今法网再次因为疫情推迟。连续两项大满贯赛事都无法按时举办，在这个大流行时代能够正常举办一项网球赛事已经成为一种奢望。虽然仅仅推迟一周的时间，但是法网的推迟可能影响到整个巡回赛的进程。按照本赛季原定赛程安排，在法网之后三周将会是温网的比赛。如今法网推迟一周举办，法网和温网之间只有两周的时间，如此短暂的时间球员需要从红土过渡到草地赛场，显然没有足够的热身时间。', 1, 67, 1, 1, 101, '2021-05-04 21:14:26', '2021-05-04 21:14:26');
INSERT INTO `news` VALUES (1069, '关注中国女足！', '奥运会女足抽签今日16：00举行', 102007, '东京奥运会男足、女足决赛圈抽签仪式将于今日16:00举行。\r\n\r\n女足有12支队伍参赛，分成3组，每组4队，小组前2和成绩最好的两个小组第3晋级淘汰赛。根据比赛规则，同一大洲球队不会分在一个小组。\r\n\r\n女足奥运会分档：\r\n\r\n第一档：日本(世界第11)、美国(第1)、荷兰(第3)\r\n\r\n第二档：瑞典(第5)、英格兰(第6)、巴西(第7)\r\n\r\n第三档：加拿大(第8)、澳大利亚(第9)、中国（第14)\r\n\r\n第四档：新西兰(第22)、智利(第37)、赞比亚(第104)\r\n\r\n东京奥运会女足比赛举行时间为7月21-至8月6日。', 1, 430, 1, 1, 101, '2021-05-05 18:15:37', '2021-05-05 18:15:37');
INSERT INTO `news` VALUES (1070, '女足奥运分组前瞻：世界第一的美国铁定同组，晋级8强只有1个障碍', '晋级8强只有1个障碍', 102007, '，2020年东京奥运会女足分组抽签将揭开答案。在本月中旬刚刚结束的两轮附加赛中，中国女足以两回合4:3的总比分淘汰韩国队晋级正赛，而在另外一场附加赛中智利女足以两回合2:1的总比分淘汰喀麦隆晋级。值得注意的是，由于今年成绩的不断下滑，中国女足被分在第三档，而前世界冠军、本次奥运会的东道主日本队则被分到第一档，美国队和荷兰队则是上届世界杯的冠亚军。作为曾经世界强敌的巴西队近两年和中国队一样出现青黄不接的情况，实力也下滑到第三档。根据奥运会分组抽签的规则要求，首先实行的是同一大洲回避的原则。由于中国队队、日本队和澳大利亚队都属于亚足联协会成员，因此在第一档、第二档中，中国女足首先不会和日本队、澳大利亚队同组。在这种情况下，二挡里面瑞典队或者英格兰队必然有一个要与中国队一个小组，这样就排斥掉了掉一档的荷兰队。因此，中国队的第一档对手铁定是当今世界第一的美国队。至于第三档的球队，无论抽到哪支都不会给中国队造成威胁。根据这个原则，中国队的同组对手应该是：美国队、瑞典/英格兰、新西兰/赞比亚/智利。在这样一个小组里面，中国队想要小组出线还是面临不小的压力，但是也并不是没有可能。首先美国队的绝对实力让其他球队很难在她身上拿到分数；其次是第三档的三支球队难以给中国队形成威胁，无论面对谁拿下三分都不是问题。而在二档球队里面，英格兰队虽然进步明显，但是对中国队并没有绝对优势，中国女足至少会保持不败。只有和瑞典对阵时将面临较大的考验，这也是女足姑娘们面临的唯一障碍。不管如何，以女足目前的实力来说，晋级8强仍有较大的希望。如果运气好的话，并不排除更进一步的可能。期待着女足姑娘们在奥运会上为国人带来更好的成绩吧！', 1, 190, 1, 1, 101, '2021-05-04 21:14:03', '2021-05-04 21:14:03');
INSERT INTO `news` VALUES (1071, '有颜有实力！锡马樱花雨再现人在画中跑 801人实现破三', '锡马樱花雨再现人在画中跑', 103005, '021年无锡马拉松圆满落幕。共有27000名跑友参赛。最终，男子组的比赛中，李子成与关思扬的争夺延续到了冲刺的最后时刻，李子成战胜关思扬，实现在无锡四连冠。女子组冠军被丁常琴获得。而据组委会消息，破三（跑进3小时）的选手人数达到了801人，创造了新高。\r\n\r\n此前，2019年锡马破三人数最高达到了740人，刷新了国内单场赛事破三的纪录。而今年，锡马的赛场上，选手们再度跑出了佳绩，刷新了破三纪录。\r\n\r\n', 1, 88, 1, 1, 101, '2021-05-04 21:13:58', '2021-05-04 21:13:58');
INSERT INTO `news` VALUES (1072, '冲刺好戏！徐州马拉松彭建华夺冠进军奥运 无锡马拉松李子成险胜关思扬', '徐州马拉松', 103005, '十数场马拉松在全国遍地开花。徐州、无锡、杨凌、海口、自贡、南京、桐庐、温州...全国跑友体验奔跑的乐趣。\r\n\r\n经历了4月10日的厦门马拉松，不少跑友直接转场前往徐州、无锡等地，背靠背参赛。从厦门前往无锡的飞机上几乎半数以上都是背靠背参赛的跑友，有跑友开玩笑说，“肌贴都不用撕掉了”。\r\n\r\n今天的两场重头戏比得都非常激烈，直到终点处才最终决出胜负。\r\n\r\n徐州马拉松\r\n\r\n徐州马拉松是全国马拉松锦标赛暨奥运会选拔赛，全程10000人，半程5000人，欢乐跑5000人。男、女前三名分别奖励人民币20000元、10000元、5000元。对入选奥运会马拉松比赛资格的运动员进行奖励第一名：30000元、第二名：20000元、第三名：10000元。破赛会纪录奖(只奖励第一名)：男子跑进2小时10分31秒（不含2小时10分31秒）将奖励20000元，女子跑进2小时31分06秒（不含2:31:06）奖励20000元人民币。最终比赛前三名（本次徐州赛达奥运标或此前已达奥运标）将获得东京奥运会参赛资格。', 1, 23, 1, 1, 101, '2021-05-04 21:13:54', '2021-05-04 21:13:54');
INSERT INTO `news` VALUES (1073, '澳游泳锦标赛霍顿一塌糊涂 莱德基终结者状态大勇', '霍顿一塌糊涂 莱德基终结者状态大勇', 103006, '2021年澳大利亚游泳锦标赛4月14日到18日在昆士兰的南港进行，本次比赛并不是澳大利亚的奥运游泳选拔赛，不过对于一年多没比过大型国内赛事的澳大利亚游泳界来说，基本上一线选手全部到齐，只是每个人的表现迥异。马克-霍顿走过场一般毫无作为，查尔莫斯称霸男子短距离自由泳，在世锦赛女子400自决赛让莱德基尝到世锦赛单项首败的蒂特穆斯，埃玛-麦基昂和凯丽-麦基昂等名将，均至少拿到两项冠军。本次澳洲锦标赛依然仿照东京奥运会晚上预赛、早上决赛的赛制进行，作为孙杨最主要对手之一的马克-霍顿，参加了本次比赛，但他的表现却让人迷之不解，或许本人就没想好好比。', 1, 16, 1, 1, 101, '2021-05-04 21:13:50', '2021-05-04 21:13:50');
INSERT INTO `news` VALUES (1074, '日本奥运选拔赛池江夺四冠 称霸短距离自由泳蝶泳', '池江', 103006, '第97届日本游泳全国锦标赛暨奥运选拔赛收官日争夺，池江璃花子再添两金，50米自由泳和蝶泳均拿下冠军，并双双创造日本大学生纪录，本次选拔赛个人包揽短距离自由泳和蝶泳四项冠军。池江璃花子则在本次选拔赛上已经先后拿到100米蝶泳和100米自由泳两项冠军，不过均未能达到单项参赛成绩，但依然获得接力的奥运资格。今日池江连战50米自由泳和非奥运项目50米蝶泳两个项目的比赛。\r\n\r\n50米自由泳，池江璃花子半决赛时游出24秒87打破日本大学生纪录。到了决赛，池江璃花子再接再厉，游出24秒84进一步改写日本大学生纪录，获得该项目冠军，不过这一成绩并未能达到24秒21的日本奥运单项参赛标准。\r\n\r\n50米蝶泳，池江璃花子以25秒56的成绩再夺一冠，创造新的日本大学纪录。虽然未能获得单项资格，但池江本次选拔赛个人独取四冠，短节目自由泳和蝶泳全部拿下冠军，状态恢复相当不错。\r\n\r\n池江璃花子2019年2月被确诊患上白血病，经过10个月的治疗后康复，在2020年8月正式复出。本次选拔赛，池江璃花子的目标本来只是能在所有四个项目中闯入决赛，没想到却一举包揽四冠。东京奥运，池江璃花子将出战4*100米自由泳和4*100米混合泳两个接力项目。', 1, 19, 1, 1, 101, '2021-05-04 21:13:48', '2021-05-04 21:13:48');
INSERT INTO `news` VALUES (1075, '国际泳联：跳水世界杯奥运测试赛5月1日东京重启', '原定于4月18日至23日在日本东京举行的跳水世界杯暨奥运资格赛和奥运测试赛将延期至5月1日至6日举行。\r\n\r\n', 103006, '国际泳联在新闻公告中表示，这是国际泳联、日本泳协、东京奥组委、日本政府、东京都政府和国际奥委会进行了卓有成效的磋商后做出的决定，确定了5月所有参加跳水世界杯的各代表团在日本的详细入境程序和住宿等安排。\r\n\r\n此外，原定于5月1日至4日在东京举行的奥运会花样游泳奥运资格赛目前在评估中，国际泳联及相关机构将在未来几天做出决定。\r\n\r\n而原定于5月29日至30日在日本福冈举行的马拉松游泳奥运会资格赛则易地举行，国际泳联与日本主办方达成一致意见，应葡萄牙泳协的要求，比赛将于6月19日至20日在塞图巴尔举行，此地曾举办2012年和2016年奥运会资格赛。\r\n\r\n国际泳联表示，举办奥运资格赛必须要提供公平、安全的竞争环境，感谢东京奥组委、日本政府和国际奥委会对赛事安排的承诺，并重申对今年夏季东京奥运会成功举办的信心。', 1, 73, 1, 1, 101, '2021-05-04 21:13:45', '2021-05-04 21:13:45');
INSERT INTO `news` VALUES (1076, '国乒备战奥运实录 樊振东陈梦孙颖莎三大王牌势争冠', '东京奥运会倒计时100天，看国乒备战实录，感受荣耀时刻\r\n\r\n承载着中国体育辉煌和荣耀的中国乒乓球队，永不满足。\r\n\r\n北京时间4月14日，是东京奥运会倒计时100天的日子。中国乒乓球队正在摩拳擦掌备战。', 103009, '虽然奥运之前可能没有国际大赛，但国乒信心十足，包括樊振东、陈梦、孙颖莎在内的三大王牌，有望成为新科奥运冠军。\r\n\r\n老将许昕和刘诗雯搭档混双，拿到东京奥运会入场券。作为队内的全能型球员，许昕在双打比赛中作用突出。东京奥运会将产生第一枚乒乓球混双金牌，国乒志在必得。\r\n\r\n国乒奥运阵容预想：男单三大主力是马龙、樊振东、许昕，女乒最有竞争力的是陈梦、刘诗雯、孙颖莎。', 1, 65, 1, 1, 101, '2021-05-04 21:13:38', '2021-05-04 21:13:38');
INSERT INTO `news` VALUES (1077, '无惧延期!中国乒协将办一系列强强对抗赛事以促备战', '无惧延期!', 103009, '今天，WTT世界乒联发布消息，确认将会在东京奥运会后举办休斯顿世乒赛和WTT中国赛事汇等一系列国际赛事。WTT世界乒联介绍到，“考虑目前的情况，运动员们都在备战奥运会，以及围绕国际旅行和训练均有一定的不确定性和限制，我们认为在晚一些的日期举办比赛将更加公平、安全。保障大家的安全健康仍是我们的第一要务。”\r\n\r\n其实目前国外疫情反复，特别是欧美疫情尚属严重，包括羽毛球苏迪曼杯、东京跳水世界杯在内的多个项目的国际赛事均宣布延期或取消。中国赛事汇放在奥运会后举办也是情理之中。\r\n\r\n对于正在成都乒乓球训练基地全力备战东京奥运会的中国乒乓球队来说，赛事延期并不会影响中国队整体的备战节奏。\r\n\r\n全运会预赛结束后，中国乒乓球队在成都乒乓球训练基地展开针对性的封闭训练。中国乒协秘书长、中国乒乓球队男队主教练秦志戬介绍说：“现在队伍刚刚完成了封闭训练的第一个单元，整体状态很好。现阶段我们还是保持自己的备战节奏，有计划地展开训练、比赛计划。男队在备战的同时也注重伤病的防护，马龙、樊振东、林高远、梁靖崑和王楚钦身体状态都不错，之前在全运会预赛受伤的许昕也已完全康复。”\r\n\r\n女队主教练李隼表示，“女队在封闭集训的主要目标是提高主力队员强对抗的能力。刘诗雯、陈梦、孙颖莎、王曼昱等主力在这次集训中都展示出了要全力冲刺奥运的决心。尤其是前段时间饱受伤病困扰的刘诗雯，通过全运会预赛和封闭集训的恢复，状态比想象更好。”', 1, 95, 1, 1, 101, '2021-05-04 21:13:35', '2021-05-04 21:13:35');
INSERT INTO `news` VALUES (1078, '国际乒联排名中国独一档 樊振东陈梦各自领跑榜单', '樊振东陈梦各自领跑榜单', 103009, '国际乒联更新了2021年第14周的世界排名榜单。\r\n\r\n男单方面，樊振东以11094分领跑榜单，许昕和马龙紧随其后，三人包揽前三名，积分均超过1万分形成了第一梯队。排名最高的外协会男单球员是日本选手张本智和，他目前排在世界第四位。此外，另外两位主力小将林高远和梁靖昆也跻身前十，两人分别排在第五和第十。女单方面，国乒共有6位球员跻身世界前十，国乒主力陈梦以1352分的分差，继续稳居世界第一；孙颖莎、王曼昱、丁宁、朱雨玲以及刘诗雯则依次包揽了女单榜单3-7位。此外，凭借两站挑战赛冠军积分，日本女乒一姐伊藤美诚目前世界排名仍居世界第二。男双方面，世界第一被韩国组合郑荣植/李尚洙摘得。国乒两对搭档梁靖昆/林高远和许昕/樊振东，目前分别位列男双第二及第三名；世乒赛冠军组合马龙/王楚钦则排在第16位。\r\n\r\n女双方面，陈梦/王曼昱及王曼昱/孙颖莎两对组合分列5-6位。混双榜单头名被中国台北组合郑怡静/林昀儒摘得，日本组合水谷隼/伊藤美诚紧随其后，国乒“昕雯联播”组合许昕/刘诗雯则名列第三。', 1, 99, 1, 1, 101, '2021-05-04 21:13:31', '2021-05-04 21:13:31');
INSERT INTO `news` VALUES (1079, '曝小卡右脚酸痛继续缺席，一周后复查！', '快船夺冠概率却26％最高', 101001, 'NBA名记查拉尼亚报道，快船队球星科怀-莱昂纳德因为右脚酸痛将继续缺席，他会在一周后复查。其实他在4月10日对阵火箭后一直伤停到4月19日，没想到复出打了森林狼一场就又因为这个伤势缺席了。\r\n\r\n纽约邮报记者马克-斯坦恩曝出了一些细节，他表示科怀-莱昂纳德其实一直都在治疗自己的右脚，他对阵森林狼也是带伤出战（23分钟），他的缺席对于快船来说是一个重大的打击，他将在下周进行复查归期难定。虽然快船的官方表示科怀-莱昂纳德会因为右脚酸痛伤势缺席至少一周，他将在下周接受复查，若复查痊愈他就会复出。小卡本赛季场均34.4分钟，得到25.7分6.7篮板5.1助攻1.7抢断，命中率51.6%，三分命中率39.3%，他仍是快船得分最多的球员，乔治场均只有23.6分。本赛季他已经缺席了整整13场比赛。上周莱昂纳德是因为同样的伤势缺席了四场比赛，也就是说他上周不是因为负荷管理缺席，他是实实在在右脚有伤。不过上周的四场比赛乔治都发挥出色连续4场30+拿下3胜1负的不俗战绩，那么说小卡的缺席对快船不会有太大的影响。所以快船目前的夺冠概率依旧是联盟第一，他们的夺冠概率高达26%。而此前联盟第一的夺冠概率篮网下滑到13%，因为杜兰特、哈登相继再次受伤，哈登如今也无限期伤停了，而且快船核心伤停后战绩有明显下滑。', 1, 111, 1, 1, 101, '2021-05-04 21:13:27', '2021-05-04 21:13:27');
INSERT INTO `news` VALUES (1080, 'CCTV5直播辽篮VS浙江第1战，蒋兴权帮杨鸣，刘维伟没秘密，谁能赢', '辽篮VS浙江', 101002, 'CCTV5今晚19:30分直播辽宁男篮VS浙江男篮的第一场比赛。目前辽宁队的优势就是外援配置和阵容方面比较全面，郭艾伦也可以重新上场，最近几场比赛赵继伟发挥的也很好。缺点是郭艾伦和新外援这几场比赛的节奏不太好，命中率有点低。张镇麟这种新人季后赛直接哑火了，发挥很低迷。还有就是辽宁队的三分球实在是太差了，不像这个级别的球队该有的样子。\r\n\r\n浙江赢下青岛进入四强后，他们创造了队史纪录，所以刘维伟在休息室做了激动人心的演讲。浙江队的优势就是队员年轻，敢闯敢拼。吴前拿了常规赛MVP之后像打了鸡血一样想证明自己。缺点就是年轻比较冲动，很容易跟着对手的节奏走。蒋兴权帮杨鸣，刘维伟没秘密。浙江队的青年军属性，都被蒋兴权了如指掌，让老爷子帮杨鸣就是最大的失策，刘维伟和浙江没秘密，辽宁直接赢情报战。不过杨鸣的排兵布阵耐人寻味，弗格代替郭艾伦进首发，弗格和赵继伟就是首发后卫，郭艾伦彻底跌出了首发，如果今年辽宁夺冠与否不管，只要弗格好于郭艾伦，郭艾伦就会淡出辽宁的首发阵容。辽宁新赛季夺冠路上最大的拦路虎是同在下半区的浙江队，目前猜测今年夺冠最热门的球队，就是下半区的辽宁和浙江，关键是半决赛辽宁和浙江，三场二胜制，到底谁会被淘汰，若辽宁淘汰了浙江，辽宁夺冠的概率就有70%，若浙江淘汰了辽宁，那辽宁就与冠军无缘。反观浙江男篮的内线就比较单薄，张大宇算不上联盟一流内线，朱旭航和赖俊豪的特点更偏重于四号位，而刘泽一虽然是内线特点的球员，但在身高上也有明显的短板。浙江进总决赛可能性不大，外援质量一般，刘维伟怕蒋兴权让球队先天不足，青年队打球真的不老练。', 1, 17, 1, 1, 101, '2021-05-04 21:13:21', '2021-05-04 21:13:21');

-- ----------------------------
-- Table structure for oppose_history
-- ----------------------------
DROP TABLE IF EXISTS `oppose_history`;
CREATE TABLE `oppose_history`  (
  `oppose_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '踩id',
  `user_id` bigint(32) NULL DEFAULT NULL COMMENT '踩用户id',
  `new_id` bigint(32) NULL DEFAULT NULL COMMENT '踩新闻id',
  `oppose_time` datetime(0) NULL DEFAULT NULL COMMENT '踩时间',
  PRIMARY KEY (`oppose_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 201 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '踩历史' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oppose_history
-- ----------------------------
INSERT INTO `oppose_history` VALUES (101, 201, 1002, '2021-04-01 15:18:19');
INSERT INTO `oppose_history` VALUES (102, 301, 1003, '2021-03-29 20:10:49');
INSERT INTO `oppose_history` VALUES (103, 301, 1001, '2021-04-01 15:18:39');
INSERT INTO `oppose_history` VALUES (104, 4512566170025984, 1050, '2021-04-25 13:26:38');
INSERT INTO `oppose_history` VALUES (105, 4512566170025984, 1040, '2021-04-25 13:26:40');
INSERT INTO `oppose_history` VALUES (106, 5744592866844332, 1024, '2021-04-25 13:26:42');
INSERT INTO `oppose_history` VALUES (107, 5744592866844332, 1022, '2021-04-25 13:22:29');
INSERT INTO `oppose_history` VALUES (108, 5746686520543108, 1044, '2021-04-25 13:26:46');
INSERT INTO `oppose_history` VALUES (109, 5746686520543108, 1031, '2021-04-25 13:27:01');
INSERT INTO `oppose_history` VALUES (110, 5746687128004140, 1034, '2021-04-25 13:27:04');
INSERT INTO `oppose_history` VALUES (111, 5746687128004140, 1035, '2021-04-25 13:27:18');
INSERT INTO `oppose_history` VALUES (112, 5746719496328924, 1052, '2021-04-25 13:27:31');
INSERT INTO `oppose_history` VALUES (113, 5746719496328924, 1051, '2021-04-25 13:27:16');
INSERT INTO `oppose_history` VALUES (114, 5747184000103856, 1050, '2021-04-25 13:27:33');
INSERT INTO `oppose_history` VALUES (115, 5747184000103856, 1047, '2021-04-25 13:27:39');
INSERT INTO `oppose_history` VALUES (116, 5747184000103856, 1049, '2021-04-25 13:26:32');
INSERT INTO `oppose_history` VALUES (117, 5747184000103857, 1039, '2021-04-25 13:27:41');
INSERT INTO `oppose_history` VALUES (118, 5747184000103857, 1038, '2021-04-25 13:26:58');
INSERT INTO `oppose_history` VALUES (119, 5769745737743028, 1029, '2021-04-25 13:27:44');
INSERT INTO `oppose_history` VALUES (120, 5769745737743028, 1028, '2021-04-25 13:27:06');
INSERT INTO `oppose_history` VALUES (121, 5769745737743030, 1005, '2021-04-25 13:27:21');
INSERT INTO `oppose_history` VALUES (122, 5769745737743030, 1007, '2021-04-25 13:27:50');
INSERT INTO `oppose_history` VALUES (123, 5769745737743035, 1066, '2021-04-25 13:27:47');
INSERT INTO `oppose_history` VALUES (124, 5769745737743035, 1067, '2021-04-25 13:27:36');
INSERT INTO `oppose_history` VALUES (125, 5769745737743036, 1068, '2021-04-25 13:27:11');
INSERT INTO `oppose_history` VALUES (126, 5769745737743036, 1069, '2021-04-25 13:27:28');
INSERT INTO `oppose_history` VALUES (127, 5769745737743037, 1074, '2021-04-25 13:27:13');
INSERT INTO `oppose_history` VALUES (128, 5769745737743037, 1073, '2021-04-25 13:27:25');
INSERT INTO `oppose_history` VALUES (129, 5769745737743038, 1077, '2021-04-25 13:27:23');
INSERT INTO `oppose_history` VALUES (130, 5769745737743038, 1076, '2021-04-25 13:26:52');
INSERT INTO `oppose_history` VALUES (131, 5769745737743039, 1062, '2021-04-25 13:26:54');
INSERT INTO `oppose_history` VALUES (132, 5769745737743039, 1066, '2021-04-25 13:26:50');
INSERT INTO `oppose_history` VALUES (133, 5769745737743040, 1022, '2021-04-25 13:26:48');
INSERT INTO `oppose_history` VALUES (134, 5769745737743040, 1011, '2021-04-25 13:26:27');
INSERT INTO `oppose_history` VALUES (135, 5769745737743041, 1005, '2021-04-25 13:26:30');
INSERT INTO `oppose_history` VALUES (136, 5769745737743041, 1009, '2021-04-25 13:26:22');
INSERT INTO `oppose_history` VALUES (137, 5769745737743042, 1033, '2021-04-25 13:26:24');
INSERT INTO `oppose_history` VALUES (138, 5769745737743042, 1027, '2021-04-25 13:26:20');
INSERT INTO `oppose_history` VALUES (139, 5775646896559288, 1080, '2021-04-25 13:26:06');
INSERT INTO `oppose_history` VALUES (140, 5775646896559288, 1055, '2021-04-25 13:26:17');
INSERT INTO `oppose_history` VALUES (141, 4512566170025984, 1010, '2021-04-25 14:50:11');
INSERT INTO `oppose_history` VALUES (142, 4512566170025984, 1022, '2021-04-25 14:50:14');
INSERT INTO `oppose_history` VALUES (143, 5744592866844332, 1044, '2021-04-25 14:50:18');
INSERT INTO `oppose_history` VALUES (144, 5744592866844332, 1052, '2021-04-25 14:50:40');
INSERT INTO `oppose_history` VALUES (145, 5746686520543108, 1032, '2021-04-25 14:50:42');
INSERT INTO `oppose_history` VALUES (146, 5746686520543108, 1001, '2021-04-25 14:50:45');
INSERT INTO `oppose_history` VALUES (147, 5746687128004140, 1005, '2021-04-25 14:50:47');
INSERT INTO `oppose_history` VALUES (148, 5746687128004140, 1004, '2021-04-25 14:47:10');
INSERT INTO `oppose_history` VALUES (149, 5746687128004140, 1007, '2021-04-25 14:51:07');
INSERT INTO `oppose_history` VALUES (150, 5746687128004140, 1010, '2021-04-25 14:51:20');
INSERT INTO `oppose_history` VALUES (151, 5746719496328924, 1022, '2021-04-25 15:14:00');
INSERT INTO `oppose_history` VALUES (152, 5746719496328924, 1042, '2021-04-25 15:14:02');
INSERT INTO `oppose_history` VALUES (153, 5746719496328924, 1077, '2021-04-25 15:14:07');
INSERT INTO `oppose_history` VALUES (154, 5746719496328924, 1005, '2021-04-25 15:14:04');
INSERT INTO `oppose_history` VALUES (155, 5747184000103856, 1002, '2021-04-25 15:48:43');
INSERT INTO `oppose_history` VALUES (156, 5747184000103856, 1005, '2021-04-25 15:48:40');
INSERT INTO `oppose_history` VALUES (157, 5747184000103856, 1001, '2021-04-25 15:48:38');
INSERT INTO `oppose_history` VALUES (158, 5747184000103857, 1054, '2021-04-25 15:49:10');
INSERT INTO `oppose_history` VALUES (159, 5747184000103857, 1002, '2021-04-25 15:49:12');
INSERT INTO `oppose_history` VALUES (160, 5747184000103857, 1001, '2021-04-25 15:48:46');
INSERT INTO `oppose_history` VALUES (161, 5769745737743028, 1062, '2021-04-25 15:49:23');
INSERT INTO `oppose_history` VALUES (162, 5769745737743028, 1001, '2021-04-25 15:49:19');
INSERT INTO `oppose_history` VALUES (163, 5769745737743028, 1002, '2021-04-25 15:49:22');
INSERT INTO `oppose_history` VALUES (164, 5769745737743030, 1001, '2021-04-25 15:49:37');
INSERT INTO `oppose_history` VALUES (165, 5769745737743030, 1052, '2021-04-25 15:49:40');
INSERT INTO `oppose_history` VALUES (166, 5769745737743030, 1024, '2021-04-25 15:49:42');
INSERT INTO `oppose_history` VALUES (167, 5769745737743035, 1001, '2021-04-25 15:49:57');
INSERT INTO `oppose_history` VALUES (168, 5769745737743035, 1023, '2021-04-25 15:50:00');
INSERT INTO `oppose_history` VALUES (169, 5769745737743035, 1042, '2021-04-25 15:50:02');
INSERT INTO `oppose_history` VALUES (170, 5769745737743036, 1003, '2021-04-25 15:50:15');
INSERT INTO `oppose_history` VALUES (171, 5769745737743036, 1055, '2021-04-25 15:50:17');
INSERT INTO `oppose_history` VALUES (172, 5769745737743036, 1005, '2021-04-25 15:50:12');
INSERT INTO `oppose_history` VALUES (173, 5769745737743037, 1022, '2021-04-25 15:50:52');
INSERT INTO `oppose_history` VALUES (174, 5769745737743037, 1035, '2021-04-25 15:50:54');
INSERT INTO `oppose_history` VALUES (175, 5769745737743037, 1003, '2021-04-25 15:50:37');
INSERT INTO `oppose_history` VALUES (176, 5769745737743037, 1043, '2021-04-25 15:50:40');
INSERT INTO `oppose_history` VALUES (177, 5769745737743038, 1034, '2021-04-25 15:51:11');
INSERT INTO `oppose_history` VALUES (178, 5769745737743038, 1048, '2021-04-25 15:51:13');
INSERT INTO `oppose_history` VALUES (179, 5769745737743038, 1004, '2021-04-25 15:51:05');
INSERT INTO `oppose_history` VALUES (180, 5769745737743038, 1075, '2021-04-25 15:51:09');
INSERT INTO `oppose_history` VALUES (181, 5769745737743039, 1002, '2021-04-25 15:51:35');
INSERT INTO `oppose_history` VALUES (182, 5769745737743039, 1055, '2021-04-25 15:51:37');
INSERT INTO `oppose_history` VALUES (183, 5769745737743039, 1033, '2021-04-25 15:53:28');
INSERT INTO `oppose_history` VALUES (184, 5769745737743041, 1024, '2021-04-25 15:53:56');
INSERT INTO `oppose_history` VALUES (185, 5769745737743041, 1038, '2021-04-25 15:53:59');
INSERT INTO `oppose_history` VALUES (186, 5769745737743041, 1049, '2021-04-25 15:54:02');
INSERT INTO `oppose_history` VALUES (187, 5769745737743041, 1057, '2021-04-25 15:54:04');
INSERT INTO `oppose_history` VALUES (188, 5769745737743040, 1001, '2021-04-25 15:53:39');
INSERT INTO `oppose_history` VALUES (189, 5769745737743040, 1052, '2021-04-25 15:53:44');
INSERT INTO `oppose_history` VALUES (190, 5769745737743042, 1020, '2021-04-25 15:54:25');
INSERT INTO `oppose_history` VALUES (191, 5769745737743042, 1001, '2021-04-25 15:54:28');
INSERT INTO `oppose_history` VALUES (192, 5775646896559288, 1028, '2021-04-25 16:26:34');
INSERT INTO `oppose_history` VALUES (193, 5775646896559288, 1038, '2021-04-25 16:26:36');
INSERT INTO `oppose_history` VALUES (194, 5775673578120840, 1002, '2021-04-25 16:26:39');
INSERT INTO `oppose_history` VALUES (195, 5775673578120840, 1008, '2021-04-25 16:26:41');
INSERT INTO `oppose_history` VALUES (196, 5775673578120840, 1056, '2021-04-25 16:26:43');
INSERT INTO `oppose_history` VALUES (197, 5775674380365248, 1003, '2021-04-25 16:26:46');
INSERT INTO `oppose_history` VALUES (198, 5775674380365248, 1001, '2021-04-25 16:26:49');
INSERT INTO `oppose_history` VALUES (199, 5775674380365248, 1009, '2021-04-25 16:26:51');
INSERT INTO `oppose_history` VALUES (200, 5775674380365248, 1075, '2021-04-25 16:26:54');
INSERT INTO `oppose_history` VALUES (201, 5775674380365248, 1002, '2021-05-02 00:53:44');

-- ----------------------------
-- Table structure for perms
-- ----------------------------
DROP TABLE IF EXISTS `perms`;
CREATE TABLE `perms`  (
  `perms_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `perms_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限名称',
  `perms_describe` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限描述',
  `permission` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`perms_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 401 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of perms
-- ----------------------------
INSERT INTO `perms` VALUES (101, 'user:manage', '管理用户所有信息', '/src/user');
INSERT INTO `perms` VALUES (102, 'news: manage', '管理新闻及分类', '/src/news');
INSERT INTO `perms` VALUES (103, 'comment:manage', '管理评论、点赞、踩', '/src/comment');
INSERT INTO `perms` VALUES (104, 'news:verify', '审核新闻', NULL);
INSERT INTO `perms` VALUES (201, 'news:renovate', '发布、更新新闻', NULL);
INSERT INTO `perms` VALUES (301, 'news:support', '点赞、踩、评论', NULL);
INSERT INTO `perms` VALUES (401, 'news:view', '浏览新闻', NULL);

-- ----------------------------
-- Table structure for renovate
-- ----------------------------
DROP TABLE IF EXISTS `renovate`;
CREATE TABLE `renovate`  (
  `renovate_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '更新id',
  `user_id` bigint(32) NULL DEFAULT NULL COMMENT '更新用户id',
  `new_id` bigint(32) NULL DEFAULT NULL COMMENT '更新新闻id',
  `renovate_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`renovate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '更新' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of renovate
-- ----------------------------
INSERT INTO `renovate` VALUES (101, 101, 1001, '2021-04-01 15:19:03');
INSERT INTO `renovate` VALUES (102, 101, 1002, '2021-03-29 11:54:18');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `role_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `role_describe` varchar(3072) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 405 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (101, 'admin', '超级管理员，拥有所有权限');
INSERT INTO `role` VALUES (201, 'editor', '编辑人员，拥有权限：发布更新、浏览、评论、点赞、踩');
INSERT INTO `role` VALUES (301, 'consumer', '普通用户，拥有权限：浏览、评论、点赞、踩');

-- ----------------------------
-- Table structure for role_perms
-- ----------------------------
DROP TABLE IF EXISTS `role_perms`;
CREATE TABLE `role_perms`  (
  `role_perms_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '角色权限id',
  `role_id` bigint(32) NULL DEFAULT NULL COMMENT '角色id',
  `perms_id` bigint(32) NULL DEFAULT NULL COMMENT '权限id',
  PRIMARY KEY (`role_perms_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色权限关联' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_perms
-- ----------------------------
INSERT INTO `role_perms` VALUES (1, 101, 101);
INSERT INTO `role_perms` VALUES (2, 101, 102);
INSERT INTO `role_perms` VALUES (3, 101, 103);
INSERT INTO `role_perms` VALUES (4, 101, 104);
INSERT INTO `role_perms` VALUES (5, 101, 201);
INSERT INTO `role_perms` VALUES (6, 101, 301);
INSERT INTO `role_perms` VALUES (7, 101, 401);
INSERT INTO `role_perms` VALUES (8, 201, 201);
INSERT INTO `role_perms` VALUES (9, 201, 301);
INSERT INTO `role_perms` VALUES (10, 201, 401);
INSERT INTO `role_perms` VALUES (11, 301, 301);
INSERT INTO `role_perms` VALUES (12, 301, 401);

-- ----------------------------
-- Table structure for support_history
-- ----------------------------
DROP TABLE IF EXISTS `support_history`;
CREATE TABLE `support_history`  (
  `support_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '点赞id',
  `user_id` bigint(32) NULL DEFAULT NULL COMMENT '点赞用户id',
  `new_id` bigint(32) NULL DEFAULT NULL COMMENT '点赞新闻id',
  `support_time` datetime(0) NULL DEFAULT NULL COMMENT '点赞时间',
  PRIMARY KEY (`support_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 217 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '点赞历史' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of support_history
-- ----------------------------
INSERT INTO `support_history` VALUES (101, 301, 1001, '2021-04-01 15:19:22');
INSERT INTO `support_history` VALUES (102, 201, 1002, '2021-03-29 14:14:08');
INSERT INTO `support_history` VALUES (103, 301, 1003, '2021-03-29 12:14:21');
INSERT INTO `support_history` VALUES (104, 5769745737743035, 1050, '2021-04-25 13:38:04');
INSERT INTO `support_history` VALUES (105, 5769745737743035, 1040, '2021-04-25 13:38:08');
INSERT INTO `support_history` VALUES (106, 5746719496328924, 1051, '2021-04-25 13:38:10');
INSERT INTO `support_history` VALUES (107, 5746719496328924, 1032, '2021-04-25 13:38:12');
INSERT INTO `support_history` VALUES (108, 5744592866844332, 1023, '2021-04-25 13:38:14');
INSERT INTO `support_history` VALUES (109, 5744592866844332, 1054, '2021-04-25 13:38:16');
INSERT INTO `support_history` VALUES (110, 4512566170025984, 1005, '2021-04-25 13:38:18');
INSERT INTO `support_history` VALUES (111, 201, 1001, '2021-04-01 18:27:26');
INSERT INTO `support_history` VALUES (112, 4512566170025984, 1007, '2021-04-25 13:38:21');
INSERT INTO `support_history` VALUES (113, 101, 1009, '2021-04-25 13:38:23');
INSERT INTO `support_history` VALUES (114, 101, 1003, '2021-04-01 20:03:58');
INSERT INTO `support_history` VALUES (115, 5775674380365248, 1004, '2021-04-25 13:38:25');
INSERT INTO `support_history` VALUES (116, 5775674380365248, 1044, '2021-04-25 13:38:27');
INSERT INTO `support_history` VALUES (117, 5775673578120840, 1041, '2021-04-25 13:38:29');
INSERT INTO `support_history` VALUES (118, 5775673578120840, 1042, '2021-04-25 13:38:36');
INSERT INTO `support_history` VALUES (119, 5769745737743041, 1032, '2021-04-25 13:38:41');
INSERT INTO `support_history` VALUES (120, 5769745737743041, 1035, '2021-04-25 13:38:46');
INSERT INTO `support_history` VALUES (121, 5769745737743041, 1047, '2021-04-25 13:29:25');
INSERT INTO `support_history` VALUES (122, 5775646896559288, 1078, '2021-04-25 13:38:53');
INSERT INTO `support_history` VALUES (123, 5775646896559288, 1058, '2021-04-25 13:39:13');
INSERT INTO `support_history` VALUES (124, 5746719496328924, 1067, '2021-04-25 13:39:11');
INSERT INTO `support_history` VALUES (125, 5746719496328924, 1033, '2021-04-25 13:38:51');
INSERT INTO `support_history` VALUES (126, 5746686520543108, 1066, '2021-04-25 13:38:48');
INSERT INTO `support_history` VALUES (127, 5746686520543108, 1048, '2021-04-25 13:38:43');
INSERT INTO `support_history` VALUES (128, 5747184000103856, 1049, '2021-04-25 13:38:38');
INSERT INTO `support_history` VALUES (129, 5747184000103856, 1062, '2021-04-25 13:38:34');
INSERT INTO `support_history` VALUES (130, 5746686520543108, 1080, '2021-04-25 13:38:31');
INSERT INTO `support_history` VALUES (131, 5746719496328924, 1022, '2021-04-25 13:41:43');
INSERT INTO `support_history` VALUES (132, 5746719496328924, 1025, '2021-04-25 13:40:34');
INSERT INTO `support_history` VALUES (133, 5769745737743035, 1035, '2021-04-25 13:41:53');
INSERT INTO `support_history` VALUES (134, 5769745737743035, 1045, '2021-04-25 13:41:55');
INSERT INTO `support_history` VALUES (135, 5746686520543108, 1054, '2021-04-25 13:41:59');
INSERT INTO `support_history` VALUES (136, 5746686520543108, 1042, '2021-04-25 13:42:04');
INSERT INTO `support_history` VALUES (137, 5769745737743028, 1061, '2021-04-25 13:42:02');
INSERT INTO `support_history` VALUES (138, 5769745737743028, 1063, '2021-04-25 13:41:58');
INSERT INTO `support_history` VALUES (139, 5769745737743040, 1069, '2021-04-25 13:41:49');
INSERT INTO `support_history` VALUES (140, 5769745737743040, 1048, '2021-04-25 13:41:46');
INSERT INTO `support_history` VALUES (141, 4512566170025984, 1011, '2021-04-25 17:24:37');
INSERT INTO `support_history` VALUES (142, 4512566170025984, 1010, '2021-04-25 17:24:41');
INSERT INTO `support_history` VALUES (143, 4512566170025984, 1052, '2021-04-25 17:24:44');
INSERT INTO `support_history` VALUES (144, 5744592866844332, 1044, '2021-04-25 17:24:51');
INSERT INTO `support_history` VALUES (145, 5744592866844332, 1001, '2021-04-25 17:24:47');
INSERT INTO `support_history` VALUES (146, 5744592866844332, 1003, '2021-04-25 17:24:49');
INSERT INTO `support_history` VALUES (147, 5746686520543108, 1032, '2021-04-25 17:25:06');
INSERT INTO `support_history` VALUES (148, 5746686520543108, 1011, '2021-04-25 17:25:09');
INSERT INTO `support_history` VALUES (149, 5746686520543108, 1002, '2021-04-25 17:24:54');
INSERT INTO `support_history` VALUES (150, 5746687128004140, 1026, '2021-04-25 17:26:42');
INSERT INTO `support_history` VALUES (151, 5746687128004140, 1054, '2021-04-25 17:26:40');
INSERT INTO `support_history` VALUES (152, 5746687128004140, 1022, '2021-04-25 17:26:38');
INSERT INTO `support_history` VALUES (153, 5746719496328924, 1029, '2021-04-25 17:27:02');
INSERT INTO `support_history` VALUES (154, 5746719496328924, 1001, '2021-04-25 17:26:57');
INSERT INTO `support_history` VALUES (155, 5746719496328924, 1003, '2021-04-25 17:27:00');
INSERT INTO `support_history` VALUES (156, 5747184000103856, 1021, '2021-04-25 18:41:35');
INSERT INTO `support_history` VALUES (157, 5747184000103856, 1031, '2021-04-25 18:41:38');
INSERT INTO `support_history` VALUES (158, 5747184000103856, 1002, '2021-04-25 17:27:05');
INSERT INTO `support_history` VALUES (159, 5747184000103857, 1054, '2021-04-25 18:41:52');
INSERT INTO `support_history` VALUES (160, 5747184000103857, 1002, '2021-04-25 18:41:50');
INSERT INTO `support_history` VALUES (161, 5747184000103857, 1053, '2021-04-25 18:41:54');
INSERT INTO `support_history` VALUES (162, 5769745737743028, 1003, '2021-04-25 18:42:10');
INSERT INTO `support_history` VALUES (163, 5769745737743028, 1001, '2021-04-25 18:42:07');
INSERT INTO `support_history` VALUES (164, 5769745737743028, 1012, '2021-04-25 18:42:12');
INSERT INTO `support_history` VALUES (165, 5769745737743030, 1074, '2021-04-25 18:42:28');
INSERT INTO `support_history` VALUES (166, 5769745737743030, 1047, '2021-04-25 18:42:30');
INSERT INTO `support_history` VALUES (167, 5769745737743030, 1063, '2021-04-25 18:42:33');
INSERT INTO `support_history` VALUES (168, 5769745737743035, 1002, '2021-04-25 18:42:59');
INSERT INTO `support_history` VALUES (169, 5769745737743035, 1013, '2021-04-25 18:43:02');
INSERT INTO `support_history` VALUES (170, 5769745737743035, 1014, '2021-04-25 18:43:04');
INSERT INTO `support_history` VALUES (171, 5769745737743036, 1058, '2021-04-25 18:43:11');
INSERT INTO `support_history` VALUES (172, 5769745737743036, 1027, '2021-04-25 18:43:07');
INSERT INTO `support_history` VALUES (173, 5769745737743036, 1029, '2021-04-25 18:43:09');
INSERT INTO `support_history` VALUES (174, 5769745737743037, 1024, '2021-04-25 18:43:51');
INSERT INTO `support_history` VALUES (175, 5769745737743037, 1044, '2021-04-25 18:43:45');
INSERT INTO `support_history` VALUES (176, 5769745737743037, 1023, '2021-04-25 18:43:49');
INSERT INTO `support_history` VALUES (177, 5769745737743038, 1031, '2021-04-25 18:43:58');
INSERT INTO `support_history` VALUES (178, 5769745737743038, 1038, '2021-04-25 18:43:56');
INSERT INTO `support_history` VALUES (179, 5769745737743038, 1025, '2021-04-25 18:43:53');
INSERT INTO `support_history` VALUES (180, 5769745737743039, 1037, '2021-04-25 18:44:06');
INSERT INTO `support_history` VALUES (181, 5769745737743039, 1008, '2021-04-25 18:44:03');
INSERT INTO `support_history` VALUES (182, 5769745737743039, 1041, '2021-04-25 18:44:01');
INSERT INTO `support_history` VALUES (183, 5769745737743040, 1020, '2021-04-25 18:44:27');
INSERT INTO `support_history` VALUES (184, 5769745737743040, 1034, '2021-04-25 18:44:17');
INSERT INTO `support_history` VALUES (185, 5769745737743040, 1039, '2021-04-25 18:44:20');
INSERT INTO `support_history` VALUES (186, 5769745737743041, 1007, '2021-04-25 18:44:48');
INSERT INTO `support_history` VALUES (187, 5769745737743041, 1022, '2021-04-25 18:44:50');
INSERT INTO `support_history` VALUES (188, 5769745737743041, 1077, '2021-04-25 18:44:53');
INSERT INTO `support_history` VALUES (189, 5769745737743042, 1002, '2021-04-25 18:45:11');
INSERT INTO `support_history` VALUES (190, 5769745737743042, 1047, '2021-04-22 18:45:15');
INSERT INTO `support_history` VALUES (191, 5769745737743042, 1059, '2021-04-16 18:45:20');
INSERT INTO `support_history` VALUES (192, 5775646896559288, 1005, '2021-04-25 18:45:34');
INSERT INTO `support_history` VALUES (193, 5775646896559288, 1004, '2021-04-25 18:45:36');
INSERT INTO `support_history` VALUES (194, 5775673578120840, 1001, '2021-04-25 18:45:52');
INSERT INTO `support_history` VALUES (195, 5775646896559288, 1024, '2021-04-25 18:45:38');
INSERT INTO `support_history` VALUES (196, 5775673578120840, 1003, '2021-04-25 18:45:55');
INSERT INTO `support_history` VALUES (197, 5775673578120840, 1057, '2021-04-25 18:45:59');
INSERT INTO `support_history` VALUES (198, 5775674380365248, 1071, '2021-04-25 18:46:15');
INSERT INTO `support_history` VALUES (199, 5775674380365248, 1009, '2021-04-25 18:46:17');
INSERT INTO `support_history` VALUES (200, 5775674380365248, 1015, '2021-04-25 18:46:20');
INSERT INTO `support_history` VALUES (201, 101, 1070, '2021-04-25 19:09:26');
INSERT INTO `support_history` VALUES (202, 201, 1070, '2021-04-25 19:09:29');
INSERT INTO `support_history` VALUES (203, 301, 1070, '2021-04-25 19:09:31');
INSERT INTO `support_history` VALUES (204, 5775674380365248, 1001, '2021-04-25 19:10:18');
INSERT INTO `support_history` VALUES (205, 5775674380365248, 1002, '2021-04-25 19:10:20');
INSERT INTO `support_history` VALUES (206, 5775674380365248, 1070, '2021-04-25 19:10:23');
INSERT INTO `support_history` VALUES (207, 5747184000103856, 1001, '2021-04-25 19:10:25');
INSERT INTO `support_history` VALUES (208, 5747184000103856, 1002, '2021-04-25 19:10:26');
INSERT INTO `support_history` VALUES (209, 5747184000103856, 1070, '2021-04-25 19:10:28');
INSERT INTO `support_history` VALUES (210, 5747184000103857, 1001, '2021-04-25 19:10:30');
INSERT INTO `support_history` VALUES (211, 5747184000103857, 1002, '2021-04-25 19:10:32');
INSERT INTO `support_history` VALUES (212, 5747184000103857, 1070, '2021-04-25 19:10:33');
INSERT INTO `support_history` VALUES (213, 4512566170025984, 1001, '2021-04-25 19:10:35');
INSERT INTO `support_history` VALUES (214, 4512566170025984, 1002, '2021-04-25 19:10:37');
INSERT INTO `support_history` VALUES (215, 4512566170025984, 1070, '2021-04-25 19:12:22');
INSERT INTO `support_history` VALUES (216, 5775646896559288, 1070, '2021-04-25 19:12:24');
INSERT INTO `support_history` VALUES (217, 5746686520543108, 1070, '2021-04-25 19:12:25');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名称',
  `user_password` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户密码',
  `user_sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户性别',
  `user_phone` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户手机',
  `user_email` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户邮箱',
  `user_status` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户状态',
  `enroll_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '注册时间',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5775674380365250 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (101, 'admin', '123', '男', '18083036910', '823495617@qq.com', '1', '2021-04-01 15:50:50');
INSERT INTO `user` VALUES (201, 'b1', '123', '女', '18011112222', '11112222@qq.com', '0', '2021-03-29 18:25:23');
INSERT INTO `user` VALUES (301, 'zf', '123', '男', '18022223333', '22223333@qq.com', '1', '2021-05-05 23:18:29');
INSERT INTO `user` VALUES (4512566170025984, '路卡', '123', '男', '18466667777', '18466667777@qq.com', '0', '2021-04-08 18:52:05');
INSERT INTO `user` VALUES (5744592866844332, '张三', '123', '男', '18055557777', '55557777@qq.com', '1', '2021-04-08 16:45:32');
INSERT INTO `user` VALUES (5746686520543108, '李四', '123', '男', '18055558888', '55558888@qq.com', '1', '2021-04-08 16:45:37');
INSERT INTO `user` VALUES (5746687128004140, '张燕', '123', '女', '18055559999', '55559999@qq.com', '1', '2021-04-08 16:45:41');
INSERT INTO `user` VALUES (5746719496328924, '张良', '123', '男', '18055556666', '55556666@qq.com', '1', '2021-04-08 16:45:43');
INSERT INTO `user` VALUES (5747184000103856, '王五', '123', '男', '18055553333', '55553333@qq.com', '1', '2021-04-08 16:45:46');
INSERT INTO `user` VALUES (5747184000103857, '刘六', '123', '男', '15922224444', '2222444@qq.com', '0', '2021-04-08 16:45:48');
INSERT INTO `user` VALUES (5769745737743028, '关七', '123', '男', '18022229999', '22229999@qq.com', '1', '2021-04-08 16:45:51');
INSERT INTO `user` VALUES (5769745737743030, '王将', '123', '男', '13911112222', '13911112222@qq.com', '0', '2021-04-08 16:45:53');
INSERT INTO `user` VALUES (5769745737743035, '陈亚', '123', '女', '13922223333', '13922223333@qq.com', '0', '2021-04-08 16:45:55');
INSERT INTO `user` VALUES (5769745737743036, '杨发', '123', '女', '13933334444', '13933334444@qq.com', '0', '2021-04-08 16:45:57');
INSERT INTO `user` VALUES (5769745737743037, '青云', '123', '女', '13944445555', '13944445555@qq.com', '0', '2021-04-08 16:45:58');
INSERT INTO `user` VALUES (5769745737743038, '田进', '123', '女', '13955556666', '13955556666@qq.com', '0', '2021-04-08 16:46:00');
INSERT INTO `user` VALUES (5769745737743039, '刘强', '123', '男', '13977778888', '77778888@qq.com', '0', '2021-04-08 16:46:02');
INSERT INTO `user` VALUES (5769745737743040, '任场', '123', '男', '15122223333', '15122223333@qq.com', '0', '2021-05-02 00:58:00');
INSERT INTO `user` VALUES (5769745737743041, '文点', '132', '男', '18947326483', '47326483@qq.com', '0', '2021-04-08 16:46:06');
INSERT INTO `user` VALUES (5769745737743042, '高点', '123', '男', '15133334444', '15133334444@qq.com', '0', '2021-05-02 00:58:12');
INSERT INTO `user` VALUES (5775646896559288, '景枫', '123', '男', '17774543343', '177445555@qq.com', '0', '2021-05-02 01:01:23');
INSERT INTO `user` VALUES (5775673578120840, '李哥', '123', '女', '15144445555', '15144445555@qq.com', '0', '2021-05-02 00:58:23');
INSERT INTO `user` VALUES (5775674380365248, '房青', '123', '男', '15155556666', '15155556666@qq.com', '0', '2021-05-02 01:01:25');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `user_role_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '用户角色id',
  `user_id` bigint(32) NULL DEFAULT NULL COMMENT '用户id',
  `role_id` bigint(32) NULL DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`user_role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色关联' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES (1, 101, 101);
INSERT INTO `user_role` VALUES (2, 201, 201);
INSERT INTO `user_role` VALUES (3, 301, 301);
INSERT INTO `user_role` VALUES (5, 4512566170025984, 301);
INSERT INTO `user_role` VALUES (6, 5744592866844332, 301);
INSERT INTO `user_role` VALUES (7, 5746686520543108, 301);
INSERT INTO `user_role` VALUES (8, 5746687128004140, 301);
INSERT INTO `user_role` VALUES (9, 5746719496328924, 301);
INSERT INTO `user_role` VALUES (10, 5747184000103856, 301);
INSERT INTO `user_role` VALUES (11, 5747184000103857, 301);
INSERT INTO `user_role` VALUES (12, 5769745737743028, 301);
INSERT INTO `user_role` VALUES (13, 5769745737743030, 301);
INSERT INTO `user_role` VALUES (14, 5769745737743035, 301);
INSERT INTO `user_role` VALUES (15, 5769745737743036, 301);
INSERT INTO `user_role` VALUES (16, 5769745737743037, 301);
INSERT INTO `user_role` VALUES (17, 5769745737743038, 301);
INSERT INTO `user_role` VALUES (18, 5769745737743039, 301);
INSERT INTO `user_role` VALUES (19, 5769745737743040, 301);
INSERT INTO `user_role` VALUES (20, 5769745737743041, 301);
INSERT INTO `user_role` VALUES (21, 5769745737743042, 301);
INSERT INTO `user_role` VALUES (22, 5775646896559288, 301);
INSERT INTO `user_role` VALUES (23, 5775673578120840, 301);
INSERT INTO `user_role` VALUES (24, 5775674380365248, 301);

-- ----------------------------
-- Table structure for verify
-- ----------------------------
DROP TABLE IF EXISTS `verify`;
CREATE TABLE `verify`  (
  `verify_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '审核id',
  `user_id` bigint(32) NULL DEFAULT NULL COMMENT '审核用户id',
  `new_id` bigint(32) NULL DEFAULT NULL COMMENT '审核新闻id',
  `verify_status` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核状态',
  `verify_time` datetime(0) NULL DEFAULT NULL COMMENT '审核时间',
  PRIMARY KEY (`verify_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '审核' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of verify
-- ----------------------------
INSERT INTO `verify` VALUES (101, 101, 1001, '审核通过', '2021-04-01 15:20:23');
INSERT INTO `verify` VALUES (102, 101, 1002, '审核通过', '2021-03-29 05:56:13');
INSERT INTO `verify` VALUES (103, 101, 1003, '审核通过', '2021-04-01 15:20:43');

-- ----------------------------
-- Table structure for view_history
-- ----------------------------
DROP TABLE IF EXISTS `view_history`;
CREATE TABLE `view_history`  (
  `view_id` bigint(32) NOT NULL AUTO_INCREMENT COMMENT '浏览id',
  `user_id` bigint(32) NULL DEFAULT NULL COMMENT '浏览用户id',
  `new_id` bigint(32) NULL DEFAULT NULL COMMENT '浏览新闻id',
  `view_time` datetime(0) NULL DEFAULT NULL COMMENT '浏览时间',
  PRIMARY KEY (`view_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 215 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '浏览历史' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of view_history
-- ----------------------------
INSERT INTO `view_history` VALUES (102, 301, 1001, '2021-03-29 12:20:40');
INSERT INTO `view_history` VALUES (103, 301, 1002, '2021-03-29 15:20:50');
INSERT INTO `view_history` VALUES (104, 201, 1002, '2021-03-29 04:20:58');
INSERT INTO `view_history` VALUES (105, 101, 1003, '2021-03-29 08:21:11');
INSERT INTO `view_history` VALUES (106, 301, 1003, '2021-03-29 06:21:22');
INSERT INTO `view_history` VALUES (109, 4512566170025984, 1020, '2021-04-25 13:50:33');
INSERT INTO `view_history` VALUES (110, 4512566170025984, 1011, '2021-04-25 13:50:38');
INSERT INTO `view_history` VALUES (111, 5744592866844332, 1014, '2021-04-25 13:50:43');
INSERT INTO `view_history` VALUES (112, 5744592866844332, 1018, '2021-04-25 13:50:47');
INSERT INTO `view_history` VALUES (113, 5746686520543108, 1055, '2021-04-25 13:51:50');
INSERT INTO `view_history` VALUES (114, 5746686520543108, 1025, '2021-04-25 13:51:55');
INSERT INTO `view_history` VALUES (115, 5746687128004140, 1024, '2021-04-25 13:51:57');
INSERT INTO `view_history` VALUES (116, 5746687128004140, 1018, '2021-04-25 13:52:06');
INSERT INTO `view_history` VALUES (117, 5775674380365248, 1032, '2021-04-25 13:52:10');
INSERT INTO `view_history` VALUES (118, 5775674380365248, 1034, '2021-04-25 13:52:16');
INSERT INTO `view_history` VALUES (119, 5775673578120840, 1038, '2021-04-25 13:52:22');
INSERT INTO `view_history` VALUES (120, 5775673578120840, 1042, '2021-04-25 13:52:27');
INSERT INTO `view_history` VALUES (121, 5775646896559288, 1043, '2021-04-25 13:52:31');
INSERT INTO `view_history` VALUES (122, 5775646896559288, 1048, '2021-04-25 13:52:37');
INSERT INTO `view_history` VALUES (123, 5769745737743042, 1053, '2021-04-25 13:52:41');
INSERT INTO `view_history` VALUES (124, 5769745737743042, 1055, '2021-04-25 13:52:48');
INSERT INTO `view_history` VALUES (125, 5769745737743041, 1057, '2021-04-25 13:52:45');
INSERT INTO `view_history` VALUES (126, 5769745737743041, 1062, '2021-04-25 13:52:43');
INSERT INTO `view_history` VALUES (127, 5769745737743040, 1067, '2021-04-25 13:52:33');
INSERT INTO `view_history` VALUES (128, 5769745737743040, 1069, '2021-04-25 13:52:28');
INSERT INTO `view_history` VALUES (129, 5769745737743039, 1071, '2021-04-25 13:52:24');
INSERT INTO `view_history` VALUES (130, 5769745737743039, 1072, '2021-04-25 13:52:18');
INSERT INTO `view_history` VALUES (131, 5769745737743038, 1075, '2021-04-25 13:52:13');
INSERT INTO `view_history` VALUES (132, 5769745737743038, 1078, '2021-04-25 13:52:08');
INSERT INTO `view_history` VALUES (133, 5769745737743037, 1080, '2021-04-25 13:52:04');
INSERT INTO `view_history` VALUES (134, 5769745737743036, 1056, '2021-04-25 13:51:59');
INSERT INTO `view_history` VALUES (135, 5769745737743035, 1051, '2021-04-25 13:51:53');
INSERT INTO `view_history` VALUES (136, 5769745737743030, 1031, '2021-04-25 13:50:45');
INSERT INTO `view_history` VALUES (137, 5769745737743028, 1036, '2021-04-25 13:50:40');
INSERT INTO `view_history` VALUES (138, 5747184000103857, 1022, '2021-04-25 13:50:35');
INSERT INTO `view_history` VALUES (139, 5747184000103856, 1017, '2021-04-25 13:50:07');
INSERT INTO `view_history` VALUES (140, 5746719496328924, 1008, '2021-04-25 13:50:01');
INSERT INTO `view_history` VALUES (141, 4512566170025984, 1002, '2021-04-25 18:51:13');
INSERT INTO `view_history` VALUES (142, 4512566170025984, 1044, '2021-04-25 18:51:16');
INSERT INTO `view_history` VALUES (143, 4512566170025984, 1074, '2021-04-25 18:51:18');
INSERT INTO `view_history` VALUES (144, 5744592866844332, 1028, '2021-04-25 18:51:33');
INSERT INTO `view_history` VALUES (145, 5744592866844332, 1029, '2021-04-25 18:51:36');
INSERT INTO `view_history` VALUES (146, 5744592866844332, 1034, '2021-04-25 18:51:41');
INSERT INTO `view_history` VALUES (147, 5746686520543108, 1020, '2021-04-25 18:51:53');
INSERT INTO `view_history` VALUES (148, 5746686520543108, 1030, '2021-04-25 18:51:56');
INSERT INTO `view_history` VALUES (149, 5746686520543108, 1010, '2021-04-25 18:51:51');
INSERT INTO `view_history` VALUES (150, 5746687128004140, 1037, '2021-04-25 18:52:08');
INSERT INTO `view_history` VALUES (151, 5746687128004140, 1058, '2021-04-25 18:52:14');
INSERT INTO `view_history` VALUES (152, 5746687128004140, 1042, '2021-04-25 18:52:12');
INSERT INTO `view_history` VALUES (153, 5746719496328924, 1059, '2021-04-25 18:52:30');
INSERT INTO `view_history` VALUES (154, 5746719496328924, 1042, '2021-04-25 18:52:27');
INSERT INTO `view_history` VALUES (155, 5746719496328924, 1005, '2021-04-25 18:52:25');
INSERT INTO `view_history` VALUES (156, 5747184000103856, 1053, '2021-04-25 18:52:50');
INSERT INTO `view_history` VALUES (157, 5747184000103856, 1080, '2021-04-25 18:52:44');
INSERT INTO `view_history` VALUES (158, 5747184000103856, 1077, '2021-04-25 18:52:48');
INSERT INTO `view_history` VALUES (159, 5747184000103857, 1038, '2021-04-25 18:53:09');
INSERT INTO `view_history` VALUES (160, 5747184000103857, 1046, '2021-04-25 18:53:07');
INSERT INTO `view_history` VALUES (161, 5747184000103857, 1052, '2021-04-25 18:53:05');
INSERT INTO `view_history` VALUES (162, 5769745737743028, 1017, '2021-04-25 18:53:30');
INSERT INTO `view_history` VALUES (163, 5769745737743028, 1014, '2021-04-25 18:53:28');
INSERT INTO `view_history` VALUES (164, 5769745737743028, 1005, '2021-04-25 18:53:25');
INSERT INTO `view_history` VALUES (165, 5769745737743030, 1035, '2021-04-25 18:53:44');
INSERT INTO `view_history` VALUES (166, 5769745737743030, 1047, '2021-04-25 18:53:46');
INSERT INTO `view_history` VALUES (167, 5769745737743030, 1058, '2021-04-25 18:53:48');
INSERT INTO `view_history` VALUES (168, 5769745737743035, 1037, '2021-04-25 18:54:00');
INSERT INTO `view_history` VALUES (169, 5769745737743035, 1021, '2021-04-25 18:53:58');
INSERT INTO `view_history` VALUES (170, 5769745737743035, 1049, '2021-04-25 18:54:02');
INSERT INTO `view_history` VALUES (171, 5769745737743036, 1061, '2021-04-25 18:54:20');
INSERT INTO `view_history` VALUES (172, 5769745737743036, 1068, '2021-04-25 18:54:18');
INSERT INTO `view_history` VALUES (173, 5769745737743036, 1044, '2021-04-25 18:54:15');
INSERT INTO `view_history` VALUES (174, 5769745737743037, 1044, '2021-04-25 18:54:39');
INSERT INTO `view_history` VALUES (175, 5769745737743037, 1020, '2021-04-25 18:54:36');
INSERT INTO `view_history` VALUES (176, 5769745737743037, 1007, '2021-04-25 18:54:33');
INSERT INTO `view_history` VALUES (177, 5769745737743038, 1020, '2021-04-25 18:54:45');
INSERT INTO `view_history` VALUES (178, 5769745737743038, 1047, '2021-04-25 18:54:52');
INSERT INTO `view_history` VALUES (179, 5769745737743038, 1027, '2021-04-25 18:54:55');
INSERT INTO `view_history` VALUES (180, 5769745737743039, 1042, '2021-04-25 18:54:59');
INSERT INTO `view_history` VALUES (181, 5769745737743039, 1048, '2021-04-25 18:55:01');
INSERT INTO `view_history` VALUES (182, 5769745737743039, 1037, '2021-04-25 18:55:13');
INSERT INTO `view_history` VALUES (183, 5769745737743039, 1027, '2021-04-25 18:55:15');
INSERT INTO `view_history` VALUES (184, 5769745737743040, 1024, '2021-04-25 18:55:28');
INSERT INTO `view_history` VALUES (185, 5769745737743040, 1029, '2021-04-25 18:55:30');
INSERT INTO `view_history` VALUES (186, 5769745737743040, 1046, '2021-04-25 18:55:32');
INSERT INTO `view_history` VALUES (187, 5769745737743041, 1023, '2021-04-25 18:55:51');
INSERT INTO `view_history` VALUES (188, 5769745737743041, 1027, '2021-04-25 18:55:54');
INSERT INTO `view_history` VALUES (189, 5769745737743041, 1038, '2021-04-25 18:55:57');
INSERT INTO `view_history` VALUES (190, 5769745737743042, 1028, '2021-04-25 18:56:08');
INSERT INTO `view_history` VALUES (191, 5769745737743042, 1036, '2021-04-25 18:56:11');
INSERT INTO `view_history` VALUES (192, 5769745737743042, 1046, '2021-04-25 18:56:13');
INSERT INTO `view_history` VALUES (193, 5775646896559288, 1001, '2021-04-25 18:56:28');
INSERT INTO `view_history` VALUES (194, 5775646896559288, 1003, '2021-04-25 18:56:32');
INSERT INTO `view_history` VALUES (195, 5775646896559288, 1035, '2021-04-25 18:56:34');
INSERT INTO `view_history` VALUES (196, 5775673578120840, 1002, '2021-04-25 18:56:50');
INSERT INTO `view_history` VALUES (197, 5775673578120840, 1041, '2021-04-25 18:56:52');
INSERT INTO `view_history` VALUES (198, 5775673578120840, 1050, '2021-04-25 18:56:54');
INSERT INTO `view_history` VALUES (199, 5775674380365248, 1002, '2021-04-25 18:57:02');
INSERT INTO `view_history` VALUES (200, 5775674380365248, 1010, '2021-04-25 18:57:05');
INSERT INTO `view_history` VALUES (201, 5769745737743037, 1001, '2021-04-25 19:06:38');
INSERT INTO `view_history` VALUES (202, 5769745737743038, 1070, '2021-04-25 19:06:40');
INSERT INTO `view_history` VALUES (203, 5769745737743038, 1002, '2021-04-25 19:06:44');
INSERT INTO `view_history` VALUES (204, 5769745737743038, 1001, '2021-04-25 19:06:46');
INSERT INTO `view_history` VALUES (205, 4512566170025984, 1001, '2021-04-25 19:06:48');
INSERT INTO `view_history` VALUES (206, 4512566170025984, 1002, '2021-04-25 19:06:50');
INSERT INTO `view_history` VALUES (207, 5769745737743041, 1070, '2021-04-25 19:06:52');
INSERT INTO `view_history` VALUES (208, 5769745737743041, 1001, '2021-04-25 19:05:43');
INSERT INTO `view_history` VALUES (209, 5769745737743041, 1002, '2021-04-25 19:06:54');
INSERT INTO `view_history` VALUES (210, 5769745737743042, 1001, '2021-04-25 19:06:56');
INSERT INTO `view_history` VALUES (211, 101, 1070, '2021-04-25 19:08:28');
INSERT INTO `view_history` VALUES (212, 201, 1070, '2021-04-25 19:08:30');
INSERT INTO `view_history` VALUES (213, 301, 1070, '2021-04-25 19:08:31');
INSERT INTO `view_history` VALUES (215, 5769745737743039, 1070, '2021-04-25 19:08:36');
