package com.ozo.news.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.Image;
import com.ozo.news.entity.Interest;
import com.ozo.news.service.InterestService;
import com.ozo.news.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/interest")
public class InterestController {

    @Autowired
    InterestService interestService;

    @GetMapping("/findAll")
    public AjaxResult findAllInterest(){
        return AjaxResult.success("成功",interestService.findAllInterest()) ;
    }

    @RequestMapping(value = "findByCondition", method = RequestMethod.GET, params = {"pageNum","pageSize"})
    public AjaxResult findInterestByCondition(@RequestBody Interest interest, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        //分页信息
        PageHelper.startPage(pageNum, pageSize);
        //执行分页查询
        PageInfo<Interest> page = new PageInfo<Interest>(interestService.findInterestByCondition(interest));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @RequestMapping(value = "findByUsername", method = RequestMethod.GET, params = {"username"})
    public AjaxResult findInterestByUsername(@RequestParam(value = "username") String username) {
        return AjaxResult.success("成功", interestService.findInterestByUsername(username));
    }

    @PostMapping("/insert")
    public AjaxResult insertInterest(@RequestBody Interest interest){
        interestService.insertInterest(interest);
        return AjaxResult.success("成功");
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET, params = {"interestId"})
    public AjaxResult deleteInterest(@RequestParam(value = "interestId") Long interestId){
        interestService.deleteInterest(interestId);
        return AjaxResult.success("成功");
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET, params = {"interestName","userId"})
    public AjaxResult deleteInterestByParam(@RequestParam(value = "interestName") String interestName, @RequestParam(value = "userId") Long userId){
        interestService.deleteInterestByParam(interestName,userId);
        return AjaxResult.success("成功");
    }

    @PostMapping("/update")
    public AjaxResult updateInterest(@RequestBody Interest interest){
        interestService.updateInterest(interest);
        return AjaxResult.success("成功");
    }
}
