package com.ozo.news.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.User;
import com.ozo.news.service.UserService;
import com.ozo.news.util.AjaxResult;
import com.ozo.news.util.GuuidUtil;
import com.ozo.news.util.UserRealm;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.sql.Timestamp;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "findAll", method = RequestMethod.GET, params = {"pageNum","pageSize"})
    public AjaxResult findAllUser(@RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize){
        //分页信息
        PageHelper.startPage(pageNum, pageSize);
        //执行分页查询
        PageInfo<User> page = new PageInfo<User>(userService.findAllUser());
        return AjaxResult.success("成功",page.getList(),page.getTotal());
    }

    @PostMapping("/login")
    public AjaxResult findLoginUser(@RequestBody User user){
        System.out.println(user);
        return AjaxResult.success("成功",userService.findLoginUser(user));
    }

    @RequestMapping(value = "findByCondition", method = RequestMethod.GET, params = {"pageNum","pageSize"})
    public AjaxResult findUserByCondition(@RequestBody User user,@RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize){
        //分页信息
        PageHelper.startPage(pageNum, pageSize);
        //执行分页查询
        PageInfo<User> page = new PageInfo<User>(userService.findUserByCondition(user));
        return AjaxResult.success("成功",page.getList(),page.getTotal());
    }

    @RequestMapping(value = "findByUsername", method = RequestMethod.GET, params = {"pageNum","pageSize","username"})
    public AjaxResult findUserByUsername(@RequestParam(value = "username") String username,@RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize){
        //分页信息
        PageHelper.startPage(pageNum, pageSize);
        //执行分页查询
        PageInfo<User> page = new PageInfo<User>(userService.findUserByUsername(username));
        return AjaxResult.success("成功",page.getList(),page.getTotal());
    }

    @RequestMapping(value = "findUserByName", method = RequestMethod.GET, params = {"name"})
    public AjaxResult findUserByName(@RequestParam("name") String name){
        return AjaxResult.success("成功",userService.findUserByName(name));
    }

    @PostMapping("/insert")
    public AjaxResult insertUser(@RequestBody User user){
        user.setUserId(GuuidUtil.getUUID());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        user.setUserStatus("0");
        user.setEnrollTime(timestamp);
        userService.insertUser(user);
        userService.insertUserRole(user.getUserId(),301L);
        System.out.println(user);
        return AjaxResult.success("成功");
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET, params = {"userId"})
    public AjaxResult deleteUser(@RequestParam(value = "userId") Long userId){
        userService.deleteUser(userId);
        return AjaxResult.success("成功");
    }

    @PostMapping("/update")
    public AjaxResult updateUser(@RequestBody User user){
        userService.updateUser(user);
        return AjaxResult.success("成功");
    }

}
