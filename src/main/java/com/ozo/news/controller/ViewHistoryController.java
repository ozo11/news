package com.ozo.news.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.Verify;
import com.ozo.news.entity.ViewHistory;
import com.ozo.news.service.ViewHistoryService;
import com.ozo.news.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/view")
public class ViewHistoryController {

    @Autowired
    ViewHistoryService viewHistoryService;

    @GetMapping("/findAll")
    public AjaxResult findAllViewHistory(){
        return AjaxResult.success("成功",viewHistoryService.findAllViewHistory());
    }

    @RequestMapping(value = "findByCondition", method = RequestMethod.GET, params = {"pageNum","pageSize"})
    public AjaxResult findViewHistoryByCondition(@RequestBody ViewHistory viewHistory, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        //分页信息
        PageHelper.startPage(pageNum, pageSize);
        //执行分页查询
        PageInfo<ViewHistory> page = new PageInfo<ViewHistory>(viewHistoryService.findViewHistoryByCondition(viewHistory));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @RequestMapping(value = "findByParam", method = RequestMethod.GET, params = {"pageNum","pageSize","username","newName"})
    public AjaxResult findViewHistoryByParam(@RequestParam(value = "username") String username,@RequestParam(value = "newName") String newName, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<ViewHistory> page = new PageInfo<ViewHistory>(viewHistoryService.findViewHistoryByParam(username,newName));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @PostMapping("/insert")
    public AjaxResult insertViewHistory(@RequestBody ViewHistory viewHistory){
        Timestamp timestamp =new Timestamp(System.currentTimeMillis());
        viewHistory.setViewTime(timestamp);
        viewHistoryService.insertViewHistory(viewHistory);
        System.out.println(viewHistory);
        return AjaxResult.success("成功");
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET, params = {"viewId"})
    public AjaxResult deleteOpposeHistory(@RequestParam(value = "viewId") Long viewId){
        viewHistoryService.deleteViewHistory(viewId);
        return AjaxResult.success("成功");
    }

}
