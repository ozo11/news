package com.ozo.news.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.OpposeHistory;
import com.ozo.news.entity.Renovate;
import com.ozo.news.service.RenovateService;
import com.ozo.news.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/renovate")
public class RenovateController {

    @Autowired
    RenovateService renovateService;

    @GetMapping("/findAll")
    public AjaxResult findAllRenovate(){
        return AjaxResult.success("成功",renovateService.findAllRenovate());
    }

    @RequestMapping(value = "findByCondition", method = RequestMethod.GET, params = {"pageNum","pageSize"})
    public AjaxResult findRenovateByCondition(@RequestBody Renovate renovate, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        //分页信息
        PageHelper.startPage(pageNum, pageSize);
        //执行分页查询
        PageInfo<Renovate> page = new PageInfo<Renovate>(renovateService.findRenovateByCondition(renovate));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @PostMapping("/insert")
    public AjaxResult insertRenovate(@RequestBody Renovate renovate){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        renovate.setRenovateTime(timestamp);
        renovateService.insertRenovate(renovate);
        System.out.println(renovate);
        return AjaxResult.success("成功");
    }

    @DeleteMapping("/delete/{id}")
    public AjaxResult deleteRenovate(@PathVariable("id") Long id){
        renovateService.deleteRenovate(id);
        return AjaxResult.success("成功");
    }

    @PutMapping("/update")
    public AjaxResult updateRenovate(@RequestBody Renovate renovate){
        renovateService.updateRenovate(renovate);
        return AjaxResult.success("成功");
    }
}
