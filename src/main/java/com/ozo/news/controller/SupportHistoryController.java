package com.ozo.news.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.SupportHistory;
import com.ozo.news.service.NewsService;
import com.ozo.news.service.SupportHistoryService;
import com.ozo.news.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/support")
public class SupportHistoryController {

    @Autowired
    SupportHistoryService supportHistoryService;

    @Autowired
    NewsService newsService;

    @GetMapping("/findAll")
    public AjaxResult findAllSupportHistory(){
        return AjaxResult.success("成功",supportHistoryService.findAllSupportHistory());
    }

    @RequestMapping(value = "findByCondition", method = RequestMethod.GET, params = {"pageNum","pageSize"})
    public AjaxResult findSupportHistoryByCondition(@RequestBody SupportHistory supportHistory, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        //分页信息
        PageHelper.startPage(pageNum, pageSize);
        //执行分页查询
        PageInfo<SupportHistory> page = new PageInfo<SupportHistory>(supportHistoryService.findSupportHistoryByCondition(supportHistory));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @PostMapping("/findByEntity")
    public AjaxResult findSupportHistoryByEntity(@RequestBody SupportHistory supportHistory) {
        SupportHistory supportHistory1 = supportHistoryService.findSupportHistoryByEntity(supportHistory);
        if(supportHistory1!=null){
            return AjaxResult.success("成功",1);
        }
        return AjaxResult.success("成功",0);
    }

    @RequestMapping(value = "findByParam", method = RequestMethod.GET, params = {"pageNum","pageSize","username","newName"})
    public AjaxResult findSupportHistoryByParam(@RequestParam(value = "username") String username,@RequestParam(value = "newName") String newName, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<SupportHistory> page = new PageInfo<SupportHistory>(supportHistoryService.findSupportHistoryByParam(username,newName));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @PostMapping("/insert")
    public AjaxResult insertSupportHistory(@RequestBody SupportHistory supportHistory){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        supportHistory.setSupportTime(timestamp);
        supportHistoryService.insertSupportHistory(supportHistory);
        Long id = supportHistory.getNews().getNewId();
        newsService.addNewsSupport(id);
        System.out.println(supportHistory);
        return AjaxResult.success("成功");
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET, params = {"supportId"})
    public AjaxResult deleteSupportHistory(@RequestParam(value = "supportId") Long supportId){
        supportHistoryService.deleteSupportHistory(supportId);
        return AjaxResult.success("成功");
    }

    @PostMapping("/deleteByCondition")
    public AjaxResult deleteSupportByCondition(@RequestBody SupportHistory supportHistory){
        supportHistoryService.deleteSupportByCondition(supportHistory);
        Long id = supportHistory.getNews().getNewId();
        newsService.subNewsSupport(id);
        return AjaxResult.success("成功");
    }

    @PostMapping("/update")
    public AjaxResult updateSupportHistory(@RequestBody SupportHistory supportHistory){
        supportHistoryService.updateSupportHistory(supportHistory);
        return AjaxResult.success("成功");
    }
}
