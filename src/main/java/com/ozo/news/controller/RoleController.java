package com.ozo.news.controller;

import com.ozo.news.entity.Role;
import com.ozo.news.service.RoleService;
import com.ozo.news.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    RoleService roleService;

    @GetMapping("/findAll")
    public AjaxResult findAllRole(){
        return AjaxResult.success("成功",roleService.findAllRole());
    }

    @PostMapping("/insert")
    public AjaxResult insertRole(@RequestBody Role role){
        roleService.insertRole(role);
        System.out.println(role);
        return AjaxResult.success("成功");
    }

    @DeleteMapping("/delete/{roleId}")
    public AjaxResult deleteRole(@PathVariable Long roleId){
        roleService.deleteRole(roleId);
        return AjaxResult.success("成功");
    }

    @PutMapping("/update/{id}")
    public AjaxResult updateRole(@PathVariable("id") Long id,@RequestBody Role role){
        roleService.updateRoleById(role,id);
        return AjaxResult.success("成功");
    }


}
