package com.ozo.news.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.Image;
import com.ozo.news.entity.Interest;
import com.ozo.news.entity.News;
import com.ozo.news.service.ImageService;
import com.ozo.news.service.NewsService;
import com.ozo.news.util.AjaxResult;
import com.ozo.news.util.GuuidUtil;
import com.ozo.news.util.ImageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/news")
public class NewsController {

    @Autowired
    NewsService newsService;

    @Autowired
    ImageService imageService;

    private String imgName;

    private static String baseUrl = "http://localhost:8011/";

    @RequestMapping(value = "upload", method = RequestMethod.POST)
    public AjaxResult uploadNewsImage(HttpServletRequest request, @RequestParam("file") MultipartFile file) throws IOException{
        String imgPath;
        Image image = new Image();
        try {
            imgPath = ImageUtil.upload(request, file);
            imgName = imgPath.replaceAll("image/","");
            if (imgPath != null) {
                // 将上传图片的地址封装到实体类
                image.setImageUrl(imgPath);
                image.setImageName(imgName);
                System.out.println("-----------------图片上传成功！");
            }else{
                System.out.println("-----------------图片上传失败！");
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println("----------------图片上传失败！");
        }
        imageService.insertImage(image);
        return AjaxResult.success("成功");
    }

    @RequestMapping(value = "upload1", method = RequestMethod.POST)
    public AjaxResult updateNewsImage(@RequestParam(value = "connectId") Long connectId, HttpServletRequest request, @RequestParam("file") MultipartFile file) throws IOException{
        String imgPath;
        Image image = new Image();
        try {
            imgPath = ImageUtil.upload(request, file);
            String imgName = imgPath.replaceAll("image/","");
            //System.out.println(imgPath);
            if (imgPath != null) {
                // 将上传图片的地址封装到实体类
                image.setConnectId(connectId);
                image.setImageUrl(imgPath);
                image.setImageName(imgName);
                System.out.println(image);
                System.out.println("-----------------图片上传成功！");
            }else{
                System.out.println("-----------------图片上传失败！");
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println("----------------图片上传失败！");
        }
        Image image1 = imageService.findImageByconnectId(connectId);
        if(image1 != null){
            String oldImageName = image1.getImageName();
            ImageUtil.delFile(oldImageName);
            imageService.updateImageByConnectId(image);
        }else {
            imageService.insertImage(image);
        }
        System.out.println("更新图片成功");
        return AjaxResult.success("成功");
    }

    @RequestMapping(value = "findImage", method = RequestMethod.GET)
    public AjaxResult findImage(){
        Image image = imageService.findImageByName(imgName);
        String url = image.getImageUrl();
        String imgUrl = baseUrl + url;
        image.setImageUrl(imgUrl);
        System.out.println(image);
        return AjaxResult.success("成功",image);
    }

    @RequestMapping(value = "findAll", method = RequestMethod.GET, params = {"pageNum","pageSize"})
    public AjaxResult findAllNews(@RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize){
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<News> page = new PageInfo<News>(newsService.findAllNews());
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @RequestMapping(value = "findByCondition", method = RequestMethod.POST, params = {"pageNum","pageSize"})
    public AjaxResult findNewsByCondition(@RequestBody News news, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        //分页信息
        PageHelper.startPage(pageNum, pageSize);
        //执行分页查询
        PageInfo<News> page = new PageInfo<News>(newsService.findNewsByCondition(news));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @RequestMapping(value = "findByParam", method = RequestMethod.GET, params = {"pageNum","pageSize","newName","parentId"})
    public AjaxResult findNewsByParam(@RequestParam(value = "newName") String newName, @RequestParam(value = "parentId") Long parentId,@RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<News> page = new PageInfo<News>(newsService.findNewsByParam(newName,parentId));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @RequestMapping(value = "findByClassifyId", method = RequestMethod.GET, params = {"pageNum","pageSize","minId","maxId"})
    public AjaxResult findNewsByClassifyId(@RequestParam(value = "minId") Long minId, @RequestParam(value = "maxId") Long maxId,@RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<News> page = new PageInfo<News>(newsService.findNewsByClassifyId(minId,maxId));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @RequestMapping(value = "findByClassifyName1", method = RequestMethod.GET, params = {"pageNum","pageSize","classifyName"})
    public AjaxResult findNewsByClassifyName(@RequestParam(value = "classifyName") String classifyName,@RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<News> page = new PageInfo<News>(newsService.findNewsByClassifyName1(classifyName));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @RequestMapping(value = "findById", method = RequestMethod.GET, params = {"newId"})
    public AjaxResult findNewsById(@RequestParam(value = "newId") Long newId) {
        return AjaxResult.success("成功", newsService.findNewsById(newId));
    }

    @PostMapping("/insert/{imageId}")
    public AjaxResult insertNews(@PathVariable("imageId") Long imageId, @RequestBody News news){
        news.setNewId(GuuidUtil.getUUID());
        news.setNewCommentsum(0);
        news.setNewViewsum(0);
        news.setNewSupportsum(0);
        news.setNewOpposesum(0);
        newsService.insertNews(news);
        Image image = imageService.findImageById(imageId);
        image.setConnectId(news.getNewId());
        imageService.updateImage(image);
        return AjaxResult.success("成功");
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET, params = {"newId"})
    public AjaxResult deleteNews(@RequestParam(value = "newId") Long newId){
        newsService.deleteNews(newId);
        return AjaxResult.success("成功");
    }

    @PostMapping("/update")
    public AjaxResult updateNews(@RequestBody News news){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        news.setNewUpdateTime(timestamp);
        newsService.updateNews(news);
        System.out.println(news);
        return AjaxResult.success("成功");
    }
}
