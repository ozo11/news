package com.ozo.news.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.SupportHistory;
import com.ozo.news.entity.Verify;
import com.ozo.news.service.VerifyService;
import com.ozo.news.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/verify")
public class VerifyController {

    @Autowired
    VerifyService verifyService;

    @GetMapping("/findAllVerify")
    public AjaxResult findAllVerify(){
        return AjaxResult.success("成功",verifyService.findAllVerify());
    }

    @RequestMapping(value = "findByCondition", method = RequestMethod.GET, params = {"pageNum","pageSize"})
    public AjaxResult findVerifyByCondition(@RequestBody Verify verify, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        //分页信息
        PageHelper.startPage(pageNum, pageSize);
        //执行分页查询
        PageInfo<Verify> page = new PageInfo<Verify>(verifyService.findVerifyByCondition(verify));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @PostMapping("/insert")
    public AjaxResult insertVerify(@RequestBody Verify verify){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        verify.setVerifyTime(timestamp);
        verifyService.insertVerify(verify);
        System.out.println(verify);
        return AjaxResult.success("成功");
    }

    @DeleteMapping("/delete/{id}")
    public AjaxResult deleteVerify(@PathVariable("id") Long id){
        verifyService.deleteVerify(id);
        return AjaxResult.success("成功");
    }

    @PutMapping("/update")
    public AjaxResult updateVerify(@RequestBody Verify verify){
        verifyService.updateVerify(verify);
        return AjaxResult.success("成功");
    }
}
