package com.ozo.news.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.Classify;
import com.ozo.news.entity.User;
import com.ozo.news.service.ClassifyService;
import com.ozo.news.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/classify")
public class ClassifyController {

    @Autowired
    ClassifyService classifyService;

    @GetMapping("/findAll")
    public AjaxResult findAllClassify(){
        return  AjaxResult.success("成功",classifyService.findAllClassify()) ;
    }


    @RequestMapping(value = "findByCondition", method = RequestMethod.GET, params = {"pageNum","pageSize"})
    public AjaxResult findClassifyByCondition(@RequestBody Classify classify, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        //分页信息
        PageHelper.startPage(pageNum, pageSize);
        //执行分页查询
        PageInfo<Classify> page = new PageInfo<Classify>(classifyService.findClassifyByCondition(classify));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @RequestMapping(value = "findSecond", method = RequestMethod.GET)
    public AjaxResult findSecondClassify() {
        return AjaxResult.success("成功", classifyService.findSecondClassify());
    }


    @RequestMapping(value = "findById", method = RequestMethod.GET)
    public AjaxResult findClassifyById(@RequestParam(value = "classifyId") Long classifyId) {
        return AjaxResult.success("成功", classifyService.findClassifyById(classifyId));
    }

    @PostMapping("/insert")
    public AjaxResult insertClassify(@RequestBody Classify classify){
        classifyService.insertClassify(classify);
        return AjaxResult.success("成功");
    }

    @DeleteMapping("/delete/{id}")
    public AjaxResult deleteClassify(@PathVariable Long id){
        classifyService.deleteClassify(id);
        return AjaxResult.success("成功");
    }

    @PutMapping("/update")
    public AjaxResult updateClassify(@RequestBody Classify classify){
        classifyService.updateClassify(classify);
        return AjaxResult.success("成功");
    }
}
