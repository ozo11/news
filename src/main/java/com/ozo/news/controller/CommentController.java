package com.ozo.news.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.Classify;
import com.ozo.news.entity.Comment;
import com.ozo.news.service.CommentService;
import com.ozo.news.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired
    CommentService commentService;

    @GetMapping("/findAll")
    public AjaxResult findAllComment(){
        return AjaxResult.success("成功",commentService.findAllComment()) ;
    }

    @RequestMapping(value = "findByCondition", method = RequestMethod.GET, params = {"pageNum","pageSize"})
    public AjaxResult findCommentByCondition(@RequestBody  Comment comment, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        //分页信息
        PageHelper.startPage(pageNum, pageSize);
        //执行分页查询
        PageInfo<Comment> page = new PageInfo<Comment>(commentService.findCommentByCondition(comment));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }


    @RequestMapping(value = "findByParam", method = RequestMethod.GET, params = {"pageNum","pageSize","username","newName"})
    public AjaxResult findCommentByParam(@RequestParam(value = "username") String username,@RequestParam(value = "newName") String newName, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<Comment> page = new PageInfo<Comment>(commentService.findCommentByParam(username,newName));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @RequestMapping(value = "findByNewId", method = RequestMethod.GET, params = {"pageNum","pageSize","newId"})
    public AjaxResult findCommentBynewId(@RequestParam(value = "newId") Long newId, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<Comment> page = new PageInfo<Comment>(commentService.findCommentByNewId(newId));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @PostMapping("/insert")
    public AjaxResult insertComment(@RequestBody Comment comment){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        comment.setCommentTime(timestamp);
        commentService.insertComment(comment);
        System.out.println(comment);
        return AjaxResult.success("成功");
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET, params = {"commentId"})
    public AjaxResult deleteComment(@RequestParam(value = "commentId") Long commentId){
        commentService.deleteComment(commentId);
        return AjaxResult.success("成功");
    }

    @PostMapping("/update")
    public AjaxResult updateComment(@RequestBody Comment comment){
        commentService.updateComment(comment);
        return AjaxResult.success("成功");
    }
}
