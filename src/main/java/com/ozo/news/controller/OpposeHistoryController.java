package com.ozo.news.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ozo.news.entity.News;
import com.ozo.news.entity.OpposeHistory;
import com.ozo.news.service.NewsService;
import com.ozo.news.service.OpposeHistoryService;
import com.ozo.news.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/oppose")
public class OpposeHistoryController {

    @Autowired
    OpposeHistoryService opposeHistoryService;

    @Autowired
    NewsService newsService;

    @GetMapping("/findAll")
    public AjaxResult findAllOpposeHistory(){
        return AjaxResult.success("成功",opposeHistoryService.findAllOpposeHistory());
    }

    @RequestMapping(value = "findByCondition", method = RequestMethod.GET, params = {"pageNum","pageSize"})
    public AjaxResult findOpposeHistoryByCondition(@RequestBody OpposeHistory opposeHistory, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        //分页信息
        PageHelper.startPage(pageNum, pageSize);
        //执行分页查询
        PageInfo<OpposeHistory> page = new PageInfo<OpposeHistory>(opposeHistoryService.findOpposeHistoryByCondition(opposeHistory));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @RequestMapping(value = "findByParam", method = RequestMethod.GET, params = {"pageNum","pageSize","username","newName"})
    public AjaxResult findOpposeHistoryByParam(@RequestParam(value = "username") String username,@RequestParam(value = "newName") String newName, @RequestParam(value = "pageNum") Integer pageNum, @RequestParam(value = "pageSize") Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<OpposeHistory> page = new PageInfo<OpposeHistory>(opposeHistoryService.findOpposeHistoryByParam(username,newName));
        return AjaxResult.success("成功", page.getList(), page.getTotal());
    }

    @PostMapping("/findByEntity")
    public AjaxResult findOpposeHistoryByEntity(@RequestBody OpposeHistory opposeHistory) {
        OpposeHistory opposeHistory1 = opposeHistoryService.findOpposeHistoryByEntity(opposeHistory);
        if(opposeHistory1!=null){
            return AjaxResult.success("成功",1);
        }
        return AjaxResult.success("成功",0);
    }

    @PostMapping("/insert")
    public AjaxResult insertOpposeHistory(@RequestBody OpposeHistory opposeHistory){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        opposeHistory.setOpposeTime(timestamp);
        opposeHistoryService.insertOpposeHistory(opposeHistory);
        Long id = opposeHistory.getNews().getNewId();
        newsService.addNewsOppose(id);
        System.out.println(opposeHistory);
        return AjaxResult.success("成功");
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET, params = {"opposeId"})
    public AjaxResult deleteOpposeHistory(@RequestParam(value = "opposeId") Long opposeId){
        opposeHistoryService.deleteOpposeHistory(opposeId);
        return AjaxResult.success("成功");
    }

    @PostMapping("/deleteByCondition")
    public AjaxResult deleteOpposeByCondition(@RequestBody OpposeHistory opposeHistory){
        opposeHistoryService.deleteOpposeByCondition(opposeHistory);
        Long id = opposeHistory.getNews().getNewId();
        newsService.subNewsOppose(id);
        return AjaxResult.success("成功");
    }

    @PostMapping("/update")
    public AjaxResult updateOpposeHistory(@RequestBody OpposeHistory opposeHistory){
        opposeHistoryService.updateOpposeHistory(opposeHistory);
        return AjaxResult.success("成功");
    }
}
