package com.ozo.news.util;

import com.ozo.news.entity.Perms;
import com.ozo.news.entity.Role;
import com.ozo.news.entity.ShiroUser;
import com.ozo.news.entity.User;
import com.ozo.news.service.PermsService;
import com.ozo.news.service.RoleService;
import com.ozo.news.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class UserRealm extends AuthorizingRealm {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private PermsService permsService;

    /**
     * 授权
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //获取当前登录的用户信息
        Subject subject = SecurityUtils.getSubject();
        ShiroUser shiroUser = (ShiroUser) subject.getPrincipal();

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        try{
            List<Role> roles = roleService.findRoleByUserId(shiroUser.getUserId());
            List<Long> roleIds = new ArrayList<Long>();
            for (Role role:roles){
                info.addRole(role.getRoleName());
                roleIds.add(role.getRoleId());//角色存储
            }
            for(Long roleId:roleIds){
                List<Perms> permss = permsService.findPermsByRoleId(roleId);
                for(Perms perms:permss) {
                    info.addStringPermission(perms.getPermsName());//权限储存
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println(info);
        return info;
    }

    /**
     * 认证
     * @param authenticationToken
     * @return
     * @throws AuthenticationException*/

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        User user = userService.findUserByName(token.getUsername());
        if(user != null){
            ShiroUser shiroUser = new ShiroUser();
            shiroUser.setUserId(user.getUserId());
            shiroUser.setName(user.getUsername());
            shiroUser.setPassword(user.getPassword());
            shiroUser.setAuthCacheKey(user.getUsername()+user.getUserId());
            //System.out.println(shiroUser);
            return new SimpleAuthenticationInfo(shiroUser,shiroUser.getPassword(),this.getName());
        }
        return null;

        /*if(user != null){
            return new SimpleAuthenticationInfo(user,user.getPassword(),getName());
        }
        return null;*/
    }
}
