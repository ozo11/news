package com.ozo.news.mapper;

import com.ozo.news.entity.ViewHistory;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ViewHistoryMapper {

    List<ViewHistory> findAllViewHistory();

    List<ViewHistory> findViewHistoryByCondition(ViewHistory viewHistory);

    List<ViewHistory> findViewHistoryByParam(@Param("username") String username, @Param("newName") String newName);

    void insertViewHistory(ViewHistory viewHistory);

    void deleteViewHistory(Long id);
}
