package com.ozo.news.mapper;

import com.ozo.news.entity.Comment;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentMapper {

    List<Comment> findAllComment();

    List<Comment> findCommentByCondition(Comment comment);

    List<Comment> findCommentByParam(@Param("username") String username, @Param("newName") String newName);

    List<Comment> findCommentByNewId(@Param("newId") Long newId);

    void insertComment(Comment comment);

    void deleteComment(Long id);

    void updateComment(Comment comment);
}
