package com.ozo.news.mapper;

import com.ozo.news.entity.SupportHistory;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupportHistoryMapper {

    List<SupportHistory> findAllSupportHistory();

    List<SupportHistory> findSupportHistoryByCondition(SupportHistory supportHistory);

    List<SupportHistory> findSupportHistoryByParam(@Param("username") String username, @Param("newName") String newName);

    SupportHistory findSupportHistoryByEntity(SupportHistory supportHistory);

    void insertSupportHistory(SupportHistory supportHistory);

    void deleteSupportHistory(Long id);

    void deleteSupportByCondition(SupportHistory supportHistory);

    void updateSupportHistory(SupportHistory supportHistory);

}
