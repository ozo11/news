package com.ozo.news.mapper;

import com.ozo.news.entity.Image;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageMapper {

    List<Image> findAllImage();

    List<Image> findImageByCondition(Image image);

    Image findImageByconnectId(@Param("connectId") Long connectId);

    Image findImageByName(@Param("imageName") String imageName);

    Image findImageById(@Param("imageId") Long imageId);

    void insertImage(Image image);

    void deleteImage(Long id);

    void updateImage(Image image);

    void updateImageByConnectId(Image image);
}
