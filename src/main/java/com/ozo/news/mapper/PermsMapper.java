package com.ozo.news.mapper;

import com.ozo.news.entity.Perms;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermsMapper {

    List<Perms> findPermsByRoleId(Long id);

    List<Perms> findAllPerms();
}
