package com.ozo.news.mapper;

import com.ozo.news.entity.Verify;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VerifyMapper {

    public List<Verify> findAllVerify();

    public List<Verify> findVerifyByCondition(Verify verify);

    public void insertVerify(Verify verify);

    public void deleteVerify(Long id);

    public void updateVerify(Verify verify);
}
