package com.ozo.news.mapper;

import com.ozo.news.entity.Renovate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RenovateMapper {

    public List<Renovate> findAllRenovate();

    public List<Renovate> findRenovateByCondition(Renovate renovate);

    public void insertRenovate(Renovate renovate);

    public void deleteRenovate(Long id);

    public void updateRenovate(Renovate renovate);
}
