package com.ozo.news.mapper;

import com.ozo.news.entity.Classify;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassifyMapper {

    List<Classify> findAllClassify();

    List<Classify> findClassifyByCondition(Classify classify);

    List<Classify> findSecondClassify();

    Classify findClassifyById(@Param("classifyId") Long classifyId);

    void insertClassify(Classify classify);

    void deleteClassify(Long id);

    void updateClassify(Classify classify);
}
