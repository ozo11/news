package com.ozo.news.mapper;

import com.ozo.news.entity.Role;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleMapper {

    List<Role> findRoleByUserId(Long id);

    List<Role> findAllRole();

    List<Role> findRoleByUsername(String username);

    void insertRole(Role role);

    void deleteRole(Long id);

    void updateRole(Role role);

    void updateRoleById(@Param("role") Role role,@Param("id") Long id);
}
