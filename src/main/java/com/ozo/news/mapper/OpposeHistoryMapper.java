package com.ozo.news.mapper;

import com.ozo.news.entity.OpposeHistory;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OpposeHistoryMapper {

    List<OpposeHistory> findAllOpposeHistory();

    List<OpposeHistory> findOpposeHistoryByCondition(OpposeHistory opposeHistory);

    List<OpposeHistory> findOpposeHistoryByParam(@Param("username") String username, @Param("newName") String newName);

    OpposeHistory findOpposeHistoryByEntity(OpposeHistory opposeHistory);

    void insertOpposeHistory(OpposeHistory opposeHistory);

    void deleteOpposeHistory(Long id);

    void deleteOpposeByCondition(OpposeHistory opposeHistory);

    void updateOpposeHistory(OpposeHistory opposeHistory);
}
