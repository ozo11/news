package com.ozo.news.service;

import com.ozo.news.entity.Comment;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;
import sun.plugin.com.event.COMEventHandler;

import java.util.List;

@Transactional
public interface CommentService {
    List<Comment> findAllComment();

    List<Comment> findCommentByCondition(Comment comment);

    List<Comment> findCommentByParam(@Param("username") String username, @Param("newName") String newName);

    List<Comment> findCommentByNewId(@Param("newId") Long newId);

    void insertComment(Comment comment);

    void deleteComment(Long id);

    void updateComment(Comment comment);
}
