package com.ozo.news.service;

import com.ozo.news.entity.Classify;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface ClassifyService {
    List<Classify> findAllClassify();

    List<Classify> findClassifyByCondition(Classify classify);

    List<Classify> findSecondClassify();

    Classify findClassifyById(@Param("classifyId") Long classifyId);

    void insertClassify(Classify classify);

    void deleteClassify(Long id);

    void updateClassify(Classify classify);

}
