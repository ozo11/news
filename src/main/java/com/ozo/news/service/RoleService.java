package com.ozo.news.service;

import com.ozo.news.entity.Role;

import java.util.List;

public interface RoleService {

    List<Role> findRoleByUserId(Long id);

    List<Role> findAllRole();

    List<Role> findRoleByUsername(String username);

    void insertRole(Role role);

    void deleteRole(Long id);

    void updateRole(Role role);

    void updateRoleById(Role role, Long id);

}
