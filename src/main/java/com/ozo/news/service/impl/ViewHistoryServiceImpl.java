package com.ozo.news.service.impl;

import com.ozo.news.entity.ViewHistory;
import com.ozo.news.mapper.ViewHistoryMapper;
import com.ozo.news.service.ViewHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class ViewHistoryServiceImpl implements ViewHistoryService {

    @Autowired
    ViewHistoryMapper viewHistoryMapper;

    @Override
    public List<ViewHistory> findAllViewHistory() {
        return viewHistoryMapper.findAllViewHistory();
    }

    @Override
    public List<ViewHistory> findViewHistoryByCondition(ViewHistory viewHistory) {
        return viewHistoryMapper.findViewHistoryByCondition(viewHistory);
    }

    @Override
    public List<ViewHistory> findViewHistoryByParam(String username, String newName) {
        return viewHistoryMapper.findViewHistoryByParam(username, newName);
    }

    @Override
    public void insertViewHistory(ViewHistory viewHistory) {
        viewHistoryMapper.insertViewHistory(viewHistory);
    }

    @Override
    public void deleteViewHistory(Long id) {
        viewHistoryMapper.deleteViewHistory(id);
    }
}
