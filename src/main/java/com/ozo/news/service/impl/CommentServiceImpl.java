package com.ozo.news.service.impl;

import com.ozo.news.entity.Comment;
import com.ozo.news.mapper.CommentMapper;
import com.ozo.news.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentMapper commentMapper;

    @Override
    public List<Comment> findAllComment() {
        return commentMapper.findAllComment();
    }

    @Override
    public List<Comment> findCommentByCondition(Comment comment) {
        return commentMapper.findCommentByCondition(comment);
    }

    @Override
    public List<Comment> findCommentByParam(String username, String newName) {
        return commentMapper.findCommentByParam(username,newName);
    }

    @Override
    public List<Comment> findCommentByNewId(Long newId) {
        return commentMapper.findCommentByNewId(newId);
    }

    @Override
    public void insertComment(Comment comment) {
        commentMapper.insertComment(comment);
    }

    @Override
    public void deleteComment(Long id) {
        commentMapper.deleteComment(id);
    }

    @Override
    public void updateComment(Comment comment) {
        commentMapper.updateComment(comment);
    }
}
