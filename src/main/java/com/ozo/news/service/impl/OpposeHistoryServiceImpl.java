package com.ozo.news.service.impl;

import com.ozo.news.entity.OpposeHistory;
import com.ozo.news.mapper.OpposeHistoryMapper;
import com.ozo.news.service.OpposeHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OpposeHistoryServiceImpl implements OpposeHistoryService {

    @Autowired
    OpposeHistoryMapper opposeHistoryMapper;

    @Override
    public List<OpposeHistory> findAllOpposeHistory() {
        return opposeHistoryMapper.findAllOpposeHistory();
    }

    @Override
    public List<OpposeHistory> findOpposeHistoryByCondition(OpposeHistory opposeHistory) {
        return opposeHistoryMapper.findOpposeHistoryByCondition(opposeHistory);
    }

    @Override
    public List<OpposeHistory> findOpposeHistoryByParam(String username, String newName) {
        return opposeHistoryMapper.findOpposeHistoryByParam(username, newName);
    }

    @Override
    public OpposeHistory findOpposeHistoryByEntity(OpposeHistory opposeHistory) {
        return opposeHistoryMapper.findOpposeHistoryByEntity(opposeHistory);
    }

    @Override
    public void insertOpposeHistory(OpposeHistory opposeHistory) {
        opposeHistoryMapper.insertOpposeHistory(opposeHistory);
    }

    @Override
    public void deleteOpposeHistory(Long id) {
        opposeHistoryMapper.deleteOpposeHistory(id);
    }

    @Override
    public void deleteOpposeByCondition(OpposeHistory opposeHistory) {
        opposeHistoryMapper.deleteOpposeByCondition(opposeHistory);
    }

    @Override
    public void updateOpposeHistory(OpposeHistory opposeHistory) {
        opposeHistoryMapper.updateOpposeHistory(opposeHistory);
    }
}
