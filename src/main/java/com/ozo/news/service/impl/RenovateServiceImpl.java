package com.ozo.news.service.impl;

import com.ozo.news.entity.Renovate;
import com.ozo.news.mapper.RenovateMapper;
import com.ozo.news.service.RenovateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RenovateServiceImpl implements RenovateService {

    @Autowired
    RenovateMapper renovateMapper;

    @Override
    public List<Renovate> findAllRenovate() {
        return renovateMapper.findAllRenovate();
    }

    @Override
    public List<Renovate> findRenovateByCondition(Renovate renovate) {
        return renovateMapper.findRenovateByCondition(renovate);
    }

    @Override
    public void insertRenovate(Renovate renovate) {
        renovateMapper.insertRenovate(renovate);
    }

    @Override
    public void deleteRenovate(Long id) {
        renovateMapper.deleteRenovate(id);
    }

    @Override
    public void updateRenovate(Renovate renovate) {
        renovateMapper.updateRenovate(renovate);
    }
}
