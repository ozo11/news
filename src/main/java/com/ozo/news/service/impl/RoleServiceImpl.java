package com.ozo.news.service.impl;

import com.ozo.news.entity.Role;
import com.ozo.news.mapper.RoleMapper;
import com.ozo.news.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public List<Role> findRoleByUserId(Long id) {
        return roleMapper.findRoleByUserId(id);
    }

    @Override
    public List<Role> findAllRole() {
        return roleMapper.findAllRole();
    }

    @Override
    public List<Role> findRoleByUsername(String username) {
        return roleMapper.findRoleByUsername(username);
    }

    @Override
    public void insertRole(Role role) {
        roleMapper.insertRole(role);
    }

    @Override
    public void deleteRole(Long id) {
        roleMapper.deleteRole(id);
    }

    @Override
    public void updateRole(Role role) {
        roleMapper.updateRole(role);
    }

    @Override
    public void updateRoleById(Role role, Long id) {
        roleMapper.updateRoleById(role,id);
    }
}
