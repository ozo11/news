package com.ozo.news.service.impl;

import com.ozo.news.entity.News;
import com.ozo.news.mapper.NewsMapper;
import com.ozo.news.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {

    @Autowired
    NewsMapper newsMapper;

    @Override
    public List<News> findAllNews() {
        return newsMapper.findAllNews();
    }

    @Override
    public List<News> findNewsByCondition(News news) {
        return newsMapper.findNewsByCondition(news);
    }

    @Override
    public List<News> findNewsByName(String newName) {
        return newsMapper.findNewsByName(newName);
    }

    @Override
    public News findNewsById(Long newId) {
        return newsMapper.findNewsById(newId);
    }

    @Override
    public List<News> findNewsByParam(String newName, Long parentId) {
        return newsMapper.findNewsByParam(newName,parentId);
    }

    @Override
    public List<News> findNewsByClassifyId(Long minId, Long maxId) {
        return newsMapper.findNewsByClassifyId(minId,maxId);
    }

    @Override
    public List<News> findNewsByClassifyName(List<String> classifyNames) {
        return newsMapper.findNewsByClassifyName(classifyNames);
    }

    @Override
    public List<News> findNewsByClassifyName1(String classifyName) {
        return newsMapper.findNewsByClassifyName1(classifyName);
    }

    @Override
    public void insertNews(News news) {
        newsMapper.insertNews(news);
    }

    @Override
    public void updateNews(News news) {
        newsMapper.updateNews(news);
    }

    @Override
    public void addNewsSupport(Long newId) {
        newsMapper.addNewsSupport(newId);
    }

    @Override
    public void subNewsSupport(Long newId) {
        newsMapper.subNewsSupport(newId);
    }

    @Override
    public void addNewsOppose(Long newId) {
        newsMapper.addNewsOppose(newId);
    }

    @Override
    public void subNewsOppose(Long newId) {
        newsMapper.subNewsOppose(newId);
    }

    @Override
    public void deleteNews(long id) {
        newsMapper.deleteNews(id);
    }
}
