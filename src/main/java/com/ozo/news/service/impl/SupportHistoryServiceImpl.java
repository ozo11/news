package com.ozo.news.service.impl;

import com.ozo.news.entity.SupportHistory;
import com.ozo.news.mapper.SupportHistoryMapper;
import com.ozo.news.service.SupportHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SupportHistoryServiceImpl implements SupportHistoryService {

    @Autowired
    SupportHistoryMapper supportHistoryMapper;

    @Override
    public List<SupportHistory> findAllSupportHistory() {
        return supportHistoryMapper.findAllSupportHistory();
    }

    @Override
    public List<SupportHistory> findSupportHistoryByCondition(SupportHistory supportHistory) {
        return supportHistoryMapper.findSupportHistoryByCondition(supportHistory);
    }

    @Override
    public List<SupportHistory> findSupportHistoryByParam(String username, String newName) {
        return supportHistoryMapper.findSupportHistoryByParam(username,newName);
    }

    @Override
    public SupportHistory findSupportHistoryByEntity(SupportHistory supportHistory) {
        return supportHistoryMapper.findSupportHistoryByEntity(supportHistory);
    }

    @Override
    public void insertSupportHistory(SupportHistory supportHistory) {
        supportHistoryMapper.insertSupportHistory(supportHistory);
    }

    @Override
    public void deleteSupportHistory(Long id) {
        supportHistoryMapper.deleteSupportHistory(id);
    }

    @Override
    public void deleteSupportByCondition(SupportHistory supportHistory) {
        supportHistoryMapper.deleteSupportByCondition(supportHistory);
    }

    @Override
    public void updateSupportHistory(SupportHistory supportHistory) {
        supportHistoryMapper.updateSupportHistory(supportHistory);
    }
}
