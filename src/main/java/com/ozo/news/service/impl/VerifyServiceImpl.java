package com.ozo.news.service.impl;

import com.ozo.news.entity.Verify;
import com.ozo.news.mapper.VerifyMapper;
import com.ozo.news.service.VerifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VerifyServiceImpl implements VerifyService {

    @Autowired
    VerifyMapper verifyMapper;

    @Override
    public List<Verify> findAllVerify() {
        return verifyMapper.findAllVerify();
    }

    @Override
    public List<Verify> findVerifyByCondition(Verify verify) {
        return verifyMapper.findVerifyByCondition(verify);
    }

    @Override
    public void insertVerify(Verify verify) {
        verifyMapper.insertVerify(verify);
    }

    @Override
    public void deleteVerify(Long id) {
        verifyMapper.deleteVerify(id);
    }

    @Override
    public void updateVerify(Verify verify) {
        verifyMapper.updateVerify(verify);
    }
}
