package com.ozo.news.service.impl;

import com.ozo.news.entity.Perms;
import com.ozo.news.mapper.PermsMapper;
import com.ozo.news.service.PermsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermsServiceImpl implements PermsService {

    @Autowired
    PermsMapper permsMapper;

    @Override
    public List<Perms> findPermsByRoleId(Long id) {
        return permsMapper.findPermsByRoleId(id);
    }

    @Override
    public List<Perms> findAllPerms() {
        return permsMapper.findAllPerms();
    }
}
