package com.ozo.news.service.impl;

import com.ozo.news.entity.Classify;
import com.ozo.news.mapper.ClassifyMapper;
import com.ozo.news.service.ClassifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassifyServiceImpl implements ClassifyService {

    @Autowired
    private ClassifyMapper classifyMapper;

    @Override
    public List<Classify> findAllClassify() {
        return classifyMapper.findAllClassify();
    }

    @Override
    public List<Classify> findClassifyByCondition(Classify classify) {
        return classifyMapper.findClassifyByCondition(classify);
    }

    @Override
    public List<Classify> findSecondClassify() {
        return classifyMapper.findSecondClassify();
    }

    @Override
    public Classify findClassifyById(Long classifyId) {
        return classifyMapper.findClassifyById(classifyId);
    }

    @Override
    public void insertClassify(Classify classify) {
        classifyMapper.insertClassify(classify);
    }

    @Override
    public void deleteClassify(Long id) {
        classifyMapper.deleteClassify(id);
    }

    @Override
    public void updateClassify(Classify classify) {
        classifyMapper.updateClassify(classify);
    }
}
