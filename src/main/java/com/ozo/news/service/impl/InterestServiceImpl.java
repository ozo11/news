package com.ozo.news.service.impl;

import com.ozo.news.entity.Interest;
import com.ozo.news.mapper.InterestMapper;
import com.ozo.news.service.InterestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InterestServiceImpl implements InterestService {

    @Autowired
    InterestMapper interestMapper;

    @Override
    public List<Interest> findAllInterest() {
        return interestMapper.findAllInterest();
    }

    @Override
    public List<Interest> findInterestByCondition(Interest interest) {
        return interestMapper.findInterestByCondition(interest);
    }

    @Override
    public List<Interest> findInterestByUsername(String username) {
        return interestMapper.findInterestByUsername(username);
    }

    @Override
    public void insertInterest(Interest interest) {
        interestMapper.insertInterest(interest);
    }

    @Override
    public void deleteInterest(Long id) {
        interestMapper.deleteInterest(id);
    }

    @Override
    public void deleteInterestByParam(String interestName, Long userId) {
        interestMapper.deleteInterestByParam(interestName, userId);
    }

    @Override
    public void updateInterest(Interest interest) {
        interestMapper.updateInterest(interest);
    }
}
