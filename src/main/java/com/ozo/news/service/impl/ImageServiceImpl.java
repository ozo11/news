package com.ozo.news.service.impl;

import com.ozo.news.entity.Image;
import com.ozo.news.mapper.ImageMapper;
import com.ozo.news.service.ImageService;
import com.ozo.news.util.ImageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImageServiceImpl implements ImageService {

    @Autowired
    ImageMapper imageMapper;

    @Override
    public List<Image> findAllImage() {
        return imageMapper.findAllImage();
    }

    @Override
    public List<Image> findImageByCondition(Image image) {
        return imageMapper.findImageByCondition(image);
    }

    @Override
    public Image findImageByconnectId(Long connectId) {
        return imageMapper.findImageByconnectId(connectId);
    }

    @Override
    public Image findImageById(Long imageId) {
        return imageMapper.findImageById(imageId);
    }

    @Override
    public Image findImageByName(String imageName) {
        return imageMapper.findImageByName(imageName);
    }

    @Override
    public void insertImage(Image image) {
        imageMapper.insertImage(image);
    }

    @Override
    public void deleteImage(Long id) {
        imageMapper.deleteImage(id);
    }

    @Override
    public void updateImage(Image image) {
        imageMapper.updateImage(image);
    }

    @Override
    public void updateImageByConnectId(Image image) {
        imageMapper.updateImageByConnectId(image);
    }


}
