package com.ozo.news.service;

import com.ozo.news.entity.Image;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface ImageService {

    List<Image> findAllImage();

    List<Image> findImageByCondition(Image image);

    Image findImageByconnectId(@Param("connectId") Long connectId);

    Image findImageById(@Param("imageId") Long imageId);

    Image findImageByName(@Param("imageName") String imageName);

    void insertImage(Image image);

    void deleteImage(Long id);

    void updateImage(Image image);

    void updateImageByConnectId(Image image);
}
