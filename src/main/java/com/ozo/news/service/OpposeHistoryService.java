package com.ozo.news.service;

import com.ozo.news.entity.OpposeHistory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OpposeHistoryService {
    List<OpposeHistory> findAllOpposeHistory();

    List<OpposeHistory> findOpposeHistoryByCondition(OpposeHistory opposeHistory);

    List<OpposeHistory> findOpposeHistoryByParam(@Param("username") String username, @Param("newName") String newName);

    OpposeHistory findOpposeHistoryByEntity(OpposeHistory opposeHistory);

    void insertOpposeHistory(OpposeHistory opposeHistory);

    void deleteOpposeHistory(Long id);

    void deleteOpposeByCondition(OpposeHistory opposeHistory);

    void updateOpposeHistory(OpposeHistory opposeHistory);
}
