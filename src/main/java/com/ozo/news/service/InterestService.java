package com.ozo.news.service;

import com.ozo.news.entity.Interest;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InterestService {
    List<Interest> findAllInterest();

    List<Interest> findInterestByCondition(Interest interest);

    List<Interest> findInterestByUsername(@Param("username") String username);

    void insertInterest(Interest interest);

    void deleteInterest(Long id);

    void deleteInterestByParam(@Param("interestName") String interestName, @Param("userId") Long userId);

    void updateInterest(Interest interest);
}
