package com.ozo.news.service;

import com.ozo.news.entity.SupportHistory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupportHistoryService {
    List<SupportHistory> findAllSupportHistory();

    List<SupportHistory> findSupportHistoryByCondition(SupportHistory supportHistory);

    List<SupportHistory> findSupportHistoryByParam(@Param("username") String username, @Param("newName") String newName);

    SupportHistory findSupportHistoryByEntity(SupportHistory supportHistory);

    void insertSupportHistory(SupportHistory supportHistory);

    void deleteSupportHistory(Long id);

    void deleteSupportByCondition(SupportHistory supportHistory);

    void updateSupportHistory(SupportHistory supportHistory);

}
