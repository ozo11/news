package com.ozo.news.service;

import com.ozo.news.entity.ViewHistory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ViewHistoryService {
    List<ViewHistory> findAllViewHistory();

    List<ViewHistory> findViewHistoryByCondition(ViewHistory viewHistory);

    List<ViewHistory> findViewHistoryByParam(@Param("username") String username, @Param("newName") String newName);

    void insertViewHistory(ViewHistory viewHistory);

    void deleteViewHistory(Long id);
}
