package com.ozo.news.service;

import com.ozo.news.entity.Perms;

import java.util.List;

public interface PermsService {

    List<Perms> findPermsByRoleId(Long id);

    List<Perms> findAllPerms();
}
