package com.ozo.news.service;

import com.ozo.news.entity.Renovate;

import java.util.List;

public interface RenovateService {
    public List<Renovate> findAllRenovate();

    public List<Renovate> findRenovateByCondition(Renovate renovate);

    public void insertRenovate(Renovate renovate);

    public void deleteRenovate(Long id);

    public void updateRenovate(Renovate renovate);
}
