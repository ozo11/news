package com.ozo.news.service;

import com.ozo.news.entity.News;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NewsService {
    List<News> findAllNews();

    List<News> findNewsByCondition(News news);

    List<News> findNewsByName(@Param("newName") String newName);

    News findNewsById(@Param("newId") Long newId);

    List<News> findNewsByParam(@Param("newName") String newName,@Param("parentId") Long parentId);

    List<News> findNewsByClassifyId(@Param("minId") Long minId,@Param("maxId") Long maxId);

    List<News> findNewsByClassifyName(List<String> classifyNames);

    List<News> findNewsByClassifyName1(@Param("classifyName") String classifyName);

    void insertNews(News news);

    void deleteNews(long id);

    void updateNews(News news);

    void addNewsSupport(@Param("newId") Long newId);

    void subNewsSupport(@Param("newId") Long newId);

    void addNewsOppose(@Param("newId") Long newId);

    void subNewsOppose(@Param("newId") Long newId);
}
