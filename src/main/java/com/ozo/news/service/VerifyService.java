package com.ozo.news.service;

import com.ozo.news.entity.Verify;

import java.util.List;

public interface VerifyService {
    public List<Verify> findAllVerify();

    public List<Verify> findVerifyByCondition(Verify verify);

    public void insertVerify(Verify verify);

    public void deleteVerify(Long id);

    public void updateVerify(Verify verify);
}
