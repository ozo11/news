package com.ozo.news.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class News {

    private Long newId;
    private String newName;
    private String newDescribe;
    private Classify classify;
    private String newWord;
    private Integer newCommentsum;
    private Integer newViewsum;
    private Integer newSupportsum;
    private Integer newOpposesum;
    private User user;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date newCreateTime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date newUpdateTime;
}
