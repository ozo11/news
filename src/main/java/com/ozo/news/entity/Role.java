package com.ozo.news.entity;

import lombok.Data;

import java.util.List;

@Data
public class Role {
    private Long roleId;
    private String roleName;
    private String roleDescribe;
    private List<User> users;
}
