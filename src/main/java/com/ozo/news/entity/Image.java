package com.ozo.news.entity;

import lombok.Data;

@Data
public class Image {

    private Long imageId;
    private String imageName;
    private Long connectId;
    private String imageUrl;
}
