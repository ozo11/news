package com.ozo.news.entity;

import lombok.Data;

import java.util.Date;

@Data
public class SupportHistory {

    private Long supportId;
    private User user;
    private News news;
    private Date supportTime;
}
