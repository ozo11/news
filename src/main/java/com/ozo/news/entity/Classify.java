package com.ozo.news.entity;

import lombok.Data;

@Data
public class Classify {
    private Long classifyId;
    private String classifyName;
    private Long parentId;
    private int classifyLevel;
}
