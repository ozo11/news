package com.ozo.news.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ShiroUser implements Serializable {

    private Long userId;
    private String name;
    private String password;
    private List<String> roles;
    private String avatar;
    private String token;
    private String authCacheKey;
}
