package com.ozo.news.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
public class User implements Serializable{

    private Long userId;
    private String username;
    private String password;
    private String userSex;
    private String userPhone;
    private String userEmail;
    private String userStatus;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date enrollTime;
}
