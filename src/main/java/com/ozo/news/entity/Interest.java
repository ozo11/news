package com.ozo.news.entity;

import lombok.Data;

@Data
public class Interest {

    private Long interestId;
    private String interestName;
    private User user1;
}
