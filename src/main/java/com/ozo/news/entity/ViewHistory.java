package com.ozo.news.entity;

import lombok.Data;

import java.util.Date;

@Data
public class ViewHistory {

    private Long viewId;
    private User user;
    private News news;
    private Date viewTime;
}
