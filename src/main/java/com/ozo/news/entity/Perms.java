package com.ozo.news.entity;

import lombok.Data;

import java.util.List;

@Data
public class Perms {
    private Long permsId;
    private String permsName;
    private String permsDescribe;
    private String permission;
    private List<Role> roles;
}
