package com.ozo.news.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Comment {

    private Long commentId;
    private User user;
    private News news;
    private String commentContent;
    private Date commentTime;
}
