package com.ozo.news.entity;

import lombok.Data;

import java.util.Date;

@Data
public class OpposeHistory {

    private Long opposeId;
    private User user;
    private News news;
    private Date opposeTime;
}
