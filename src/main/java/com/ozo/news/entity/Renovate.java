package com.ozo.news.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Renovate {

    private Long renovateId;
    private User user;
    private News news;
    private Date renovateTime;
}
