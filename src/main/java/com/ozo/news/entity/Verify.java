package com.ozo.news.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Verify {

    private Long verifyId;
    private User user;
    private News news;
    private String verifyStatus;
    private Date verifyTime;
}
